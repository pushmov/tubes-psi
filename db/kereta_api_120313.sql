-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 03, 2013 at 02:25 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kereta_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_penumpang`
--

CREATE TABLE IF NOT EXISTS `data_penumpang` (
  `IDPenumpang` int(20) NOT NULL AUTO_INCREMENT,
  `NamaPenumpang` varchar(255) DEFAULT NULL,
  `IDPengenal` varchar(255) DEFAULT NULL,
  `OrderID` varchar(16) DEFAULT NULL,
  `JenisKelamin` char(1) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  PRIMARY KEY (`IDPenumpang`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `data_penumpang`
--

INSERT INTO `data_penumpang` (`IDPenumpang`, `NamaPenumpang`, `IDPengenal`, `OrderID`, `JenisKelamin`, `DOB`) VALUES
(4, 'Rizki 1', '721983719479187987218', 'EUGBDHHL6Q4HGKWO', 'L', '1990-04-23'),
(5, 'Rizki 2', '739827492859267637672', 'EUGBDHHL6Q4HGKWO', NULL, NULL),
(6, 'Rizki 3', '838729582763726176382', 'EUGBDHHL6Q4HGKWO', 'L', '1990-04-23'),
(7, 'Rizki', '93982402820829892', 'VZBLU0FJLKW57ST3', NULL, NULL),
(8, 'Rizki 2', '93982402820829893', 'VZBLU0FJLKW57ST3', NULL, NULL),
(9, 'Rizk 3', '93982402820829894', 'VZBLU0FJLKW57ST3', NULL, NULL),
(10, 'Rizki A', '888288328923283823', 'BW8B75KZ72TCQOQJ', NULL, NULL),
(11, 'Rizki A', '888288328923283823', 'WWED6HAOPVJN27E3', NULL, NULL),
(12, 'Rizki A', '888288328923283823', '279NJ4ZFSXEIZ5WI', NULL, NULL),
(13, 'Rizki A', '888288328923283823', 'CEQRPWTSZ23GZUZU', NULL, NULL),
(14, 'Rizki A', '888288328923283823', 'K7GMQQZGV0L0FIFL', NULL, NULL),
(15, 'Rizki A', '888288328923283823', 'H9137FLF6MSCIOFS', NULL, NULL),
(16, 'Rizki Aprilian 1', '88888888888888888888', 'G547UWH5L9RUDEQN', NULL, NULL),
(17, 'Rizki Aprilian', '92938928389282982948294', 'BS6BYL1HQLK1MQTN', NULL, NULL),
(18, 'Penumpang Wanita', '92390290029520592094', 'UPRWZLYTWNQIQFRF', 'W', '1990-04-23'),
(19, 'Rizki BY 1', '923902930204992402940293', 'ERZDF9OMY96TN6LV', 'P', '1990-04-05'),
(20, 'Rizki BY 2', '902939028928982938239282', 'EQRTOFD5AA160KTD', 'P', '1990-03-21'),
(21, 'Rizki BY 5', '92390290294029402930239', '7CW2SV0Q0677S762', 'P', '1990-03-23'),
(22, 'Rizki Aprilian 8', '8888888888888888888888888888888', 'PPKDIRN7IOXL6VRN', 'P', '1990-03-23'),
(23, 'ccc', '1234555555555555555555555', 'GYJ9N4GDFEHDM4SF', 'P', '1990-03-01'),
(24, 'aaa', '34566666666666666666666666', 'GYJ9N4GDFEHDM4SF', NULL, '1990-03-05');

-- --------------------------------------------------------

--
-- Table structure for table `data_stasiun`
--

CREATE TABLE IF NOT EXISTS `data_stasiun` (
  `IDStasiun` int(11) NOT NULL AUTO_INCREMENT,
  `Kota` varchar(255) DEFAULT NULL,
  `NamaStasiun` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDStasiun`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=201 ;

--
-- Dumping data for table `data_stasiun`
--

INSERT INTO `data_stasiun` (`IDStasiun`, `Kota`, `NamaStasiun`) VALUES
(1, 'Bandung', 'Bandung'),
(2, 'Bandung', 'Cimahi'),
(3, 'Bandung', 'GedeBage'),
(4, 'Bandung', 'KiaraCondong'),
(5, 'Bandung', 'Padalarang'),
(6, 'Bandung', 'Rancaekek'),
(7, 'Bekasi', 'Bekasi'),
(8, 'Banjar', 'Banjar'),
(9, 'Banjar', 'Langen'),
(10, 'Banyumas', 'Karangsari'),
(11, 'Banyumas', 'Kebasen'),
(12, 'Banyumas', 'Kemranjen'),
(13, 'Banyumas', 'Notog'),
(14, 'Banyumas', 'Sumpiuh'),
(15, 'Banyumas', 'Tambak'),
(16, 'Batang', 'Batang'),
(17, 'Batang', 'Ujungnegoro'),
(18, 'Blitar', 'Garum'),
(19, 'Blitar', 'Kesamben'),
(20, 'Blitar', 'Talun'),
(21, 'Blitar', 'Wlingi'),
(22, 'Blora', 'Cepu'),
(23, 'Blora', 'Doplang'),
(24, 'Blora', 'RanduBlatung'),
(25, 'Brebes', 'Bulakamba'),
(26, 'Brebes', 'Bumiayu'),
(27, 'Brebes', 'Ketanggungan'),
(28, 'Brebes', 'Ketanggungan Barat'),
(29, 'Brebes', 'Larangan'),
(30, 'Brebes', 'Larangan'),
(31, 'Brebes', 'Linggapura'),
(32, 'Brebes', 'Patuguran'),
(33, 'Brebes', 'Songgom'),
(34, 'Brebes', 'Tanjung'),
(35, 'Ciamis', 'Banjarsari'),
(36, 'Ciamis', 'Ciamis'),
(37, 'Cikampek', 'Cikampek'),
(38, 'Cilacap', 'Cilacap'),
(39, 'Cilacap', 'Cipari'),
(40, 'Cilacap', 'Gandrungmangun'),
(41, 'Cilacap', 'Gumilir'),
(42, 'Cilacap', 'Jeruklegi'),
(43, 'Cilacap', 'Kawunganten'),
(44, 'Cilacap', 'Kroya'),
(45, 'Cilacap', 'Lebeng'),
(46, 'Cilacap', 'Maos'),
(47, 'Cilacap', 'Mluwung'),
(48, 'Cilacap', 'Sidareja'),
(49, 'Cilacap', 'Sikampuh'),
(50, 'Cilegon', 'Merak'),
(51, 'Cirebon', 'Arjawinangun'),
(52, 'Cirebon', 'Babakan'),
(53, 'Cirebon', 'Ciledug'),
(54, 'Cirebon', 'Cirebon'),
(55, 'Cirebon', 'Cirebonprujakan'),
(56, 'Cirebon', 'Karangsuwung'),
(57, 'Cirebon', 'Losari'),
(58, 'Cirebon', 'Luwung'),
(59, 'Cirebon', 'Sindanglaut'),
(60, 'Cirebon', 'Waruduwur'),
(61, 'Demak', 'Brumbung'),
(62, 'Garut', 'Cibatu'),
(63, 'Garut', 'Cipeundeuy'),
(64, 'Garut', 'Lebakjero'),
(65, 'Garut', 'Leles'),
(66, 'Garut', 'Nagrek'),
(67, 'Garut', 'Warung Bandrek'),
(68, 'Gombong', 'Gombong'),
(69, 'Gombong', 'Ijo'),
(70, 'Grobogan', 'Gundih'),
(71, 'Grobogan', 'Karangjati'),
(72, 'Grobogan', 'Kedungjati'),
(73, 'Grobogan', 'Ngrombo'),
(74, 'Indramayu', 'Cilegeh'),
(75, 'Indramayu', 'Haurgeulis'),
(76, 'Indramayu', 'Jatibarang'),
(77, 'Indramayu', 'Kadokangangabus'),
(78, 'Indramayu', 'Kertasemaya'),
(79, 'Indramayu', 'Terisi'),
(80, 'Jakarta', 'Gambir'),
(81, 'Jakarta', 'Jakarta Kota'),
(82, 'Jakarta', 'Jatinegara'),
(83, 'Jakarta', 'Manggarai'),
(84, 'Jakarta', 'Pasar Senen'),
(85, 'Jakarta', 'Tanah Abang'),
(86, 'Jakarta', 'Tanjung Priuk'),
(87, 'Jatibarang', 'Bangoduwa'),
(88, 'Jember', 'Jatiroto'),
(89, 'Jember', 'Kalisat'),
(90, 'Jember', 'Jember'),
(91, 'Jember', 'Kalipuji'),
(92, 'Jember', 'Rambipuji'),
(93, 'Jember', 'Tanggul'),
(94, 'Jombang', 'Jombang'),
(95, 'Jombang', 'Sembung'),
(96, 'Kebumen', 'Karanganyar'),
(97, 'Kebumen', 'Kebumen'),
(98, 'Kebumen', 'Kutowinangun'),
(99, 'Kebumen', 'Prembun'),
(100, 'Kebumen', 'Sruweng'),
(101, 'Kebumen', 'Wonosari'),
(102, 'Kediri', 'Kediri'),
(103, 'Kediri', 'Papar'),
(104, 'Kendal', 'Kalibodri'),
(105, 'Kendal', 'Kaliwungu'),
(106, 'Kendal', 'Weleri'),
(107, 'Klaten', 'Srowot'),
(108, 'Klaten', 'Klaten'),
(109, 'Kutoarjo', 'Kutoarjo'),
(110, 'Lamongan', 'Babat'),
(111, 'Lamongan', 'Lamongan'),
(112, 'Lebak', 'Rangkas Bitung'),
(113, 'Lumajang', 'Klakah'),
(114, 'Madiun', 'Caruban'),
(115, 'Madiun', 'Madiun'),
(116, 'Madiun', 'Paron'),
(117, 'Madiun', 'Saradan'),
(118, 'Malang', 'Blimbing'),
(119, 'Malang', 'Kepanjen'),
(120, 'Malang', 'Lawang'),
(121, 'Malang', 'Malang'),
(122, 'Malang', 'Malang Kota Lama'),
(123, 'Malang', 'Sumberpucung'),
(124, 'Nganjuk', 'Bagor'),
(125, 'Nganjuk', 'Baron'),
(126, 'Nganjuk', 'Kertosono'),
(127, 'Nganjuk', 'Nganjuk'),
(128, 'Nganjuk', 'Wilangan'),
(129, 'Ngawi', 'Geneng'),
(130, 'Ngawi', 'Kedunggalar'),
(131, 'Ngawi', 'Walikukun'),
(132, 'Pasuruan', 'Bangil'),
(133, 'Pasuruan', 'Grati'),
(134, 'Pasuruan', 'Pasuruan'),
(135, 'Pekalongan', 'Bojong'),
(136, 'Pekalongan', 'Krengseng'),
(137, 'Pekalongan', 'Pekalongan'),
(138, 'Pekalongan', 'Plabuan'),
(139, 'Pekalongan', 'Sragi'),
(140, 'Pemalang', 'Comal'),
(141, 'Pemalang', 'Pemalang'),
(142, 'Pemalang', 'Petarukan'),
(143, 'Prabumulih', 'Blimbingpendopo'),
(144, 'Prabumulih', 'Gunungmegang'),
(145, 'Prabumulih', 'Prabumulih'),
(146, 'Purwakarta', 'Plered'),
(147, 'Purwakarta', 'Purwakarta'),
(148, 'Purwokerto', 'Purwokerto'),
(149, 'Purworejo', 'Jenar'),
(150, 'Semarang', 'Poncol'),
(151, 'Semarang', 'Tawang'),
(152, 'Serang', 'Serang'),
(153, 'Sidoarjo', 'Gedangan'),
(154, 'Sidoarjo', 'Krian'),
(155, 'Sidoarjo', 'Sepanjang'),
(156, 'Sidoarjo', 'Sidoarjo'),
(157, 'Solo', 'Purwosari'),
(158, 'Solo', 'Balapan'),
(159, 'Solo', 'Jebres'),
(160, 'Sragen', 'Kebonromo'),
(161, 'Sragen', 'Kedungbanteng'),
(162, 'Sragen', 'Masaran'),
(163, 'Sragen', 'Sragen'),
(164, 'Subang', 'Cipunegara'),
(165, 'Subang', 'Pegadenbaru'),
(166, 'Subang', 'Tanjungrasa'),
(167, 'Surabaya', 'Gubeng'),
(168, 'Surabaya', 'Pasar Turi'),
(169, 'Surabaya', 'Wonokromo'),
(170, 'Tangerang', 'Tangerang'),
(171, 'Tasikmalaya', 'Awipari'),
(172, 'Tasikmalaya', 'Ciawi'),
(173, 'Tasikmalaya', 'Manonjaya'),
(174, 'Tasikmalaya', 'Rajapolah'),
(175, 'Tasikmalaya', 'Tasikmalaya'),
(176, 'Tegal', 'Tegal'),
(177, 'Tegal', 'Prupuk'),
(178, 'Tulungagung', 'Ngunut'),
(179, 'Tulungagung', 'Tulungagung'),
(180, 'Yogyakarta', 'Brambanan'),
(181, 'Yogyakarta', 'Lempuyangan'),
(182, 'Yogyakarta', 'Sentolo'),
(183, 'Yogyakarta', 'Wates'),
(184, 'Yogyakarta', 'Yogyakarta'),
(185, 'Bojonegoro', 'Bojonegoro'),
(186, 'Bojonegoro', 'Kalitidu'),
(187, 'Bojonegoro', 'Sumberrejo'),
(188, 'Mojokerto', 'Mojokerto'),
(189, 'Cirebon', 'Prujakan'),
(190, 'Yogyakarta', 'Tugu'),
(191, 'Blitar', 'Blitar'),
(192, 'Banyuwangi', 'Banyuwangi Baru'),
(193, 'Banyuwangi', 'Glenmore'),
(194, 'Banyuwangi', 'Kalibaru'),
(195, 'Banyuwangi', 'Kalisetail'),
(196, 'Banyuwangi', 'Karangasem'),
(197, 'Banyuwangi', 'Rogojampi'),
(198, 'Banyuwangi', 'Sumberwadung'),
(199, 'Banyuwangi', 'Temuguruh'),
(200, 'Probolinggo', 'Probolinggo');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_berangkat`
--

CREATE TABLE IF NOT EXISTS `jadwal_berangkat` (
  `IDJadwal` int(20) NOT NULL AUTO_INCREMENT,
  `IDKeretaApi` int(20) DEFAULT NULL,
  `JamBerangkat` time DEFAULT NULL,
  `JamSampai` time DEFAULT NULL,
  `Dari` int(20) DEFAULT NULL,
  `Tujuan` int(20) DEFAULT NULL,
  PRIMARY KEY (`IDJadwal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76 ;

--
-- Dumping data for table `jadwal_berangkat`
--

INSERT INTO `jadwal_berangkat` (`IDJadwal`, `IDKeretaApi`, `JamBerangkat`, `JamSampai`, `Dari`, `Tujuan`) VALUES
(1, 3, '09:30:00', '19:11:00', 80, 168),
(2, 3, '21:30:00', '07:11:00', 80, 168),
(3, 3, '08:10:00', '17:41:00', 168, 80),
(4, 3, '20:10:00', '05:41:00', 168, 80),
(5, 21, '07:30:00', NULL, 80, 151),
(6, 21, '16:00:00', NULL, 151, 80),
(7, 23, '08:00:00', '16:30:00', 158, 80),
(8, 23, '20:20:00', '04:42:00', 80, 158),
(9, 24, '05:00:00', '07:54:00', 1, 80),
(10, 24, '12:00:00', '14:54:00', 80, 1),
(11, 25, '05:30:00', '11:34:00', 151, 80),
(12, 25, '16:45:00', '22:59:00', 80, 151),
(13, 26, '07:30:00', '18:43:00', 167, 1),
(14, 26, '08:00:00', '19:28:00', 1, 167),
(15, 27, '16:00:00', '04:43:00', 94, 80),
(16, 27, '17:45:00', '06:15:00', 80, 94),
(17, 34, '20:00:00', '04:30:00', 158, 80),
(18, 34, '08:00:00', '16:29:00', 80, 158),
(20, 490, '05:45:00', '08:34:00', 54, 80),
(21, 490, '09:00:00', '11:49:00', 80, 54),
(22, 490, '14:00:00', '16:55:00', 54, 80),
(23, 490, '17:15:00', '20:09:00', 80, 54),
(24, 35, '18:15:00', '06:24:00', 167, 80),
(25, 35, '17:00:00', '05:15:00', 80, 167),
(26, 36, NULL, NULL, 119, 80),
(27, 36, NULL, NULL, 80, 119),
(28, 37, NULL, NULL, 137, 185),
(29, 37, NULL, NULL, 185, 137),
(30, 38, NULL, NULL, 184, 80),
(31, 38, NULL, NULL, 80, 184),
(32, 39, '19:00:00', '06:52:00', 1, 167),
(33, 39, '18:00:00', '06:10:00', 167, 1),
(34, 41, '06:00:00', NULL, 176, 80),
(35, 41, '11:00:00', '15:47:00', 80, 176),
(36, 41, '16:15:00', '20:40:00', 176, 80),
(37, 41, '18:30:00', '23:15:00', 80, 176),
(38, 43, '08:00:00', '14:45:00', 151, 84),
(40, 44, '06:50:00', '14:57:00', 84, 184),
(41, 44, '07:15:00', '15:22:00', 184, 84),
(42, 45, '15:15:00', '04:25:00', 81, 168),
(43, 45, '17:10:00', '05:31:00', 168, 81),
(44, 46, '20:45:00', NULL, 1, 168),
(45, 46, '16:00:00', NULL, 168, 1),
(46, 47, '05:00:00', '07:30:00', 176, 151),
(47, 47, '08:35:00', '11:03:00', 151, 176),
(48, 47, '11:51:00', '14:25:00', 176, 151),
(49, 47, '16:30:00', '18:53:00', 151, 176),
(50, 48, '07:00:00', '15:41:00', 1, 158),
(51, 48, '07:00:00', '15:41:00', 158, 1),
(52, 48, '19:00:00', '03:45:00', 1, 158),
(53, 48, '19:00:00', '03:27:00', 158, 1),
(54, 50, '15:35:00', '07:26:00', 1, 121),
(55, 50, '12:45:00', '04:10:00', 121, 1),
(56, 51, '08:00:00', '15:01:00', 121, 184),
(57, 51, '22:15:00', '05:14:00', 184, 121),
(58, 52, '17:00:00', '06:08:00', 1, 167),
(59, 52, '16:30:00', '05:59:00', 167, 1),
(60, 53, NULL, NULL, 167, 192),
(61, 53, NULL, NULL, 192, 167),
(62, 54, '18:15:00', '01:23:00', 38, 80),
(63, 54, '06:30:00', '13:20:00', 80, 38),
(64, 56, NULL, NULL, 167, 184),
(65, 56, NULL, NULL, 184, 167),
(66, 57, '07:00:00', '14:29:00', 109, 84),
(67, 57, '20:30:00', '03:38:00', 84, 109),
(68, 57, '18:30:00', '02:21:00', 109, 84),
(69, 57, '07:10:00', '14:24:00', 84, 109),
(71, 58, '19:50:00', '02:39:00', 84, 151),
(72, 59, '21:15:00', '05:56:00', 84, 158),
(73, 59, '18:00:00', '03:36:00', 158, 84),
(74, 60, '19:30:00', '04:51:00', 84, 184),
(75, 60, '18:30:00', '03:48:00', 184, 84);

-- --------------------------------------------------------

--
-- Table structure for table `kereta_api`
--

CREATE TABLE IF NOT EXISTS `kereta_api` (
  `IDKeretaApi` int(10) NOT NULL AUTO_INCREMENT,
  `NamaKeretaApi` varchar(255) DEFAULT NULL,
  `Kelas` varchar(10) DEFAULT NULL,
  `JumlahGerbong` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IDKeretaApi`),
  UNIQUE KEY `IDKeretaApi` (`IDKeretaApi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=491 ;

--
-- Dumping data for table `kereta_api`
--

INSERT INTO `kereta_api` (`IDKeretaApi`, `NamaKeretaApi`, `Kelas`, `JumlahGerbong`) VALUES
(3, 'Argo Bromo Anggrek', 'eksekutif', 4),
(21, 'Argo Muria', 'eksekutif', 4),
(23, 'Argo Lawu', 'eksekutif', 4),
(24, 'Argo Parahyangan 19', 'eksekutif', 4),
(25, 'Argo Sindoro', 'eksekutif', 4),
(26, 'Argo Wilis', 'eksekutif', 4),
(27, 'Bangunkarta', 'eksekutif', 4),
(34, 'Argo Dwipangga 9', 'eksekutif', 4),
(35, 'Bima', 'eksekutif', 4),
(36, 'Gajayana', 'eksekutif', 4),
(37, 'Sembrani', 'eksekutif', 4),
(38, 'Taksaka', 'eksekutif', 4),
(39, 'Turangga', 'eksekutif', 4),
(40, 'Cianjuran', 'bisnis', 4),
(41, 'Cirebon Ekspres', 'bisnis', 4),
(43, 'Fajar Utama Semarang', 'bisnis', 4),
(44, 'Fajar Utama Yogya', 'bisnis', 4),
(45, 'Gumarang', 'bisnis', 4),
(46, 'Harina', 'bisnis', 4),
(47, 'Kaligung Mas', 'bisnis', 4),
(48, 'Lodaya', 'bisnis', 4),
(50, 'Malabar', 'bisnis', 4),
(51, 'Malioboro Ekspres', 'bisnis', 4),
(52, 'Mutiara Selatan', 'bisnis', 4),
(53, 'Mutiara Timur', 'bisnis', 4),
(54, 'Purwojaya', 'bisnis', 4),
(56, 'Sancaka', 'bisnis', 4),
(57, 'Sawunggalih Utama', 'bisnis', 4),
(58, 'Senja Utama Semarang', 'bisnis', 4),
(59, 'Senja Utama Solo', 'bisnis', 4),
(60, 'Senja Utama Yogya', 'bisnis', 4),
(63, 'Bogowonto', 'ekonomi', 4),
(64, 'Gajah Wong', 'ekonomi', 4),
(65, 'Majapahit', 'ekonomi', 4),
(66, 'Menoreh', 'ekonomi', 4),
(67, 'Kalimaya', 'ekonomi', 4),
(68, 'Banten Ekspres', 'ekonomi', 4),
(69, 'Brantas', 'ekonomi', 4),
(70, 'Gaya Baru Malam Selatan', 'ekonomi', 4),
(71, 'Kahuripan', 'ekonomi', 4),
(72, 'Kaligung Ekonomi', 'ekonomi', 4),
(73, 'Kertajaya', 'ekonomi', 4),
(74, 'Kutojaya Selatan', 'ekonomi', 4),
(75, 'Kutojaya Utara', 'ekonomi', 4),
(76, 'Langsam', 'ekonomi', 4),
(77, 'Logawa', 'ekonomi', 4),
(78, 'Matarmaja', 'ekonomi', 4),
(79, 'Pasundan', 'ekonomi', 4),
(80, 'Feeder Tawang Jaya', 'ekonomi', 4),
(81, 'Patas Purwakarta', 'ekonomi', 4),
(82, 'Penataran', 'ekonomi', 4),
(83, 'Probowangi', 'ekonomi', 4),
(84, 'Progo', 'ekonomi', 4),
(85, 'Putri Deli', 'ekonomi', 4),
(86, 'Rangkas Jaya', 'ekonomi', 4),
(87, 'Rapih Dhoho', 'ekonomi', 4),
(88, 'Rajabasa', 'ekonomi', 4),
(89, 'Senja Bengawan', 'ekonomi', 4),
(90, 'Serayu', 'ekonomi', 4),
(91, 'Serelo', 'ekonomi', 4),
(92, 'Siantar Ekspres', 'ekonomi', 4),
(93, 'Sibinuang', 'ekonomi', 4),
(94, 'Simandra', 'ekonomi', 4),
(95, 'Sri Tanjung', 'ekonomi', 4),
(96, 'KRD Tanjung Priok-Cikampek', 'ekonomi', 4),
(97, 'Tawang Alun', 'ekonomi', 4),
(98, 'Tawang Jaya', 'ekonomi', 4),
(99, 'Tegal Arum', 'ekonomi', 4),
(490, 'New Argo Jati', 'ekonomi', 4);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `OrderID` int(7) NOT NULL AUTO_INCREMENT,
  `OrderNumber` varchar(16) NOT NULL,
  `TanggalOrder` datetime DEFAULT NULL,
  `TanggalBerangkat` date DEFAULT NULL,
  `IDKeretaApi` int(20) DEFAULT NULL,
  `IDAsal` int(20) DEFAULT NULL,
  `IDTujuan` int(20) DEFAULT NULL,
  `IsVerified` tinyint(1) NOT NULL DEFAULT '0',
  `IDUser` int(10) DEFAULT NULL,
  PRIMARY KEY (`OrderID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`OrderID`, `OrderNumber`, `TanggalOrder`, `TanggalBerangkat`, `IDKeretaApi`, `IDAsal`, `IDTujuan`, `IsVerified`, `IDUser`) VALUES
(2, 'EUGBDHHL6Q4HGKWO', '2013-11-10 12:14:54', '2013-11-14', 39, 1, 167, 1, 2),
(3, 'VZBLU0FJLKW57ST3', '2013-11-15 13:57:50', '2013-11-21', 48, 1, 158, 0, 3),
(4, 'BW8B75KZ72TCQOQJ', '2013-11-17 12:32:59', '2013-11-19', 46, 1, 168, 0, 2),
(5, 'WWED6HAOPVJN27E3', '2013-11-17 12:33:54', '2013-11-19', 46, 1, 168, 0, 2),
(6, '279NJ4ZFSXEIZ5WI', '2013-11-17 12:34:13', '2013-11-19', 46, 1, 168, 0, 2),
(7, 'CEQRPWTSZ23GZUZU', '2013-11-17 12:34:27', '2013-11-19', 46, 1, 168, 0, 2),
(8, 'K7GMQQZGV0L0FIFL', '2013-11-17 12:34:36', '2013-11-19', 46, 1, 168, 0, 2),
(9, 'H9137FLF6MSCIOFS', '2013-11-17 12:34:42', '2013-11-19', 46, 1, 168, 1, 2),
(10, 'G547UWH5L9RUDEQN', '2013-11-30 13:58:36', '2013-12-04', 48, 1, 158, 1, 2),
(11, 'BS6BYL1HQLK1MQTN', '2013-11-30 16:00:06', '2013-12-04', 50, 1, 121, 0, 2),
(12, 'UPRWZLYTWNQIQFRF', '2013-11-30 18:06:38', '2013-12-03', 50, 1, 121, 0, 2),
(13, 'ERZDF9OMY96TN6LV', '2013-12-01 14:45:03', '2013-12-05', 50, 1, 121, 0, 2),
(14, 'EQRTOFD5AA160KTD', '2013-12-01 14:46:04', '2013-12-05', 50, 1, 121, 0, 2),
(15, '7CW2SV0Q0677S762', '2013-12-01 15:01:16', '2013-12-05', 50, 1, 121, 0, 2),
(16, 'PPKDIRN7IOXL6VRN', '2013-12-02 03:19:05', '2013-12-05', 50, 1, 121, 1, 2),
(17, 'GYJ9N4GDFEHDM4SF', '2013-12-02 04:09:48', '2013-12-07', 50, 1, 121, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `pemberangkatan`
--

CREATE TABLE IF NOT EXISTS `pemberangkatan` (
  `IDPemberangkatan` int(20) NOT NULL AUTO_INCREMENT,
  `PemberangkatanTanggal` date DEFAULT NULL,
  `IDJadwal` int(20) DEFAULT NULL,
  `IDKeretaApi` int(20) DEFAULT NULL,
  `AddDateTime` datetime DEFAULT NULL,
  `NomorGerbong` int(10) DEFAULT NULL,
  `NomorKursi` varchar(10) DEFAULT NULL,
  `IDPenumpang` int(20) DEFAULT NULL,
  PRIMARY KEY (`IDPemberangkatan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `pemberangkatan`
--

INSERT INTO `pemberangkatan` (`IDPemberangkatan`, `PemberangkatanTanggal`, `IDJadwal`, `IDKeretaApi`, `AddDateTime`, `NomorGerbong`, `NomorKursi`, `IDPenumpang`) VALUES
(1, '2013-11-14', 32, 39, '2013-11-17 17:50:10', 3, '5D', 6),
(2, '2013-11-14', 32, 39, '2013-11-17 12:32:59', 3, '5C', 4),
(3, '2013-11-14', 32, 39, '2013-11-17 12:34:42', 1, '1A', 5),
(4, '2013-12-04', 52, 48, '2013-11-30 13:58:37', 1, 'A1', 16),
(5, '2013-12-04', 54, 50, '2013-11-30 16:00:06', 1, 'A1', 17),
(6, '2013-12-03', 54, 50, '2013-11-30 18:06:38', 1, '1A', 18),
(7, '2013-12-05', 54, 50, '2013-12-01 14:45:03', 1, '1B', 19),
(8, '2013-12-05', 54, 50, '2013-12-01 14:46:04', 1, '1C', 20),
(9, '2013-12-05', 54, 50, '2013-12-01 15:01:16', 1, '1D', 21),
(10, '2013-12-05', 54, 50, '2013-12-02 03:19:05', 1, '2B', 22),
(11, '2013-12-07', 54, 50, '2013-12-02 04:09:48', 2, '5A', 23),
(12, '2013-12-07', 54, 50, '2013-12-02 04:09:48', 2, '5B', 24);

-- --------------------------------------------------------

--
-- Table structure for table `rute_kereta_api`
--

CREATE TABLE IF NOT EXISTS `rute_kereta_api` (
  `IDRoute` int(11) NOT NULL AUTO_INCREMENT,
  `IDKeretaApi` int(20) NOT NULL,
  `Rute` text,
  PRIMARY KEY (`IDRoute`),
  UNIQUE KEY `IDRoute` (`IDRoute`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `rute_kereta_api`
--

INSERT INTO `rute_kereta_api` (`IDRoute`, `IDKeretaApi`, `Rute`) VALUES
(1, 3, '168,185,22,151,137,176,54,83,80'),
(2, 23, '158,108,184,96,148,54,76,75,37,7,82,83,80'),
(3, 24, '1,82,80'),
(4, 25, '151,150,137,176,54,82,80'),
(5, 26, '167,94,115,158,184,109,44,8,175,63,1'),
(6, 34, '158,108,184,96,148,54,76,75,37,7,82,83,80'),
(7, 490, '54,76,37,7,82,83,80'),
(8, 26, '1,63,175,8,44,184,158,115,126,94,167'),
(9, 27, '94,126,127,115,116,151,137,176,54,82,80'),
(10, 27, '80,82,54,176,137,151,116,115,127,126,94'),
(11, 21, '80,151,150,137,176,54,82'),
(12, 23, '80,82,54,148,97,184,108,158'),
(13, 25, '80,54,176,137,150,151'),
(14, 26, '1,63,175,8,44,184,158,115,126,94,167'),
(15, 34, '80,82,54,148,12,184,108,158'),
(16, 490, '80,82,76,54'),
(17, 35, '167,188,94,115,158,184,148,54,82,80'),
(18, 35, '80,82,54,148,184,158,115,94,188,167'),
(19, 36, '119,21,179,102,126,115,158,184,148,54,82,80'),
(20, 36, '80,82,54,148,184,158,115,126,102,179,21,119'),
(21, 37, '137,176,151,22,185'),
(22, 37, '185,22,151,176,137'),
(23, 38, '184,109,68,148,54,82,80'),
(24, 38, '80,54,27,148,68,184'),
(25, 38, '184,148,177,28,54,82,80'),
(26, 38, '80,54,27,177,148,109,184'),
(27, 39, '1,63,175,8,44,109,184,158,115,126,94,188,169,167'),
(28, 39, '167,188,94,126,115,158,184,44,8,175,63,1'),
(29, 41, '176,54,82,80'),
(30, 41, '80,82,54,176'),
(31, 43, '151,106,137,141,176,189,82,84'),
(32, 44, '184,183,148,54,82,84'),
(33, 44, '84,76,54,148,109,183,184'),
(34, 45, '81,84,54,176,137,151,22,185,110,111,168'),
(35, 45, '168,111,110,185,22,151,137,176,54,82,84,81'),
(36, 46, '1,147,37,54,176,137,151,22,185,168'),
(37, 46, '168,185,22,151,137,176,54,37,147,1'),
(38, 47, '176,141,137,150,151'),
(39, 47, '151,150,137,141,176'),
(40, 48, '1,4,63,175,8,96,97,109,184,108,158'),
(41, 48, '158,108,184,109,97,96,8,175,63,4,1'),
(42, 50, '1,4,63,175,8,44,68,97,98,109,183,184,108,158,116,115,127,126,102,179,21,119,121'),
(43, 50, '121,119,21,179,102,126,127,115,116,158,108,184,183,109,98,97,68,44,8,175,63,4,1'),
(44, 51, '121,119,21,191,179,102,127,115,158,184'),
(45, 51, '184,158,115,127,102,179,191,21,119,121'),
(46, 52, '1,4,63,175,36,8,46,97,184,158,115,127,94,188,169,167'),
(47, 52, '167,169,188,94,126,127,115,158,184,96,8,36,175,63,4,1'),
(48, 53, '167,156,132,134,200,93,89,194,195,199,197,196,192'),
(49, 53, '192,196,197,199,195,194,89,93,200,134,132,156,167'),
(50, 54, '38,44,148,80'),
(51, 54, '80,82,7,148,44,38'),
(52, 56, '167,188,94,126,127,115,158,108,184'),
(53, 56, '184,108,158,115,127,126,94,188,167'),
(54, 56, '167,94,126,127,115,158,184'),
(55, 56, '184,158,115,127,126,94,167'),
(56, 57, '109,97,96,68,44,148,26,177,54,82,84'),
(57, 57, '84,54,148,44,68,96,97,109'),
(58, 57, '109,97,96,68,44,148,177,54,82,84'),
(59, 57, '84,54,148,26,44,68,96,97,109'),
(60, 58, '84,189,176,141,137,106,151'),
(61, 59, '84,54,148,184,108,158'),
(62, 59, '158,108,184,109,96,148,54,82,84'),
(63, 60, '84,82,54,148,44,68,96,97,109,183,184'),
(64, 60, '184,183,109,97,96,68,44,148,54,82,84'),
(65, 21, '151,150,137,176,54,82,80');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `IDUser` int(11) NOT NULL AUTO_INCREMENT,
  `Level` tinyint(1) NOT NULL DEFAULT '0',
  `DateRegistrasi` datetime DEFAULT NULL,
  `NamaUser` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `NomorHandphone` varchar(20) DEFAULT NULL,
  `Alamat` text,
  `Password` varchar(40) DEFAULT NULL,
  `HashedPassword` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`IDUser`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`IDUser`, `Level`, `DateRegistrasi`, `NamaUser`, `Email`, `NomorHandphone`, `Alamat`, `Password`, `HashedPassword`) VALUES
(2, 0, '2013-11-10 12:14:54', 'Rizki Aprilian', 'rizki.aprilian@kiranatama.com', '2823924827', 'sekelimus', 'pepedan', '205a26b1533645c87294a643e84c6a5c660e60ca'),
(3, 1, '2013-11-01 14:32:32', 'Administrator', 'admin@localhost.loc', '822988838383', 'js', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997'),
(4, 0, '2013-12-02 04:09:48', 'sss', 'rrrr@yahoo.com', '087822234223', 'sssss', '12345', '8cb2237d0679ca88db6464eac60da96345513964');
