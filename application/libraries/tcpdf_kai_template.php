<?php

class Tcpdf_kai_template {
	
	function template_pemesanan($params){

		$daftar_penumpang = '';
		$iter = 0;
		foreach($params['penumpang'] as $penumpang){
			$daftar_penumpang .= '<tr>
				<td style="padding:7px 20px;font-size:12px;">'.$penumpang->NamaPenumpang.'</td>
				<td style="padding:7px 20px;font-size:12px;">'.$penumpang->IDPengenal.'</td>
				<td style="padding:7px 20px;font-size:12px;">'.$penumpang->Kursi.'</td>';

			if($iter == 0){
				$daftar_penumpang .= '<td rowspan="1" style="padding:7px 20px;">
					<div style="font-size:12px;">'.$params['pemesan']->NamaUser.'</div>
					<div style="font-size:12px;">'.$params['pemesan']->Email.'</div>
					<div style="font-size:12px;">'.$params['pemesan']->NomorHandphone.'</div>
					<div style="font-size:12px;">'.$params['pemesan']->Alamat.'</div>
				</td>
				</tr>';
			}
		}

		if(isset($params['verified'])){
			$title = '<div>
				<div style="text-align:right"><img src="'.$params['verified'].'"></div>
				<h1 style="float:left;">Reservasi Tiket Online - Confirmed</h1>
				
			</div>';
			$max_bayar = NULL;
			$max_bayar_title = NULL;
		}
		else
		{
			$title = '<h1>Reservasi Tiket Online - Invoice</h1>';
			$max_bayar = '<span style="color:#FF0000">'.$params['max_bayar'].'</span>';
			$max_bayar_title = 'Batas Waktu Pembayaran';
		}
			
		$template = 
		$title.'<h3 style="font-size:14px">'.$params['kode'].'</h3>
		<div>
			<table align="center" style="width:100%;">
				<tbody>
					<tr>
						<td class="center" colspan="3"><div style="font-size:14px;background-color:#CCCCCC;width:100%;padding:5px 0">Kode Pemesanan</div></td>
					</tr>
					<tr>
						<td colspan="3">
							<table style="width:100%;">
								<tbody>
									<tr>
										<td style="padding:7px 20px;width:50%;font-size:12px;font-weight:900;">Kode Pembayaran</td>
										<td style="padding:7px 20px;width:50%;font-size:12px;font-weight:900;">'.$max_bayar_title.'</td>
									</tr>
									<tr>
										<td style="padding:7px 20px;font-size:12px;font-weight:900;">'.$params['kode'].'</td>
										<td style="padding:7px 20px;font-size:12px;font-weight:900;">
											'.$max_bayar.'
										</td>
									</tr>
								</tbody>
							</table>
						</td>
						</tr>
						<tr>
							<td class="center" colspan="3"><div style="font-size:14px;background-color:#CCCCCC;width:100%;padding:5px 0">Info Perjalanan</div></td>
						</tr>
						<tr>
							<td colspan="3">
								<table style="width:100%">
									<tbody>
										<tr>
											<td style="font-size:12px;padding:7px 20px;font-weight:900">Tanggal</td>
											<td style="font-size:12px;padding:7px 20px;font-weight:900">No. Kereta</td>
											<td style="font-size:12px;padding:7px 20px;font-weight:900">Nama Kereta</td>
											<td style="font-size:12px;padding:7px 20px;font-weight:900">Berangkat</td>
											<td style="font-size:12px;padding:7px 20px;font-weight:900">Tiba</td>
										</tr>
										<tr>
											<td style="font-size:12px;padding:7px 20px;">'.$params['tanggal_berangkat'].'</td>
											<td style="font-size:12px;padding:7px 20px;">'.$params['nomor_ka'].'</td>
											<td style="font-size:12px;padding:7px 20px;">'.$params['nama_ka'].'</td>
											<td style="font-size:12px;padding:7px 20px;">
												<div style="font-size:12px;">'.$params['berangkat'].'</div>
												<div style="font-size:12px;">'.$params['dari'].'</div>
											</td>
											<td style="padding:7px 20px;">
												<div style="font-size:12px;">'.$params['tiba'].'</div>
												<div style="font-size:12px;">'.$params['tujuan'].'</div>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td class="center" colspan="3"><div style="font-size:14px;background-color:#CCCCCC;width:100%;padding:5px 0">Info Data Penumpang</div></td>
						</tr>
						<tr>
							<td colspan="3">
								<table style="width:100%">
									<tbody>
										<tr>
											<td class="strong" style="font-size:12px;padding:7px 20px;">Nama Penumpang</td>
											<td class="strong" style="font-size:12px;padding:7px 20px;">No. Identitas</td>
											<td class="strong" style="font-size:12px;padding:7px 20px;">No. Kursi</td>
											<td class="strong" style="font-size:12px;padding:7px 20px;">Kontak</td>
										</tr>
										'.$daftar_penumpang.'
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td class="center" colspan="3"><div style="font-size:14px;background-color:#CCCCCC;width:100%;padding:5px 0">Info harga</div></td>
						</tr>
						<tr>
							<td colspan="3">
								<table style="width:100%">
									<tbody>
										<tr>
											<td style="font-size:12px;padding:7px 20px" class="strong">Jenis Penumpang</td>
											<td style="font-size:12px;padding:7px 20px" class="strong">Jumlah Penumpang</td>
											<td style="font-size:12px;padding:7px 20px" class="strong">Harga Satuan</td>
											<td style="font-size:12px;padding:7px 20px" class="strong">Total Harga</td>
										</tr>
										<tr>
											<td style="font-size:12px;padding:7px 20px">Dewasa</td>
											<td style="font-size:12px;padding:7px 20px">'.number_format($params['total_penumpang']).'</td>
											<td style="font-size:12px;padding:7px 20px">Rp '.number_format($params['satuan_harga']).'</td>
											<td style="font-size:12px;padding:7px 20px" class="right">Rp '.number_format($params['total_harga']).'</td>
										</tr>
										<tr>
											<td style="font-size:12px;padding:7px 20px">Biaya Layanan Pelanggan</td>
											<td style="font-size:12px;padding:7px 20px"></td>
											<td style="font-size:12px;padding:7px 20px">Rp. 7,500</td>
											<td style="font-size:12px;padding:7px 20px" class="right">Rp. 7,500</td>
										</tr>
										<tr>
											<td style="font-size:12px;padding:7px 20px" class="right strong" colspan="3">Total</td>
											<td style="font-size:12px;padding:7px 20px" class="right strong">Rp. '.number_format($params['total_harga'] + 7500).'</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td class="center" colspan="3"><div style="font-size:14px;background-color:#CCCCCC;width:100%;padding:5px 0">QR Code</div></td>
						</tr>
						<tr>
							<td colspan="3"><div style="text-align:center"><img src="'.$params['qrcode'].'" alt="qrcode"></div></td>
						</tr>
					</tbody>
				</table>
		</div>
		';

		return $template;
	}

	function template_tiket($params){

		$template = 
		'<h1>Hell to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
		<i>This is the first example of TCPDF library.</i>
		<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
		<p>Please check the source code documentation and other examples for further information.</p>
		<p style="color:#CC0000;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
		';

		return $template;

	}
}