<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Core_controller.php');

class Dashboard extends Core_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Member_model');
	}

	public function index(){

		$this->secure_area();
		$data = $this->session();

		$this->load->view('dashboard',$data);

	}

	public function logout(){
		$this->session->unset_userdata('logged_in_psi');
		session_unset();
		session_destroy();
		
		$this->session->sess_destroy();
		redirect('/login');
	}

}