<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Core_controller.php');

class Order extends Core_controller {

	const VERTICAL_BLOCK_EKONOMI = 5;
	const HORIZONTAL_BLOCK = 10;

	var $hashed_map;

	public function __construct(){
		parent::__construct();
		$this->load->model('Booking_model');
		$this->load->model('Member_model');
	}

	private function check_availability($params){
		$kelas = $params['kelas'];
		$jmlgerbong = $params['jmlgerbong'];
		$jmlpenumpang = $params['jmlpenumpang'];

		$idjadwal = $params['idjadwal'];
		$idkereta = $params['idkeretaapi'];

		$this->Booking_model->set_table('pemberangkatan');
		$seat_booked = $this->Booking_model->count_seat_booked(array('IDJadwal' => $idjadwal, 'IDKeretaApi' => $idkereta, 'PemberangkatanTanggal' => $this->session->userdata('tanggal_berangkat')));

		$all_seats_per_gerbong = ($kelas == 'ekonomi') ? self::VERTICAL_BLOCK_EKONOMI * self::HORIZONTAL_BLOCK : (self::VERTICAL_BLOCK_EKONOMI - 1) * self::HORIZONTAL_BLOCK;

		$all_seats = ($all_seats_per_gerbong * $params['jmlgerbong']);

		if(($seat_booked + $jmlpenumpang) > $all_seats)
		{
			/* kursi tidak tersedia utk {x} jumlah penumpang */
			return true;
		}
		else
		{
			//
			return false;
		}
		
	}

	public function info_jadwal(){
		
		$sql_date = $this->date_to_sql($this->input->post('date'));

		if(strtotime(date('Y-m-d') ."+1 days") >= strtotime($sql_date)){
			$this->session->set_flashdata('tanggal','error');
			redirect('/');
		}


		$request = array(
			'from' => $this->input->post('stasiun-from'),
			'to' =>$this->input->post('stasiun-to'),
			'date' => $sql_date,
			'kelas' => $this->input->post('kelas'),
			'total_penumpang' => $this->input->post('jumlah_pemesan')
		);

		$this->session->set_userdata('user_input',$request);

		if($this->input->post('stasiun-from') == $this->input->post('stasiun-to')){
			$this->session->set_flashdata('stasiun','error');
			redirect('/');
		}

		$this->Booking_model->set_table('data_stasiun');
		$stasiun_from_detail = $this->Booking_model->fetch_row(NULL,array('NamaStasiun' => $request['from']));
		$stasiun_to_detail = $this->Booking_model->fetch_row(NULL,array('NamaStasiun' => $request['to']));

		$this->Booking_model->set_table('jadwal_berangkat');
		$jadwal_detail = $this->Booking_model->fetch_rows(NULL,array('Dari' => $stasiun_from_detail->IDStasiun));

		$this->Booking_model->set_table('rute_kereta_api');
		$query = $this->db->query("SELECT r.Rute,r.IDKeretaApi,k.NamaKeretaApi,k.Kelas,k.JumlahGerbong FROM rute_kereta_api r, kereta_api k, jadwal_berangkat j WHERE
			FIND_IN_SET($stasiun_to_detail->IDStasiun,r.Rute) AND FIND_IN_SET($stasiun_from_detail->IDStasiun,r.Rute) 
			AND k.Kelas = '$request[kelas]' AND r.IDKeretaApi = k.IDKeretaApi GROUP BY k.IDKeretaApi");
		$id_kereta_api = $query->result();

		
		$check_point = $this->check_point();
		$tarif = 0;


		foreach($id_kereta_api as $row){

			$count_check_point = 0;
			$rute = explode(',', $row->Rute);
			foreach($rute as $rute_elem){
				if(in_array($rute_elem, $check_point)){
					$count_check_point++;
				}
			}

			if($row->Kelas == 'ekonomi'){
				$row->Price = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_EKONOMI;
			} elseif ($row->Kelas == 'bisnis'){
				$row->Price = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_BISNIS;
			} elseif ($row->Kelas == 'eksekutif'){
				$row->Price = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_EKSEKUTIF;
			}
			$tarif = $row->Price;
			
			$this->Booking_model->set_table('jadwal_berangkat');
			$row->Jadwal = $this->Booking_model->custom_rows(NULL,"Dari = '$stasiun_from_detail->IDStasiun' AND IDKeretaApi = '$row->IDKeretaApi'");
			
			foreach($row->Jadwal as $jadwal){
				
				$check_availability_params = array(
					'kelas' => $row->Kelas,
					'jmlgerbong' => $row->JumlahGerbong,
					'jmlpenumpang' => $request['total_penumpang'],
					'idjadwal' => $jadwal->IDJadwal,
					'idkeretaapi' => $jadwal->IDKeretaApi
				);

				$jadwal->FullyBooked = $this->check_availability($check_availability_params);

			}

		}


		$data['tanggal_berangkat'] = $request['date'];
		$data['jadwal'] = $id_kereta_api;

		$data['dari'] = $request['from'];
		$data['to'] = $request['to'];
		$data['tanggal'] = $this->input->post('date');
		$data['jumlah_kereta'] = sizeof($id_kereta_api);

		$this->session->set_userdata('tanggal_berangkat',$request['date']);
		$this->session->set_userdata('tarif',$tarif);
		$this->load->view('info-jadwal',$data);

	}


	public function agreement($idjadwal=NULL){
		
		if(!isset($idjadwal)){
			redirect('/');
		}

		if(!$this->session->userdata('tanggal_berangkat')){
			redirect('/');
		}

		$request = $this->session->userdata('user_input');

		$this->Booking_model->set_table('jadwal_berangkat');
		$detail_jadwal = $this->Booking_model->fetch_row(NULL,array('IDJadwal' => $idjadwal));
		$data['detail_jadwal'] = $detail_jadwal;

		$this->Booking_model->set_table('data_stasiun');
		$data['stasiun_from'] = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $detail_jadwal->Dari));
		$data['stasiun_to'] = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $detail_jadwal->Tujuan));

		$data['id_jadwal'] = $idjadwal;

		$data['tanggal_berangkat'] = $this->session->userdata('tanggal_berangkat');

		$data['total_penumpang'] = $request['total_penumpang'];

		$this->Booking_model->set_table('kereta_api');
		$data['kereta_api'] = $this->Booking_model->fetch_row(NULL,array('IDKeretaApi' => $detail_jadwal->IDKeretaApi));
		$this->load->view('user-aggreement',$data);

	}

	public function register($idjadwal=NULL){

		
		if(!isset($idjadwal)){
			redirect('/');
		}

		if(!$this->session->userdata('tanggal_berangkat')){
			redirect('/');
		}

		$request = $this->session->userdata('user_input');
		$data['total_penumpang'] = $request['total_penumpang'];

		$this->Booking_model->set_table('jadwal_berangkat');
		$detail_jadwal = $this->Booking_model->fetch_row(NULL,array('IDJadwal' => $idjadwal));
		if(empty($detail_jadwal)){
			redirect('/');
		}
		$data['jadwal'] = $detail_jadwal;

		$this->Booking_model->set_table('data_stasiun');
		$detail_from = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $detail_jadwal->Dari));
		$detail_to = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $detail_jadwal->Tujuan));
		$data['from'] = $detail_from;
		$data['to'] = $detail_to;

		$this->Booking_model->set_table('kereta_api');
		$detail_kereta = $this->Booking_model->fetch_row(NULL,array('IDKeretaApi' => $detail_jadwal->IDKeretaApi));
		$data['kereta'] = $detail_kereta;

		$random_id = $this->random_id();
		$data['random_id'] = $random_id;

		$data['tanggal_berangkat'] = $this->session->userdata('tanggal_berangkat');

		$this->load->view('isi-data',$data);
	}

	public function process_pembayaran(){

		$input_jadwal = array(
			'idkeretaapi' => $this->input->post('idkeretaapi'),
			'idjadwal' => $this->input->post('idjadwal'),
			'jmlpenumpang' => $this->input->post('jmlpenumpang')
		);
		$data = array();
		$this->session->set_userdata('input_jadwal',$input_jadwal);

		$penumpang = array(
			'namapenumpang' => $this->input->post('namapenumpang'),
			'idpenumpang' => $this->input->post('idpenumpang'),
			'jeniskelamin' => $this->input->post('jeniskelamin'),
			'dob' => $this->input->post('dob')
		);
		$this->session->set_userdata('input_penumpang',$penumpang);

		$pemesan = array(
			'nama_pemesan' => $this->input->post('nama_pemesan'),
			'email_pemesan' => $this->input->post('email_pemesan'),
			'password_pemesan' => $this->input->post('password_pemesan'),
			'telp_pemesan' => $this->input->post('telp_pemesan'),
			'alamat_pemesan' => $this->input->post('alamat_pemesan')
		);
		$this->session->set_userdata('input_pemesan',$pemesan);

		$this->Booking_model->set_table('pemberangkatan');
		$latest_seat = $this->Booking_model->latest_seat(NULL,array('IDJadwal' => $input_jadwal['idjadwal'], 'IDKeretaApi' => $input_jadwal['idkeretaapi']), "AddDateTime DESC");

		$total_penumpang = $input_jadwal['jmlpenumpang'];

		$arr_get_seat = array();
		if(empty($latest_seat))
		{

			$gerbong = 1;
			$block = 'A';
			$kursi = 1;
			for($x=1;$x<=$total_penumpang;$x++){
				$arr_get_seat[$x] = $gerbong . $block . $kursi;
				$kursi++;
			}
			
		}
		else
		{
			$this->Booking_model->set_table('kereta_api');
			$detail_kereta = $this->Booking_model->fetch_row(NULL,array('IDKeretaApi' => $input_jadwal['idkeretaapi']));
			$params = array('IDJadwal' => $input_jadwal['idjadwal'], 'IDKeretaApi' => $input_jadwal['idkeretaapi'], 'kelas' => $detail_kereta->Kelas, 'max_gerbong' => $detail_kereta->JumlahGerbong, 'total_penumpang' => $total_penumpang);
			$arr_get_seat = $this->get_seat($params);
		}

		$this->session->set_userdata('tempat_duduk',$arr_get_seat);

		redirect('/order/pembayaran');
	}

	private function get_seat($params){
		$return = array();

		$hashed_map = array();

		$hashed_map = $this->mapping_kursi($params);

		$arr_seat_taken = array();
		$this->Booking_model->set_table('pemberangkatan');
		$all_seats = $this->Booking_model->all_seat(NULL,array('IDJadwal' => $params['IDJadwal'], 'IDKeretaApi' => $params['IDKeretaApi']), "AddDateTime DESC");

		$increased_index = 0;
		foreach($all_seats as $seat){
			$arr_seat_taken[] = $seat->NomorGerbong . $seat->NomorKursi;
		}

		$arr_available_seat = array();
		foreach($arr_seat_taken as $key => $seat_taken){

			if(($key = array_search($seat_taken, $hashed_map)) !== false) {
			  unset($hashed_map[$key]);
			}

		}

		$slice = array_slice($hashed_map, 0, $params['total_penumpang']);

		for($t=1;$t<=$params['total_penumpang'];$t++){
			$return[$t] = $slice[$t-1];
		}

		return $return;

	}

	public function pembayaran(){

		$date = date('Y-m-d H:i:s');

		$input_jadwal = $this->session->userdata('input_jadwal');
		$data['input_jadwal'] = $input_jadwal;


		$input_penumpang = $this->session->userdata('input_penumpang');
		$data['input_penumpang'] = $input_penumpang;

		$input_pemesan = $this->session->userdata('input_pemesan');
		$data['input_pemesan'] = $input_pemesan;

		$this->Booking_model->set_table('kereta_api');
		$detail_kereta = $this->Booking_model->fetch_row(NULL,array('IDKeretaApi' => $input_jadwal['idkeretaapi']));
		$data['detail_kereta'] = $detail_kereta;

		$data['kelas'] = $detail_kereta->Kelas;
		$data['tempat_duduk'] = $this->session->userdata('tempat_duduk');

		$this->Booking_model->set_table('jadwal_berangkat');
		$detail_jadwal = $this->Booking_model->fetch_row(NULL,array('IDJadwal' => $input_jadwal['idjadwal']));
		$data['detail_jadwal'] = $detail_jadwal;

		$this->Booking_model->set_table('data_stasiun');
		$detail_from = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $detail_jadwal->Dari));
		$data['from'] = $detail_from;

		$detail_to = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $detail_jadwal->Tujuan));
		$data['to'] = $detail_to;

		$data['namapenumpang'] = $input_penumpang['namapenumpang'];
		$data['idpenumpang'] = $input_penumpang['idpenumpang'];

		$this->load->view('pembayaran',$data);

	}

	public function discard(){
		$this->session->set_userdata('user_input','');
		$this->session->set_userdata('tanggal_berangkat','');
		$this->session->set_userdata('input_jadwal','');
		$this->session->set_userdata('input_penumpang','');
		$this->session->set_userdata('input_pemesan','');
		redirect('/');
	}

	public function konfirmasi(){

		$random_id = $this->random_id();
		$data['random_id'] = $random_id;
		$this->session->set_userdata('random_id',$random_id);
		$date = date('Y-m-d H:i:s');

		$input_jadwal = $this->session->userdata('input_jadwal');

		$this->Booking_model->set_table('jadwal_berangkat');
		$jadwal_detail = $this->Booking_model->fetch_row(NULL,array('IDJadwal' => $input_jadwal['idjadwal']));

		$input_penumpang = $this->session->userdata('input_penumpang');
		/** insert data penumpang */
		$input_penumpang = $this->session->userdata('input_penumpang');
		$arr_id_penumpang = $input_penumpang['idpenumpang'];
		$arr_jenis_kelamin = $input_penumpang['jeniskelamin'];
		$arr_dob = $input_penumpang['dob'];
		$iter = 0;
		$id_penumpang = array();
		$this->Booking_model->set_table('data_penumpang');
		foreach($input_penumpang['namapenumpang'] as $penumpang){

			$arr_insert = array(
				'NamaPenumpang' => $penumpang,
				'IDPengenal' => $arr_id_penumpang[$iter],
				'OrderID' => $random_id,
				'JenisKelamin' => $arr_jenis_kelamin[$iter],
				'DOB' => $arr_dob[$iter]
			);
			
			$id_penumpang[] = $this->Booking_model->insert($arr_insert);
			$iter++;
		}


		$input_pemesan = $this->session->userdata('input_pemesan');
		/** insert data pemesan */
		$this->Booking_model->set_table('user');
		$check_user = $this->Booking_model->fetch_row(NULL,array('Email' => $input_pemesan['email_pemesan']));

		if(!empty($check_user))
		{
			$userid = $check_user->IDUser;
		}
		else
		{

			$arr_user = array(
				'DateRegistrasi' => $date,
				'NamaUser' => $input_pemesan['nama_pemesan'],
				'Email' => $input_pemesan['email_pemesan'],
				'NomorHandphone' => $input_pemesan['telp_pemesan'],
				'Alamat' => $input_pemesan['alamat_pemesan'],
				'Password' => $input_pemesan['password_pemesan'],
				'HashedPassword' => sha1($input_pemesan['password_pemesan'])
			);
			$userid = $this->Booking_model->insert($arr_user);

		}

		$this->Booking_model->set_table('order');
		$arr_order = array(
			'OrderNumber' => $random_id,
			'TanggalOrder' => $date,
			'TanggalBerangkat' => $this->session->userdata('tanggal_berangkat'),
			'IDKeretaApi' => $input_jadwal['idkeretaapi'],
			'IDAsal' => $jadwal_detail->Dari,
			'IDTujuan' => $jadwal_detail->Tujuan,
			'IDUser' => $userid
		);
		$this->Booking_model->insert($arr_order);

		$this->Booking_model->set_table('pemberangkatan');

		$count = 1;
		$sess_kursi = $this->session->userdata('tempat_duduk');
		foreach($input_penumpang['namapenumpang'] as $penumpang){
			$arr_pemberangkatan = array(
				'IDJadwal' => $input_jadwal['idjadwal'],
				'IDKeretaApi' => $input_jadwal['idkeretaapi'],
				'AddDateTime' => date('Y-m-d H:i:s'),
				'PemberangkatanTanggal' => $this->session->userdata('tanggal_berangkat'),
				'NomorGerbong' => substr($sess_kursi[$count],0,1),
				'NomorKursi' => substr($sess_kursi[$count],1,2),
				'IDPenumpang' => $id_penumpang[$count-1]
			);

			$this->Booking_model->insert($arr_pemberangkatan);
			$count++;
		}

		redirect('/order/confirm/');
		
	}

	public function confirm(){
		
		$random_id = $this->session->userdata('random_id');
		$data['random_id'] = $random_id;

		$this->Booking_model->set_table('order');
		$order_detail = $this->Booking_model->fetch_row(NULL,array('OrderNumber' => $random_id));
		$data['order_detail'] = $order_detail;

		$this->Booking_model->set_table('jadwal_berangkat');
		$detail_jadwal = $this->Booking_model->fetch_row(NULL,array('IDKeretaApi' => $order_detail->IDKeretaApi, 'Dari' => $order_detail->IDAsal, 'Tujuan' => $order_detail->IDTujuan));
		$data['detail_jadwal'] = $detail_jadwal;

		$this->Booking_model->set_table('data_stasiun');
		$from = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $detail_jadwal->Dari));
		$data['from'] = $from;

		$to = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $detail_jadwal->Tujuan));
		$data['to'] = $to;

		$this->Booking_model->set_table('kereta_api');
		$data['detail_kereta'] = $this->Booking_model->fetch_row(NULL,array('IDKeretaApi' => $order_detail->IDKeretaApi));

		$this->Booking_model->set_table('data_penumpang');
		$data['penumpang'] = $this->Booking_model->fetch_rows(NULL,array('OrderID' => $random_id));

		$this->Booking_model->set_table('user');
		$data['pemesan'] = $this->Booking_model->fetch_row(NULL,array('IDUser' => $order_detail->IDUser));

		$arr_kursi = array();
		foreach($data['penumpang'] as $penumpang){

			$this->Booking_model->set_table('pemberangkatan');
			$row_pemberangkatan = $this->Booking_model->fetch_row(NULL,array('IDPenumpang' => $penumpang->IDPenumpang));
			
			$arr_kursi[] = $row_pemberangkatan->NomorGerbong . $row_pemberangkatan->NomorKursi;
		}
		$data['kursi'] = $arr_kursi;

		$this->load->view('konfirmasi',$data);
	}

	public function get_info_kursi_on_hover($kodekursi=NULL,$id_kereta_api=NULL,$edit_id_penumpang=NULL){
		
		if(!isset($kodekursi) || !isset($id_kereta_api)){
			exit();
		}

		$kodekursi = strtoupper($kodekursi);
		$nomer_gerbong = substr($kodekursi,0,1);
		
		$nomer_kursi = substr($kodekursi,1,3);

		$this->Booking_model->set_table('pemberangkatan');
		$row_pemberangkatan = $this->Booking_model->fetch_row(NULL,array('IDKeretaApi' => $id_kereta_api, 'NomorGerbong' => $nomer_gerbong, 'NomorKursi' => $nomer_kursi));

		if(empty($row_pemberangkatan)){

			$data['edit_id_penumpang'] = $edit_id_penumpang;
			$this->load->view('info_kursi_on_hover_pick',$data);
			
		}
		else
		{

			$idpenumpang = $row_pemberangkatan->IDPenumpang;

			$this->Booking_model->set_table('data_penumpang');
			$detail_penumpang = $this->Booking_model->fetch_row(NULL,array('IDPenumpang' => $idpenumpang));

			$this->Booking_model->set_table('pemberangkatan');
			$detail_penumpang->Pemberangkatan = $this->Booking_model->fetch_row(NULL,array('IDPenumpang' => $idpenumpang));

			/** mendapatkan umur penumpang dari data DOB */
			if($detail_penumpang->DOB != ''){
				$arr_dob = explode("-",$detail_penumpang->DOB);
				$birthDate = $arr_dob[1]."/".$arr_dob[2]."/".$arr_dob[0];
		    $birthDate = explode("/", $birthDate);
				$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y")-$birthDate[2])-1):(date("Y")-$birthDate[2]));
				$detail_penumpang->Age = $age;
			}
			

			$data['detail'] = $detail_penumpang;
			
			$this->load->view('info_kursi_on_hover',$data);

		}

	}

	public function ajax_edit_kursi($id_penumpang=NULL,$seat_pick=NULL){

		$response = array();
		if(!isset($id_penumpang) || !isset($seat_pick)){
			$response['success'] = 'failed';
			echo json_encode($response);
			exit();
		}

		$seat_pick = strtoupper($seat_pick);

		$nomer_gerbong = substr($seat_pick,0,1);
		$nomer_kursi = substr($seat_pick,1,3);

		$this->Booking_model->set_table('pemberangkatan');

		$data = array(
			'NomorGerbong' => $nomer_gerbong,
			'NomorKursi' => $nomer_kursi
		);
		$this->Booking_model->update($data,array('IDPenumpang' => $id_penumpang));
		$response["success"] = "success";

		echo json_encode($response);

	}

}
