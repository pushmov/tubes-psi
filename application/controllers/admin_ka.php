<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_controller.php');

class Admin_ka extends Admin_controller {

	const ITEM_PER_PAGE = 10;

	public function __construct(){
		parent::__construct();
		$this->load->model('Admin_model');
		$this->admin_area();
	}

	public function index($page=0){
		$data = $this->admin_session();

		$this->Admin_model->set_table('kereta_api');
		$all_result = $this->Admin_model->fetch_ka();
		$result = $this->Admin_model->fetch_ka(NULL,self::ITEM_PER_PAGE,$page);


		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'admin_ka/index/';
		$config['total_rows'] = count($all_result);
		$config['per_page'] = self::ITEM_PER_PAGE;
		$config['cur_tag_open'] = '<span class="page active">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '';
		$config['prev_page'] = '';
		
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();


		$data['result'] = $result;
		$this->load->view('admin/ka_index',$data);
	}

	public function edit($id=NULL){
		if(!isset($id)){
			redirect('admin_ka/index');
		}

		$data = $this->admin_session();
		$this->Admin_model->set_table('kereta_api');
		$row = $this->Admin_model->fetch_row(NULL,array('IDKeretaApi' => $id));

		if(empty($row)){
			redirect('admin_ka/index');
		}

		if($this->input->post('nama')){
			$request = array(
				'NamaKeretaApi' => stripslashes($this->input->post('nama')),
				'Kelas' => $this->input->post('kelas'),
				'JumlahGerbong' => $this->input->post('jmlgerbong')
			);

			$this->Admin_model->set_table('kereta_api');
			$this->Admin_model->update($request,array('IDKeretaApi' => $id));

			$this->session->set_flashdata('message','Data berhasil di edit');
			redirect('admin_ka/index');
		}

		$data['row'] = $row;
		$this->load->view('admin/ka_edit',$data);
	}

	public function add(){
		$data = $this->admin_session();

		if($this->input->post('nama')){
			$request = array(
				'NamaKeretaApi' => stripslashes($this->input->post('nama')),
				'Kelas' => $this->input->post('kelas'),
				'JumlahGerbong' => $this->input->post('jmlgerbong')
			);

			$this->Admin_model->set_table('kereta_api');
			$this->Admin_model->insert($request);
			$this->session->set_flashdata('message','Data berhasil ditambah.');
			redirect('admin_ka/index');
		}

		$this->load->view('admin/ka_add',$data);
	}

	public function search($params=NULL){
		$data = $this->admin_session();

		$params = urldecode($params);

		$this->Admin_model->set_table('kereta_api');
		$row = $this->Admin_model->fetch_row(NULL,array('NamaKeretaApi' => $params));
		$data['row'] = $row;
		$this->load->view('admin/ka_search',$data);
	}

	public function delete($id=NULL){

		if(!isset($id)){
			redirect('admin_ka/index');
		}

		$this->Admin_model->set_table('kereta_api');
		$this->Admin_model->delete(array('IDKeretaApi' => $id));


		$this->Admin_model->set_table('rute_kereta_api');
		$this->Admin_model->delete(array('IDKeretaApi' => id));

		$this->session->set_flashdata('message','Data berhasil di delete');
		redirect('admin_ka/index');

	}

}