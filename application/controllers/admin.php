<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_controller.php');

class Admin extends Admin_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Member_model');
	}

	public function index(){
		$this->admin_area();
		redirect('/admin_dashboard/');
	}

	public function validate(){

		if(!$this->input->post('username') || !$this->input->post('password')){
			redirect('/admin/login/');
		}

		$request = array(
			'Email' => $this->input->post('username'),
			'Password' => $this->input->post('password'),
			'HashedPassword' => sha1($this->input->post('password')),
			'Level' => 1
		);

		$this->Member_model->set_table('user');
		$row = $this->Member_model->fetch_row(NULL,$request);

		if(empty($row)){
			$this->session->set_flashdata('message','Invalid email or password');
			$this->session->set_flashdata('error','error');
			redirect('/admin/login');
		}

		$admin_session = array(
			'session' => sha1(date('Y-m-d H:i:s')),
			'memberid' => $row->IDUser,
			'email' => $row->Email
		);
		$this->session->set_userdata('admin_psi', $admin_session);

		redirect('/admin_dashboard');

	}

	public function login(){

		$this->load->view('/admin/login');

	}

	public function logout(){
		$this->session->unset_userdata('admin_psi');
		session_unset();
		session_destroy();
		
		$this->session->sess_destroy();
		redirect('/admin/login/');
	}

}