<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends Core_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Booking_model');
	}

	public function autosuggest($input=NULL){
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Cache-Control: no-cache");
		header("Pragma: no-cache");
		header("Content-type: text/xml");
		
		$input = urldecode($input);

		if($input == '' || !isset($input))
		{
			echo NULL;
			exit();
		}
		else
		{


			$this->Booking_model->set_table('data_stasiun');
			$result = $this->Booking_model->search_stasiun($input);
			
			if(!empty($result)){
				echo "<response>";
				foreach ($result as $key => $value) {
					echo "<keywords>" . $value->NamaStasiun . ", " . $value->Kota . "</keywords>";
				}
				echo "</response>";
				
			}
			
		}
	}

	public function index()
	{
		$query = $this->db->query("SELECT d.Kota,d.NamaStasiun,d.Kota,j.IDJadwal,j.JamBerangkat,j.JamSampai,j.Tujuan FROM data_stasiun d, jadwal_berangkat j 
			WHERE j.Dari = d.IDStasiun ORDER BY RAND(),j.JamBerangkat ASC LIMIT 0,6");
		$kota = $query->result();

		foreach($kota as $row){
			$this->Booking_model->set_table('data_stasiun');
			$row->Tujuan = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $row->Tujuan));
		}
		$data['kota'] = $kota;
		
		$this->load->view('index',$data);
	}


	public function gallery(){

		$this->load->view('gallery');
	}

	public function layanan_produk(){
		$this->load->view('layanan-produk');
	}

	public function ajax_get_stasiun($kota=NULL){

		
		$this->Booking_model->set_table('data_stasiun');
		$result = $this->Booking_model->fetch_rows(NULL,array('Kota' => $kota));
		$data['list_stasiun'] = $result;
		$this->load->view('ajaxresponse/liststasiun',$data);

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */