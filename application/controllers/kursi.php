<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Core_controller.php');

class Kursi extends Core_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Booking_model');
	}

	public function editkursi($idpengenal=NULL,$person=NULL){


		if(!isset($idpengenal)){
			redirect('/order/pembayaran');
		}


		$tanggal_berangkat = $this->session->userdata('tanggal_berangkat');

		$inputjadwal = $this->session->userdata('input_jadwal');
		$idjadwal = $inputjadwal['idjadwal'];

		$this->Booking_model->set_table('jadwal_berangkat');
		$detail_jadwal = $this->Booking_model->fetch_row(NULL,array('IDJadwal' => $idjadwal));

		$this->Booking_model->set_table('pemberangkatan');
		$results = $this->Booking_model->fetch_rows(NULL,array('PemberangkatanTanggal' => $tanggal_berangkat, 'IDJadwal' => $idjadwal, 'IDKeretaApi' => $detail_jadwal->IDKeretaApi));
		
		$this->Booking_model->set_table('kereta_api');
		$detail_kereta = $this->Booking_model->fetch_row(NULL,array('IDKeretaApi' => $detail_jadwal->IDKeretaApi));

		$taken_seat = array();
		foreach($results as $row){
			$taken_seat[] = $row->NomorGerbong . $row->NomorKursi;
		}


		$mapping_kursi = $this->mapping_kursi(array('kelas' => $detail_kereta->Kelas, 'max_gerbong' => $detail_kereta->JumlahGerbong));

		$data['jumlahgerbong'] = $detail_kereta->JumlahGerbong;
		$data['mapping_kursi'] = $mapping_kursi;
		$data['taken_seat'] = $taken_seat;

		$data['idkeretaapi'] = $detail_jadwal->IDKeretaApi;
		
		$data['kelas'] = $detail_kereta->Kelas;
		$data['block_vertical'] = ($detail_kereta->Kelas == 'ekonomi') ? 5 : 4;
		$data['max_range'] = ($detail_kereta->Kelas == 'ekonomi') ? 'E' : 'D';
		$data['block_horisontal'] = 10;

		$my_kursi = $this->session->userdata('tempat_duduk');
		$data['my_kursi'] = $my_kursi[$person];

		$data['person'] = $person;
		$data['idpengenal'] = $idpengenal;

		$kursi_sess = $this->session->userdata('tempat_duduk');
		$modified_kursi = array();
		$iter = 0;
		foreach($kursi_sess as $kursi){
			$modified_kursi[$iter] = (substr($kursi, 0, 2) > 10) ? substr($kursi, 1, 2) : substr($kursi, 0, 2);
			$iter++;
		}

		$data['tempat_duduk'] = $kursi_sess;
		
		$this->load->view('editkursi',$data);

	}

	public function updatekursi($idperson=NULL,$person=NULL,$kursi=NULL){

		if(!isset($idperson) || !isset($person) || !isset($kursi)){
			redirect('/');
		}

		$newkursi = strtoupper($kursi);
		$sess_kursi = $this->session->userdata('tempat_duduk');
		$sess_kursi[$person] = $newkursi;

		$this->session->set_userdata('tempat_duduk',$sess_kursi);
		
		redirect('/order/pembayaran');

	}

}