<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Core_controller.php');

class Berita extends Core_controller {

	const ITEM_PER_PAGE = 10;

	public function __construct(){
		parent::__construct();
		$this->load->model('Berita_model');
	}

	public function index(){
		redirect('/berita/list/');
	}

	public function list($page=0){

		$data = $this->session();

		$this->Berita_model->set_table('news');
		$jml_berita = $this->Berita_model->jumlah_berita();

		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'berita/page';
		$config['total_rows'] = $jml_berita;
		$config['per_page'] = self::ITEM_PER_PAGE;
		$config['cur_tag_open'] = '<span class="yellow" style="margin:0 10px">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '&laquo;';
		$config['prev_page'] = '&raquo;';
		
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();


		$list_berita = $this->Berita_model->getlist(NULL,NULL,self::ITEM_PER_PAGE,$page);

		print_r($list_berita);
		exit();

	}

	public function detail(){

	}

	public function insert(){
		
	}

}