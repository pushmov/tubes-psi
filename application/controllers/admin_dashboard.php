<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_controller.php');

class Admin_dashboard extends Admin_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Admin_model');
		$this->admin_area();
	}

	public function index(){
		$data = $this->admin_session();

		$this->Admin_model->set_table('order');

		$data['total_order'] = $this->Admin_model->count_rows();
		$data['pending_order'] = $this->Admin_model->count_rows(array('IsVerified' => 0));

		$this->Admin_model->set_table('kereta_api');
		$data['total_kereta'] = $this->Admin_model->count_rows();

		$this->Admin_model->set_table('data_stasiun');
		$data['total_stasiun'] = $this->Admin_model->count_rows();

		$this->Admin_model->set_table('rute_kereta_api');
		$data['total_rute'] = $this->Admin_model->count_rows();

		$this->Admin_model->set_table('jadwal_berangkat');
		$data['total_jadwal'] = $this->Admin_model->count_rows();

		$this->load->view('admin/dashboard',$data);
	}


}