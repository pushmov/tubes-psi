<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Core_controller.php');

class Member extends Core_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Member_model');
	}

	public function checkemail($email=NULL){

		$response = array();

		if(!isset($email)){
			$response['success'] = 'fail';
			echo json_encode($response);
			exit();
		}

		$this->Member_model->set_table('user');
		$check_email = $this->Member_model->fetch_row(NULL,array('Email' => urldecode($email)));

		if(!empty($check_email))
		{
			$response['success'] = 'registered';
			$response['password'] = $check_email->Password;
		}
		else
		{
			$response['success'] = 'empty';
		}

		echo json_encode($response);

	}
}