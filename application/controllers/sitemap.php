<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Core_controller.php');

class Sitemap extends Core_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Member_model');
	}

	public function index(){
		$data = $this->session();
		$this->load->view('sitemap',$data);
	}

}