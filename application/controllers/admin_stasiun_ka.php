<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_controller.php');

class Admin_stasiun_ka extends Admin_controller {

	const ITEM_PER_PAGE = 10;

	public function __construct(){
		parent::__construct();
		$this->load->model('Admin_model');
		$this->admin_area();
	}

	public function index($page=0){

		$data = $this->admin_session();

		$this->Admin_model->set_table('data_stasiun');
		$all_result = $this->Admin_model->fetch_ka();
		$result = $this->Admin_model->fetch_ka(NULL,self::ITEM_PER_PAGE,$page);


		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'admin_stasiun_ka/index/';
		$config['total_rows'] = count($all_result);
		$config['per_page'] = self::ITEM_PER_PAGE;
		$config['cur_tag_open'] = '<span class="page active">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '';
		$config['prev_page'] = '';
		
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();

		$data['result'] = $result;
		$this->load->view('admin/stasiun_index',$data);

	}

	public function edit($id=NULL){

		$data = $this->admin_session();

		if(!isset($id)){
			redirect('admin_stasiun_ka/index');
		}

		if($this->input->post('kota')){
			$request = array(
				'Kota' => stripslashes($this->input->post('kota')),
				'NamaStasiun' => stripslashes($this->input->post('nama')),
			);

			$this->Admin_model->set_table('data_stasiun');
			$this->Admin_model->update($request,array('IDStasiun' => $id));
			$this->session->set_flashdata('message','Record berhasil di update');
			redirect('admin_stasiun_ka/index');
		}

		$this->Admin_model->set_table('data_stasiun');
		$row = $this->Admin_model->fetch_row(NULL,array('IDStasiun' => $id));
		if(empty($id))
		{
			redirect('admin_stasiun_ka/index');
		}

		$data['detail'] = $row;
		$this->load->view('admin/stasiun_edit',$data);

	}

	public function add(){

		$data = $this->admin_session();
		if($this->input->post('nama')){

			$request = array(
				'Kota' => $this->input->post('kota'),
				'NamaStasiun' => $this->input->post('nama')
			);

			$this->Admin_model->set_table('data_stasiun');
			$this->Admin_model->insert($request);

			$this->session->set_flashdata('message','Data berhasil ditambah');
			redirect('admin_stasiun_ka/index');

		}


		$this->load->view('admin/stasiun_add',$data);

	}

	public function delete($id=NULL){
		if(!isset($id)){
			redirect('admin_stasiun_ka/index');
		}

		$this->Admin_model->set_table('data_stasiun');
		$this->Admin_model->delete(array('IDStasiun' => $id));

		$this->Admin_model->set_table('rute_kereta_api');
		$this->Admin_model->custom_delete("FIND_IN_SET($id,Rute)");

		$this->session->set_flashdata('message','Data berhasil di delete');
		redirect('admin_stasiun_ka/index');
	}

	public function search($params=NULL){

		$params = urldecode($params);
		$this->Admin_model->set_table('data_stasiun');
		$row = $this->Admin_model->fetch_row(NULL,array('NamaStasiun' => $params));

		$data['row'] = $row;
		$this->load->view('admin/stasiun_search',$data);
	}

}