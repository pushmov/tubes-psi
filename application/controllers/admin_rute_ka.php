<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_controller.php');

class Admin_rute_ka extends Admin_controller {

	const ITEM_PER_PAGE = 10;

	public function __construct(){
		parent::__construct();
		$this->load->model('Admin_model');
		$this->admin_area();
	}

	public function index($page=0){

		$data = $this->admin_session();

		$this->Admin_model->set_table('rute_kereta_api');
		$all_result = $this->Admin_model->fetch_ka();

		$where = array(
			'tablejoin' => "kereta_api",
			'where2' => "kereta_api.IDKeretaApi = rute_kereta_api.IDKeretaApi",
			'limit' => self::ITEM_PER_PAGE,
			'offset' => $page
		);

		$result = $this->Admin_model->multi_table_rows($where);

		foreach($result as $row){


			$arr_rute = explode(',', $row->Rute);
			$x = array();
			foreach($arr_rute as $rute){
				$this->Admin_model->set_table('data_stasiun');
				$xtemp = $this->Admin_model->fetch_row(NULL,array('IDStasiun' => $rute));
				$x[] = $xtemp->NamaStasiun . ' ('.$xtemp->Kota.')';
			}
			$row->TotalRute = sizeof($x);
			$row->ListRute = implode(', ', $x);
		}

		$data['result'] = $result;

		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'admin_rute_ka/index/';
		$config['total_rows'] = count($all_result);
		$config['per_page'] = self::ITEM_PER_PAGE;
		$config['cur_tag_open'] = '<span class="page active">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '';
		$config['prev_page'] = '';
		
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();

		$this->load->view('admin/rute_index',$data);
	}

	public function add(){
		$data = $this->admin_session();
	}

	public function edit($id=NULL){

		$data = $this->admin_session();
		if(!isset($id)){
			redirect('admin_rute_ka/index');
		}

		$this->Admin_model->set_table('rute_kereta_api');
		$row = $this->Admin_model->fetch_row(NULL,array('IDRoute' => $id));

		$arr_rute = explode(',', $row->Rute);
		$row->ArrRute = $arr_rute;
		$x = array();
		foreach($arr_rute as $rute){
			$this->Admin_model->set_table('data_stasiun');
			$xtemp = $this->Admin_model->fetch_row(NULL,array('IDStasiun' => $rute));
			$x[] = $xtemp->NamaStasiun . ' ('.$xtemp->Kota.')';
		}
		$row->TotalRute = sizeof($x);
		$row->ListRute = implode(', ', $x);


		$this->Admin_model->set_table('kereta_api');
		$row->Kereta = $this->Admin_model->fetch_row(NULL,array('IDKeretaApi' => $row->IDKeretaApi));

		$this->Admin_model->set_table('data_stasiun');
		$data['data_stasiun'] = $this->Admin_model->fetch_ka();

		$data['row'] = $row;
		$this->load->view('admin/rute_edit',$data);

	}

	public function search($params=NULL){

		$data = $this->admin_session();
		if(!isset($params)){
			exit();
		}

		$params = urldecode($params);
		$this->Admin_model->set_table('kereta_api');
		$kereta = $this->Admin_model->fetch_row(NULL,array('NamaKeretaApi' => $params ));

		$this->Admin_model->set_table('rute_kereta_api');
		$where = array(
			'where1' => "rute_kereta_api.IDKeretaApi = $kereta->IDKeretaApi",
			'tablejoin' => "kereta_api",
			'where2' => "kereta_api.IDKeretaApi = rute_kereta_api.IDKeretaApi"
		);

		$result = $this->Admin_model->multi_table_rows($where);

		foreach($result as $row){


			$arr_rute = explode(',', $row->Rute);
			$x = array();
			foreach($arr_rute as $rute){
				$this->Admin_model->set_table('data_stasiun');
				$xtemp = $this->Admin_model->fetch_row(NULL,array('IDStasiun' => $rute));
				$x[] = $xtemp->NamaStasiun . ' ('.$xtemp->Kota.')';
			}
			$row->TotalRute = sizeof($x);
			$row->ListRute = implode(', ', $x);
		}

		$data['result'] = $result;
		$this->load->view('admin/rute_search',$data);
	}

	public function delete($id=NULL){
		if(!isset($id)){
			redirect('admin_rute_ka/index');
		}

		$this->Admin_model->set_table('rute_kereta_api');
		$this->Admin_model->delete(array('IDRoute' => $id));
		$this->session->set_flashdata('message','Record berhasil di delete');
		redirect('admin_rute_ka/index');
	}

}