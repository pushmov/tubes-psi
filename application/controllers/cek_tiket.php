<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Core_controller.php');

class Cek_tiket extends Core_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Booking_model');
		$this->load->model('Member_model');
	}

	public function index(){

		$this->load->view('cek_tiket_index');

	}

	public function ajax_search($orderid=NULL){
		if(!isset($orderid)){
			exit();
		}

		$orderid = strtoupper($orderid);

		$this->Booking_model->set_table('order');
		$row_tiket = $this->Booking_model->fetch_row(NULL,array('OrderNumber' => $orderid));

		$data['detail'] = $row_tiket;
		$data['orderid'] = $orderid;

		$this->Booking_model->set_table('data_penumpang');
		$penumpang = $this->Booking_model->fetch_rows(NULL,array('OrderID' => $orderid));
		$data['penumpang'] = $penumpang;

		$this->Booking_model->set_table('data_stasiun');
		$asal = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $row_tiket->IDAsal));
		$tujuan = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $row_tiket->IDTujuan));
		$data['asal'] = $asal;
		$data['tujuan'] = $tujuan;

		$this->Booking_model->set_table('kereta_api');
		$kereta = $this->Booking_model->fetch_row(NULL,array('IDKeretaApi' => $row_tiket->IDKeretaApi));
		$data['kereta'] = $kereta;

		$this->Booking_model->set_table('rute_kereta_api');
		$query = $this->db->query("SELECT r.Rute,r.IDKeretaApi,k.NamaKeretaApi,k.Kelas,k.JumlahGerbong FROM rute_kereta_api r, kereta_api k, jadwal_berangkat j WHERE
			FIND_IN_SET($row_tiket->IDTujuan,r.Rute) AND FIND_IN_SET($row_tiket->IDAsal,r.Rute) 
			AND k.Kelas = '$kereta->Kelas' AND r.IDKeretaApi = k.IDKeretaApi GROUP BY k.IDKeretaApi");
		$id_kereta_api = $query->result();

		
		$check_point = $this->check_point();
		$tarif = 0;


		foreach($id_kereta_api as $row){
			$count_check_point = 0;
			$rute = explode(',', $row->Rute);
			foreach($rute as $rute_elem){
				if(in_array($rute_elem, $check_point)){
					$count_check_point++;
				}
			}

			if($kereta->Kelas == 'ekonomi'){
				$price = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_EKONOMI;
			} elseif ($kereta->Kelas == 'bisnis'){
				$price = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_BISNIS;
			} elseif ($kereta->Kelas == 'eksekutif'){
				$price = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_EKSEKUTIF;
			}
			
		}

		$data['tarif'] = $price + 7500;

		$this->load->view('cek_tiket_search',$data);

	}

}