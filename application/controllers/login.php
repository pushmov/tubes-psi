<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Core_controller.php');

class Login extends Core_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Member_model');
	}


	public function index(){

		if($this->session->userdata('logged_in_psi')){
			redirect('/dashboard/');
		}

		$data = $this->session();
		$this->load->view('login',$data);
	}

	public function verify(){

		if(!$this->input->post('email') || !$this->input->post('password')){
			redirect('/login');
		}

		$this->Member_model->set_table('user');
		$member_detail = $this->Member_model->fetch_row(NULL,
			array(
				'Email' => $this->input->post('email'), 
				'Password' => $this->input->post('password'),
				'HashedPassword' => sha1($this->input->post('password'))

			)
		);

		if(empty($member_detail)){
			$this->session->set_flashdata('message','Error : Invalid email or password.');
			$this->session->set_flashdata('error','emailnotfound');
			redirect('/login');
		}

		$session_add = array(
			'IDUser' => $member_detail->IDUser,
			'NamaUser' => $member_detail->NamaUser,
			'Email' => $member_detail->Email,
			'Alamat' => $member_detail->Alamat,
			'HashedPassword' => $member_detail->HashedPassword
		);
		$this->session->set_userdata('logged_in_psi',$session_add);

		redirect('/dashboard/');


	}


}