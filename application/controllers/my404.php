<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Core_controller.php');

class my404 extends Core_controller {

	public function __construct(){
		parent::__construct();
	}

	public function index(){

		$data = $this->session();
		$this->load->view('custom404',$data);

	}

}