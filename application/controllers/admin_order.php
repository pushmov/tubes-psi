<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_controller.php');

class Admin_order extends Admin_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Admin_model');
		$this->load->model('Booking_model');
		$this->admin_area();
	}

	public function index(){
		$data = $this->admin_session();

		$this->Admin_model->set_table('order');
		$result = $this->Admin_model->fetchall(NULL);

		foreach($result as $row){
			$this->Admin_model->set_table('kereta_api');
			$row->DetailKereta = $this->Admin_model->fetch_row(NULL,array('IDKeretaApi' => $row->IDKeretaApi));

			$this->Admin_model->set_table('data_stasiun');
			$row->DetailAsal = $this->Admin_model->fetch_row(NULL,array('IDStasiun' => $row->IDAsal));
			$row->DetailTujuan = $this->Admin_model->fetch_row(NULL,array('IDStasiun' => $row->IDTujuan));
		}

		$data['result'] = $result;

		$this->load->view('admin/order_index',$data);
	}

	public function verifikasi(){
		$data = $this->admin_session();
		$this->load->view('admin/order_verifikasi',$data);
	}

	public function verifikasi_search($params=NULL){
		if(!isset($params)){
			exit();
		}

		$this->Admin_model->set_table('order');
		$result = $this->Admin_model->fetch_rows(NULL,array('OrderNumber' => urldecode(trim($params))));

		$data_penumpang = array();
		foreach($result as $row){
			$this->Admin_model->set_table('data_penumpang');
			$data_penumpang = $this->Admin_model->fetch_rows(NULL,array('OrderID' => $row->OrderNumber));
			$row->TotalPenumpang = sizeof($data_penumpang);
		}


		$data['penumpang'] = $data_penumpang;
		$data['result'] = $result;
		$this->load->view('admin/order_verifikasi_search',$data);
	}

	public function add(){
		$data = $this->admin_session();
		$this->load->view('admin/order_add',$data);
	}

	public function tools(){
		$data = $this->admin_session();
		$this->load->view('admin/order_tools',$data);
	}

	public function search($params=NULL){
		if(!isset($params)){
			exit();
		}
		$this->Admin_model->set_table('order');
		$result = $this->Admin_model->fetch_rows(NULL,array('OrderNumber' => urldecode(trim($params))));

		foreach($result as $row){
			$this->Admin_model->set_table('kereta_api');
			$row->DetailKereta = $this->Admin_model->fetch_row(NULL,array('IDKeretaApi' => $row->IDKeretaApi));

			$this->Admin_model->set_table('data_stasiun');
			$row->DetailAsal = $this->Admin_model->fetch_row(NULL,array('IDStasiun' => $row->IDAsal));
			$row->DetailTujuan = $this->Admin_model->fetch_row(NULL,array('IDStasiun' => $row->IDTujuan));
		}

		$data['result'] = $result;

		$this->load->view('admin/order_search',$data);

	}

	public function setverified($id=NULL){
		if(!isset($id)){
			redirect('/admin_order/verifikasi/');
		}

		$this->Admin_model->set_table('order');

		$detail = $this->Admin_model->fetch_row(NULL,array('OrderID' => $id));
		$this->Admin_model->update(array('IsVerified' => 1), array('OrderID' => $id));
		$this->session->set_flashdata('message','Nomer ID '.$detail->OrderNumber.' Berhasil di update');
		redirect('/admin_order/verifikasi/');
	}

	public function edit($id=NULL){
		if(!isset($id)){
			redirect('/admin_order');
		}

		$data = $this->admin_session();

		$this->Admin_model->set_table('order');
		$row = $this->Admin_model->fetch_row(NULL,array('OrderID' => $id));

		if(empty($row)){
			redirect('/admin_order');
		}

		if($this->input->post('tanggalberangkat')){
			
			$this->Admin_model->set_table('kereta_api');
			$req_kereta = $this->Admin_model->fetch_row(NULL,array('NamaKeretaApi' => $this->input->post('keretaapi')));

			$this->Admin_model->set_table('data_stasiun');
			$req_asal = $this->Admin_model->fetch_row(NULL,array('NamaStasiun' => $this->input->post('asal')));
			$req_tujuan = $this->Admin_model->fetch_row(NULL,array('NamaStasiun' => $this->input->post('tujuan')));

			$request = array(
				'TanggalBerangkat' => $this->input->post('tanggalberangkat'),
				'IDKeretaApi' => $req_kereta->IDKeretaApi,
				'IDAsal' => $req_asal->IDStasiun,
				'IDTujuan' => $req_tujuan->IDStasiun,
				'IsVerified' => $this->input->post('status')
			);

			$this->Admin_model->set_table('order');
			$this->Admin_model->update($request,array('OrderID' => $id));
			$this->session->set_flashdata('message','Order ID '.$row->OrderNumber.' berhasil di update');
			redirect('/admin_order');

		}

		$this->Admin_model->set_table('kereta_api');
		$row->Kereta = $this->Admin_model->fetch_row(NULL,array('IDKeretaApi' => $row->IDKeretaApi));

		$this->Admin_model->set_table('data_stasiun');
		$row->Asal = $this->Admin_model->fetch_row(NULL,array('IDStasiun' => $row->IDAsal));
		$row->Tujuan = $this->Admin_model->fetch_row(NULL,array('IDStasiun' => $row->IDTujuan));

		$data['order_row'] = $row;

		$this->load->view('admin/order_edit',$data);


	}

	public function tools_search($params=NULL){
		if(!isset($params)){
			exit();
		}

		$params = strtoupper(trim(urldecode($params)));

		$this->Admin_model->set_table('order');
		$result = $this->Admin_model->fetch_rows(NULL,array('OrderNumber' => $params));

		foreach($result as $row){
			

			$this->Admin_model->set_table('data_penumpang');
			$row->Penumpang = $this->db->query("SELECT * FROM data_penumpang d, pemberangkatan p WHERE d.OrderID = '$params' AND p.IDPenumpang = d.IDPenumpang")->result();

		}

		$data['result'] = $result;

		$this->Admin_model->set_table('jadwal_berangkat');
		$row_jadwal_berangkat = $this->Admin_model->fetch_row('IDJadwal',array('IDKeretaApi' => $result[0]->IDKeretaApi, 'Dari' => $result[0]->IDAsal, 'Tujuan' => $result[0]->IDTujuan));

		$tanggal_berangkat = $result[0]->TanggalBerangkat;
		
		$this->Booking_model->set_table('pemberangkatan');
		$results = $this->Booking_model->fetch_rows(NULL,array('PemberangkatanTanggal' => $tanggal_berangkat, 'IDJadwal' => $row_jadwal_berangkat->IDJadwal, 'IDKeretaApi' => $result[0]->IDKeretaApi));

		$taken_seat = array();
		foreach($results as $seat){
			$taken_seat[] = $seat->NomorGerbong . $seat->NomorKursi;
		}
		$data['taken_seat'] = $taken_seat;

		/* data below is for seat display  */
		$this->Admin_model->set_table('kereta_api');
		$detailkereta = $this->Admin_model->fetch_row(NULL,array('IDKeretaApi' => $result[0]->IDKeretaApi));
		$data['jumlahgerbong'] = $detailkereta->JumlahGerbong;
		$data['id_kereta_api'] = $result[0]->IDKeretaApi;
		$data['kelas'] = $detailkereta->Kelas;
		$data['max_range'] = ($detailkereta->Kelas == 'ekonomi') ? 'E' : 'D';

		$data['block_vertical'] = ($detailkereta->Kelas == 'ekonomi') ? 5 : 4;
		$data['block_horisontal'] = 10;


		$this->load->view('admin/order_tools_search',$data);
	}

	public function load_tooltip(){
		$this->load->view('admin/test_tooltip');
	}

}