<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Core_controller.php');
include_once(APPPATH . 'libraries/kai_pdf_config.php');

class Pdfgenerator extends Core_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Booking_model');
		$this->load->model('Member_model');
		$this->load->library('tcpdf');
		$this->load->library('tcpdf_kai_template');
	}

	public function pemesanan($orderid=NULL,$option='I'){

		if(!isset($orderid)){
			redirect('/');
		}

		$orderid = strtoupper($orderid);
		$option = strtoupper($option);

		// set document information
		$this->tcpdf->SetCreator(PDF_CREATOR);
		$this->tcpdf->SetAuthor('Team PSI - Kereta Api Indonesia');
		$this->tcpdf->SetTitle('INVOICE');
		$this->tcpdf->SetSubject('Detail Pemesanan - '.$orderid.' - ' . date('D, d/M/Y'));

		$this->tcpdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' - ORDER ID : '.$orderid, PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
		$this->tcpdf->setFooterData(array(0,64,0), array(0,64,128));

		// set header and footer fonts
		$this->tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$this->tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$this->tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$this->tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$this->tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$this->tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$this->tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$this->tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$this->tcpdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$this->tcpdf->SetFont('dejavusans', '', 14, '', true);

		// Add a page
		// This method has several options, check the source code documentation for more information.
		$this->tcpdf->AddPage();

		// set text shadow effect
		$this->tcpdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

		// Set some content to print
		$this->Booking_model->set_table('order');
		$order_detail = $this->Booking_model->fetch_row(NULL,array('OrderNumber' => $orderid));

		$this->Booking_model->set_table('data_penumpang');
		$penumpang = $this->Booking_model->fetch_rows(NULL,array('OrderID' => $orderid));

		$this->Booking_model->set_table('kereta_api');
		$detail_kereta = $this->Booking_model->fetch_row(NULL,array('IDKeretaApi' => $order_detail->IDKeretaApi));

		$this->Booking_model->set_table('pemberangkatan');
		foreach ($penumpang as $row) {
			$pemberangkatan = $this->Booking_model->fetch_row(NULL,array('IDPenumpang' => $row->IDPenumpang, 'IDKeretaApi' => $order_detail->IDKeretaApi));
			$row->Kursi = substr(strtoupper($detail_kereta->Kelas), 0, 3) . $pemberangkatan->NomorGerbong . '-'.$pemberangkatan->NomorKursi;
		}

		$this->Booking_model->set_table('user');
		$pemesan = $this->Booking_model->fetch_row(NULL,array('IDUser' => $order_detail->IDUser));

		$this->Booking_model->set_table('jadwal_berangkat');
		$berangkat = $this->Booking_model->fetch_row(NULL,array('Dari' => $order_detail->IDAsal, 'Tujuan' => $order_detail->IDTujuan, 'IDKeretaApi' => $order_detail->IDKeretaApi));

		$this->Booking_model->set_table('data_stasiun');
		$dari = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $order_detail->IDAsal));

		$tujuan = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $order_detail->IDTujuan));


		$this->Booking_model->set_table('rute_kereta_api');
		$query = $this->db->query("SELECT r.Rute,r.IDKeretaApi,k.NamaKeretaApi,k.Kelas,k.JumlahGerbong FROM rute_kereta_api r, kereta_api k, jadwal_berangkat j WHERE
			FIND_IN_SET($order_detail->IDTujuan,r.Rute) AND FIND_IN_SET($order_detail->IDAsal,r.Rute) 
			AND k.Kelas = '$detail_kereta->Kelas' AND r.IDKeretaApi = k.IDKeretaApi GROUP BY k.IDKeretaApi");
		$id_kereta_api = $query->result();

		
		$check_point = $this->check_point();
		$tarif = 0;


		foreach($id_kereta_api as $row){
			$count_check_point = 0;
			$rute = explode(',', $row->Rute);
			foreach($rute as $rute_elem){
				if(in_array($rute_elem, $check_point)){
					$count_check_point++;
				}
			}

			if($detail_kereta->Kelas == 'ekonomi'){
				$price = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_EKONOMI;
			} elseif ($detail_kereta->Kelas == 'bisnis'){
				$price = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_BISNIS;
			} elseif ($detail_kereta->Kelas == 'eksekutif'){
				$price = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_EKSEKUTIF;
			}
			
		}


		$qr_params = array(
			'data' => $orderid,
			'level' => 'H',
			'size' => 10,
			'savename' => $orderid. '.png'
		);
		$this->generate_qr($qr_params);

		

		$tcpdf_kai_template = new Tcpdf_kai_template();
		$params = array(
			'penumpang' => $penumpang,
			'pemesan' => $pemesan,
			'kode' => $orderid,
			'max_bayar' => date('D, d/M/Y H:i:s',strtotime($order_detail->TanggalOrder."+1 days")),
			'nomor_ka' => $detail_kereta->IDKeretaApi,
			'nama_ka' => $detail_kereta->NamaKeretaApi,
			'tanggal_berangkat' => $order_detail->TanggalBerangkat,
			'berangkat' =>  ($berangkat->JamBerangkat != '') ? date('D, d/M/Y',strtotime($order_detail->TanggalBerangkat)) . ' ' . date('H:i:s',strtotime($berangkat->JamBerangkat)) : date('D, d/M/Y',strtotime($order_detail->TanggalBerangkat)) . ' N/A',
			'dari' => $dari->NamaStasiun . ', '.$dari->Kota,
			'tiba' => (strtotime($berangkat->JamSampai) < strtotime($berangkat->JamBerangkat)) ? date('D, d/M/Y',strtotime($order_detail->TanggalBerangkat . "tomorrow")) . ' ' . date('H:i:s',strtotime($berangkat->JamSampai)) : date('D, d/M/Y',strtotime($order_detail->TanggalBerangkat)) . ' ' . date('H:i:s',strtotime($berangkat->JamSampai)) ,
			'tujuan' => $tujuan->NamaStasiun . ', ' .$tujuan->Kota,
			'total_penumpang' => sizeof($penumpang),
			'satuan_harga' => $price,
			'total_harga' => sizeof($penumpang) * $price,
			'qrcode' => base_url() . 'public/images/qrcode/' . $orderid . '.png'

		);
		$html = $tcpdf_kai_template->template_pemesanan($params);

		// Print text using writeHTMLCell()
		$this->tcpdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

		// ---------------------------------------------------------

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		$this->tcpdf->Output('Bukti Pemesanan tiket - '.$orderid.'.pdf', $option);

	}

	public function generate_qr($params){
	
		if(!file_exists(PUBLICPATH.'images/qrcode/'.$params['savename'])){
			$this->load->library('ciqrcode');
			$params['savename'] = PUBLICPATH.'images/qrcode/'.$params['savename'];
			$qr_data = $this->ciqrcode->generate($params);
		}
		
	}

	public function tiket($orderid=NULL,$option='I'){
		$orderid = strtoupper($orderid);
		$option = strtoupper($option);
		
		// set document information
		$this->tcpdf->SetCreator(PDF_CREATOR);
		$this->tcpdf->SetAuthor('Team PSI - Kereta Api Indonesia');
		$this->tcpdf->SetTitle('INVOICE');
		$this->tcpdf->SetSubject('Tiket Detail');


		// set default header data
		$this->tcpdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' - ORDER ID : '.$orderid, PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
		$this->tcpdf->setFooterData(array(0,64,0), array(0,64,128));

		// set header and footer fonts
		$this->tcpdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$this->tcpdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$this->tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$this->tcpdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$this->tcpdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$this->tcpdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$this->tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$this->tcpdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		    require_once(dirname(__FILE__).'/lang/eng.php');
		    $pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$this->tcpdf->setFontSubsetting(true);

		// Set font
		// dejavusans is a UTF-8 Unicode font, if you only need to
		// print standard ASCII chars, you can use core fonts like
		// helvetica or times to reduce file size.
		$this->tcpdf->SetFont('dejavusans', '', 14, '', true);

		// Add a page
		// This method has several options, check the source code documentation for more information.
		$this->tcpdf->AddPage();

		// set text shadow effect
		$this->tcpdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

		// Set some content to print
		$this->Booking_model->set_table('order');
		$order_detail = $this->Booking_model->fetch_row(NULL,array('OrderNumber' => $orderid));

		$this->Booking_model->set_table('data_penumpang');
		$penumpang = $this->Booking_model->fetch_rows(NULL,array('OrderID' => $orderid));

		$this->Booking_model->set_table('kereta_api');
		$detail_kereta = $this->Booking_model->fetch_row(NULL,array('IDKeretaApi' => $order_detail->IDKeretaApi));

		$this->Booking_model->set_table('pemberangkatan');
		foreach ($penumpang as $row) {
			$pemberangkatan = $this->Booking_model->fetch_row(NULL,array('IDPenumpang' => $row->IDPenumpang, 'IDKeretaApi' => $order_detail->IDKeretaApi));
			$row->Kursi = substr(strtoupper($detail_kereta->Kelas), 0, 3) . $pemberangkatan->NomorGerbong . '-'.$pemberangkatan->NomorKursi;
		}

		$this->Booking_model->set_table('user');
		$pemesan = $this->Booking_model->fetch_row(NULL,array('IDUser' => $order_detail->IDUser));

		$this->Booking_model->set_table('jadwal_berangkat');
		$berangkat = $this->Booking_model->fetch_row(NULL,array('Dari' => $order_detail->IDAsal, 'Tujuan' => $order_detail->IDTujuan, 'IDKeretaApi' => $order_detail->IDKeretaApi));

		$this->Booking_model->set_table('data_stasiun');
		$dari = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $order_detail->IDAsal));

		$tujuan = $this->Booking_model->fetch_row(NULL,array('IDStasiun' => $order_detail->IDTujuan));


		$this->Booking_model->set_table('rute_kereta_api');
		$query = $this->db->query("SELECT r.Rute,r.IDKeretaApi,k.NamaKeretaApi,k.Kelas,k.JumlahGerbong FROM rute_kereta_api r, kereta_api k, jadwal_berangkat j WHERE
			FIND_IN_SET($order_detail->IDTujuan,r.Rute) AND FIND_IN_SET($order_detail->IDAsal,r.Rute) 
			AND k.Kelas = '$detail_kereta->Kelas' AND r.IDKeretaApi = k.IDKeretaApi GROUP BY k.IDKeretaApi");
		$id_kereta_api = $query->result();

		
		$check_point = $this->check_point();
		$tarif = 0;


		foreach($id_kereta_api as $row){
			$count_check_point = 0;
			$rute = explode(',', $row->Rute);
			foreach($rute as $rute_elem){
				if(in_array($rute_elem, $check_point)){
					$count_check_point++;
				}
			}

			if($detail_kereta->Kelas == 'ekonomi'){
				$price = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_EKONOMI;
			} elseif ($detail_kereta->Kelas == 'bisnis'){
				$price = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_BISNIS;
			} elseif ($detail_kereta->Kelas == 'eksekutif'){
				$price = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_EKSEKUTIF;
			}
			
		}


		$qr_params = array(
			'data' => $orderid,
			'level' => 'H',
			'size' => 10,
			'savename' => $orderid. '.png'
		);
		$this->generate_qr($qr_params);

		

		$tcpdf_kai_template = new Tcpdf_kai_template();
		$params = array(
			'penumpang' => $penumpang,
			'pemesan' => $pemesan,
			'kode' => $orderid,
			'max_bayar' => date('D, d/M/Y H:i:s',strtotime($order_detail->TanggalOrder."+1 days")),
			'nomor_ka' => $detail_kereta->IDKeretaApi,
			'nama_ka' => $detail_kereta->NamaKeretaApi,
			'tanggal_berangkat' => $order_detail->TanggalBerangkat,
			'berangkat' =>  ($berangkat->JamBerangkat != '') ? date('D, d/M/Y',strtotime($order_detail->TanggalBerangkat)) . ' ' . date('H:i:s',strtotime($berangkat->JamBerangkat)) : date('D, d/M/Y',strtotime($order_detail->TanggalBerangkat)) . ' N/A',
			'dari' => $dari->NamaStasiun . ', '.$dari->Kota,
			'tiba' => (strtotime($berangkat->JamSampai) < strtotime($berangkat->JamBerangkat)) ? date('D, d/M/Y',strtotime($order_detail->TanggalBerangkat . "tomorrow")) . ' ' . date('H:i:s',strtotime($berangkat->JamSampai)) : date('D, d/M/Y',strtotime($order_detail->TanggalBerangkat)) . ' ' . date('H:i:s',strtotime($berangkat->JamSampai)) ,
			'tujuan' => $tujuan->NamaStasiun . ', ' .$tujuan->Kota,
			'total_penumpang' => sizeof($penumpang),
			'satuan_harga' => $price,
			'total_harga' => sizeof($penumpang) * $price,
			'qrcode' => base_url() . 'public/images/qrcode/' . $orderid . '.png',
			'verified' => ($order_detail->IsVerified) ? base_url() . 'public/images/verified.png' : NULL,

		);
		$html = $tcpdf_kai_template->template_pemesanan($params);

		// Print text using writeHTMLCell()
		$this->tcpdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

		// ---------------------------------------------------------

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		$this->tcpdf->Output('Bukti Pemesanan tiket - '.$orderid.'.pdf', $option);


	}

}