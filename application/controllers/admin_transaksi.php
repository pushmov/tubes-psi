<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Admin_controller.php');

class Admin_transaksi extends Admin_controller {

	const ITEM_PER_PAGE = 10;

	public function __construct(){
		parent::__construct();
		$this->load->model('Admin_model');
		$this->admin_area();
	}

	private function get_price($params){

		$this->Admin_model->set_table('rute_kereta_api');
		$query = $this->db->query("SELECT r.Rute,r.IDKeretaApi,k.NamaKeretaApi,k.Kelas,k.JumlahGerbong FROM rute_kereta_api r, kereta_api k, jadwal_berangkat j WHERE
			FIND_IN_SET($params[toid],r.Rute) AND FIND_IN_SET($params[fromid],r.Rute) 
			AND k.Kelas = '$params[kelas]' AND r.IDKeretaApi = k.IDKeretaApi GROUP BY k.IDKeretaApi");
		$id_kereta_api = $query->result();
		
		$check_point = $this->check_point();
		$tarif = 0;

		foreach($id_kereta_api as $row){

			$count_check_point = 0;
			$rute = explode(',', $row->Rute);
			foreach($rute as $rute_elem){
				if(in_array($rute_elem, $check_point)){
					$count_check_point++;
				}
			}

			if($row->Kelas == 'ekonomi'){
				$tarif = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_EKONOMI;
			} elseif ($row->Kelas == 'bisnis'){
				$tarif = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_BISNIS;
			} elseif ($row->Kelas == 'eksekutif'){
				$tarif = ($count_check_point * self::PRICE_PER_CHECKPOINT) + self::PRICE_EKSEKUTIF;
			}
			
		}

		return $tarif;
	}

	public function index($page=0){

		$this->Admin_model->set_table('order');
		$all_result = $this->Admin_model->fetchall(NULL,array('IsVerified' => 1));

		$data['total_record'] = count($all_result);

		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'admin_ka/index/';
		$config['total_rows'] = count($all_result);
		$config['per_page'] = self::ITEM_PER_PAGE;
		$config['cur_tag_open'] = '<span class="page active">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '';
		$config['prev_page'] = '';
		
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();

		
		$this->Admin_model->set_table('order');
		$result = $this->Admin_model->get_transaksi(NULL,array('IsVerified' => 1),"TanggalOrder DESC",self::ITEM_PER_PAGE,$page);
		foreach($result as $row){
			$this->Admin_model->set_table('data_penumpang');
			$row->TotalPenumpang = sizeof($this->Admin_model->fetch_rows(NULL,array('OrderID' => $row->OrderNumber)));

			$this->Admin_model->set_table('kereta_api');
			$row_kereta = $this->Admin_model->fetch_row('Kelas',array('IDKeretaApi' => $row->IDKeretaApi));
			$tarif_params = array(
				'toid' => $row->IDAsal,
				'fromid' => $row->IDTujuan,
				'kelas' => $row_kereta->Kelas,
			);
			$row->Tarif = $this->get_price($tarif_params);
		}
		$data['result'] = $result;

		$this->load->view('admin/transaksi_index',$data);
	}

	public function pending($page=0){
		$this->Admin_model->set_table('order');
		$all_result = $this->Admin_model->fetchall(NULL,array('IsVerified' => 0));

		$data['total_record'] = count($all_result);

		$this->load->library('pagination');
		$config['base_url'] = base_url() . 'admin_transaksi/pending/';
		$config['total_rows'] = count($all_result);
		$config['per_page'] = self::ITEM_PER_PAGE;
		$config['cur_tag_open'] = '<span class="page active">';
		$config['cur_tag_close'] = '</span>';
		$config['next_page'] = '';
		$config['prev_page'] = '';
		
		$this->pagination->initialize($config);
		$data['page'] = $this->pagination->create_links();

		
		$this->Admin_model->set_table('order');
		$result = $this->Admin_model->get_transaksi(NULL,array('IsVerified' => 0),"TanggalOrder DESC",self::ITEM_PER_PAGE,$page);
		foreach($result as $row){
			$this->Admin_model->set_table('data_penumpang');
			$row->TotalPenumpang = sizeof($this->Admin_model->fetch_rows(NULL,array('OrderID' => $row->OrderNumber)));

			$this->Admin_model->set_table('kereta_api');
			$row_kereta = $this->Admin_model->fetch_row('Kelas',array('IDKeretaApi' => $row->IDKeretaApi));
			$tarif_params = array(
				'toid' => $row->IDAsal,
				'fromid' => $row->IDTujuan,
				'kelas' => $row_kereta->Kelas,
			);
			$row->Tarif = $this->get_price($tarif_params);
		}
		$data['result'] = $result;

		$this->load->view('admin/transaksi_pending',$data);
	}

	public function search($isverified=NULL){

		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');

		$sql_start_date = $this->date_to_sql($start_date);
		$sql_end_date = $this->date_to_sql($end_date);
		
		$this->Admin_model->set_table('order');
		$result = $this->Admin_model->custom_rows(NULL,"IsVerified = $isverified AND TanggalOrder >= '$sql_start_date' AND TanggalOrder <= '$sql_end_date'");
		$data['total_record'] = sizeof($result);
		foreach($result as $row){

			$this->Admin_model->set_table('kereta_api');
			$row_kereta = $this->Admin_model->fetch_row(NULL,array('IDKeretaApi' => $row->IDKeretaApi));
			$tarif_params = array(
				'toid' => $row->IDAsal,
				'fromid' => $row->IDTujuan,
				'kelas' => $row_kereta->Kelas,
			);
			$row->Tarif = $this->get_price($tarif_params);

			$this->Admin_model->set_table('data_penumpang');
			$row->TotalPenumpang = sizeof($this->Admin_model->fetch_row(NULL,array('OrderID' => $row->OrderNumber)));
		}

		$data['result'] = $result;

		$this->load->view('admin/transaksi_search',$data);
	}

}