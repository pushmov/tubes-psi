<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Core_controller.php');
include_once(APPPATH . 'libraries/tcpdf/examples/tcpdf_include.php');

class Qrgenerator extends Core_controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Booking_model');
		$this->load->model('Member_model');
	}

	public function generate_qr($params){
		$this->load->library('ciqrcode');

		//$params['data'] = 'EUGBDHHL6Q4HGKWO';
		//$params['level'] = 'H';
		//$params['size'] = 10;
		$params['savename'] = FCPATH.$params['savename'];

		$this->ciqrcode->generate($params);

		return '<img src="'.base_url().$params['savename'].'" />';
	}

}