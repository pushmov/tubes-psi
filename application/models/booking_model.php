<?php

include_once( APPPATH .  'models/appmodel.php');

Class Booking_model extends Appmodel
{

	
	function search_stasiun($params){
		return $this->db->like('NamaStasiun',$params,'after')
			->get($this->table)
			->result();
	}

	function latest_seat($select=NULL,$where=NULL,$order_by){
		if(isset($select)){
			$this->db->select($select);
		}
		if(isset($where)){
			$this->db->where($where);
		}
		if(isset($order_by)){
			$this->db->order_by($order_by);
		}

		return $this->db->get($this->table)
			->row();
	}

	function all_seat($select=NULL,$where=NULL,$order_by=NULL){
		if(isset($select)){
			$this->db->select($select);
		}
		if(isset($where)){
			$this->db->where($where);
		}
		if(isset($order_by)){
			$this->db->order_by($order_by);
		}

		return $this->db->get($this->table)
			->result();	
	}

	function count_seat_booked($where=NULL){

		return $this->db->where($where)
			->from($this->table)
			->count_all_results();

	}

}