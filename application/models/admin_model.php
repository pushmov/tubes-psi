<?php

include_once( APPPATH .  'models/appmodel.php');

Class Admin_model extends Appmodel
{

	function fetchall($select=NULL,$where=NULL){

		if(isset($select)){
			$this->db->select($select);
		}

		if(isset($where)){
			$this->db->where($where);
		}

		return $this->db->order_by("TanggalOrder DESC")
			->get($this->table)
			->result();

	}

	function count_rows($where=NULL){
		if(isset($where)){
			$this->db->where($where);
		}

		return $this->db->from($this->table)
			->count_all_results();


	}


	function join_table($params){
		
		$this->db->where($params['where']);
		$this->db->join($params['table'],$params['arg']);

		return $this->db->get($this->table)
			>result();
		
	}

	function fetch_ka($select=NULL,$limit=NULL,$offset=NULL){
		if(isset($select)){
			$this->db->select($select);
		}

		return $this->db
			->get($this->table,$limit,$offset)
			->result();
	}

	function multi_table_rows($params){
		if(isset($params['select'])){
			$this->db->select($params['select']);
		}
		
		if(isset($params['where1'])){
			$this->db->where($params['where1'],NULL,FALSE);
		}
		
		$this->db->join($params['tablejoin'],$params['where2']);
		
		if(isset($params['group_by'])){
			$this->db->group_by($params['group_by']);
		}
		
		if(isset($params['order_by'])){
			$this->db->order_by($params['order_by']);
		}
		
		$limit = (isset($params['limit'])) ? $params['limit'] : NULL;
		$offset = (isset($params['offset'])) ? $params['offset'] : NULL;

		$query = $this->db->get($this->table,$limit,$offset);
		return $query->result();
	}
	
	function multi_table_row($params){
		if(isset($params['select'])){
			$this->db->select($params['select']);
		}
		
		$this->db->where($params['where1'],NULL,FALSE);
		$this->db->join($params['tablejoin'],$params['where2']);
		
		if(isset($params['group_by'])){
			$this->db->group_by($params['group_by']);
		}
		
		if(isset($params['order_by'])){
			$this->db->order_by($params['order_by']);
		}
		
		$query = $this->db->get($this->table);
		return $query->row();
	}

	function custom_delete($where){
		$this->db->where($where,NULL,FALSE)
			->delete($this->table);
	}

	function get_transaksi($select=NULL,$where=NULL,$order_by=NULL,$limit=NULL,$offset=NULL){
		
		if(isset($select)){
			$this->db->select($select);
		}

		if(isset($where)){
			$this->db->where($where);
		}

		if(isset($order_by)){
			$this->db->order_by($order_by);
		}

		return $this->db->get($this->table,$limit,$offset)
			->result();


	}

}