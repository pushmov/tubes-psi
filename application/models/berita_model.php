<?php

include_once( APPPATH .  'models/appmodel.php');

Class Berita_model extends Appmodel
{


	function getlist($select=NULL,$where=NULL,$limit=NULL,$offset=NULL){

		if(isset($select)){
			$this->db->select($select);
		}

		if(isset($where)){
			$this->db->where($where);
		}

		$query = $this->db->get($this->table,$limit,$offset);
	}

	function jumlah_berita($select=NULL,$where=NULL){

		if(isset($select)){
			$this->db->select($select);
		}

		if(isset($where)){
			$this->db->where($where);
		}

		return $this->db->from($this->table)
			->count_all_results();
	}

}