<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . 'core/Core_controller.php');

class Admin_controller extends Core_Controller {

	public function __construct(){
		parent::__construct();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
	}

	protected function admin_area(){
		if(!$this->session->userdata('admin_psi')){
			$this->session->set_flashdata('error','accessdenied');
			$this->session->set_flashdata('message','Access denied');
			redirect('/admin/login/');
		}
	}

	protected function admin_session(){
		return $this->session->userdata('admin_psi');
	}

}