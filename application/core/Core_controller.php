<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Core_controller extends CI_Controller {

	const PRICE_PER_CHECKPOINT = 30000;
	const PRICE_EKONOMI = 20000;
	const PRICE_BISNIS = 30000;
	const PRICE_EKSEKUTIF = 35000;


	public function __construct(){
		parent::__construct();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
	}

	protected function date_to_sql($date){
		$arr_date = explode('/', $date);

		$build_date = $arr_date[2] . '-' . $arr_date[0] . '-' .$arr_date[1];

		return $build_date;
	}

	protected function date_to_jqui($date){

	}

	protected function random_id($length = 16, $seeds = 'alphanum'){
		$seedings['alpha'] = 'QWERTYUIOPASDFGHJKLZXCVBNM';
		$seedings['numeric'] = '0123456789';
		$seedings['alphanum'] = 'QWERTYUIOPASDFGHJKLZXCVBNM0123456789';
		$seedings['hexidec'] = '0123456789ABCDEF';
			
		// Choose seed
		if (isset($seedings[$seeds]))
		{
			$seeds = $seedings[$seeds];
		}
			
		// Seed generator
		list($usec, $sec) = explode(' ', microtime());
		$seed = (float) $sec + ((float) $usec * 100000);
		mt_srand($seed);
			
		// Generate
		$str = '';
		$seeds_count = strlen($seeds);
			
		for($i = 0; $length > $i; $i++)
		{
			$str .= $seeds{mt_rand(0, $seeds_count - 1)};
		}
			
		return $str;
	}

	protected function check_point(){
		$return = array(80, 84, 81, 83, 82, 37, 1, 54, 137, 151, 150, 22, 148, 44, 109, 184, 181, 108, 158, 115, 126, 121, 167, 168 );
		return $return;
	}

	protected function session(){
		return $this->session->userdata('logged_in_psi');
	}

	protected function secure_area(){
		if(!$this->session->userdata('logged_in_psi')){
			$this->session->set_flashdata('message','Halaman ini terproteksi. Anda harus login terlebih dahulu sebelum melanjutkan');
			$this->session->set_flashdata('secure_area','secure_area');
			redirect('/login');
		}
	}

	protected function friendly_segment($name){
		$m = strlen($name);
		$segment = '';
		for ($n = 0; $n < $m; ++$n)
		{
			$c = substr($name, $n, 1);
			$x = ord($c);
			if ($x == 45 || ($x >= 48 && $x <= 57) || ($x >= 97 && $x <= 122))
			{
				$segment .= $c;
			}
			else if ($x >= 65 && $x <= 90)
			{
				$segment .= chr($x + 32);
			}
			else if ($x == 32)
			{
				$segment .= '-';
			}
		}
		return $segment;
	}

	protected function mapping_kursi($params){
		$hashed_map = array();
		$kelas = $params['kelas'];
		

		$init_gerbong = 1;
		$init_block = 'A';
		$init_kursi = 1;

		$max_gerbong = $params['max_gerbong'];
		$max_block = ($params['kelas'] == 'ekonomi') ? array('A','B','C','D','E') : array('A','B','C','D');
		$max_kursi = 10;

		$block_seat = '';

		for($i=$init_gerbong;$i<=$max_gerbong;$i++){

			for($y=1;$y<=$max_kursi;$y++){

				for($z=0;$z<=count($max_block)-1;$z++){
					
					$block_seat = $i . $y . $max_block[$z];
					//echo $block_seat;
					//echo '<br>';
					$hashed_map[] = $block_seat;
				}
				
			}

		}

		return $hashed_map;
	}


}