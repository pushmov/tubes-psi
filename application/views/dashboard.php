<?php 
	
	$title['title'] = 'PT KAI Sistem Informasi Tiket - Dashboard';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/datepicker');
	$this->load->view('header/plugin/autosuggest');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');
?>	

		<!-- main -->
		<div id="main" class="main">
			<div class="wrapper clearfix center">
				<div style="width:30%;margin:50px auto;border:1px solid #CCCCCC;padding:25px;" class="center">
				<a href="<?php echo base_url(); ?>dashboard/logout/">logout</a>
				</div>
			</div>
		</div>
		<!-- end main -->
		
<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
