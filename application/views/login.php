<?php 
	
	$title['title'] = 'PT KAI Sistem Informasi Tiket - Index';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/datepicker');
	$this->load->view('header/plugin/autosuggest');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');
?>	

		<!-- main -->
		<div id="main" class="main">
			<div class="wrapper clearfix center">
				<div style="width:30%;margin:50px auto;border:1px solid #CCCCCC;padding:25px;" class="center">
				<h3 class="left" style="padding:8px 0">Login</h3>

				
				<span id="error" style="color:#FF0000" class="fs12">
					<?php if($this->session->flashdata('error')) : ?>
					<?php echo $this->session->flashdata('message'); ?>
					<?php endif; ?>
				</span>
				<form action="<?php echo base_url(); ?>login/verify/" method="POST" name="login" id="login">
					<table style="width:100%" align="center">
						<tr>
							<td class="left" style="padding:8px 0;width:40%">Email : </td>
							<td class="left" ><input type="text" style="width:90%;border:1px solid #CCCCCC;" name="email" id="email"></td>
						</tr>
						<tr>
							<td class="left" style="padding:8px 0;width:40%">Password : </td>
							<td class="left" ><input type="password" style="width:90%;border:1px solid #CCCCCC;" name="password" id="password"></td>
						</tr>
						<tr>
							<td class="right" style="padding:8px 10px;" colspan="2"><button class="orange-button pointer" id="submit-ticket">Login</button></td>
						</tr>
					</table>
				</form>
				</div>
			</div>
		</div>
		<!-- end main -->
		<script>
			$(document).ready(function(){

			});

			$('#submit-ticket').click(function(){
				$('#error').html('');
				var email = $('#email').val();
				var password = $('#password').val();

				if(email == ''){
					$('#error').html('Error : email tidak boleh kosong');
					$('#email').focus();
					return false;
				}

				if(password == ''){
					$('#error').html('Error : password tidak boleh kosong');
					$('#password').focus();
					return false;
				}


				$('#login').submit();

			});
		</script>
<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
