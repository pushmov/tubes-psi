<?php 
	
	$title['title'] = 'PT KAI Sistem Informasi Tiket - Index';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/datepicker');
	$this->load->view('header/plugin/autosuggest');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');
?>	
		<!-- main -->
		<div id="main" class="main">
			<div class="wrapper clearfix">
				<div class="float-left" style="width:70%">
					<div class="clearfix">
						<div class="cols-1 float-left" style="width:32%;margin-right:20px;">
						<!-- left panel here -->
							<?php $this->load->view('leftpanel'); ?>

						</div>
						<div class="cols-2 float-right" style="width:65%">
							<!-- center panel here -->
							<div class="center">
								
								<div class="ticket-box">
									<div class="train-white circle-radius"><img src="<?php echo base_url(); ?>public/images/train-white.png"></div>
									<div class="clear">
										<div class="clearfix">
											<ul class="ticket-tabs">
												<li><a class="strong" href="javascript:void(0)">PESAN TIKET</a></li>
												<!--
												<li><a class="strong" href="#tab2">Tab 2</a></li>
												<li><a class="strong" href="#tab3">Tab 3</a></li>
											-->
											</ul>
										</div>
										<div class="clearfix ticket-wrapper left">
											<div>
												<?php if($this->session->flashdata('stasiun') == 'error') : ?>
												<div style="color:#FF0000" class="fs12">Error. Nama stasiun sama</div>
												<?php endif; ?>

												<?php if($this->session->flashdata('tanggal') == 'error'):?>
												<div style="color:#FF0000" class="fs12">Error. Tanggal booking minimal 2 hari dari tanggal sekarang</div>
												<?php endif; ?>

												<form action="<?php echo base_url(); ?>order/info_jadwal/" method="POST" name="pesantiket" id="pesantiket">
												<table style="width:100%">
													<tr>
														<td><h4 class="form-label">DARI</h4></td>
														<td>
															<div style="padding:5px 0;">
																<input type="text" autocomplete="off" name="stasiun-from" id="stasiun-from" class="city" onkeyup="autoSuggestNew(this.id, 'listWrap1', 'searchList1', 'stasiun-from','stasiun-from','stasiun-from', event,'1');">
																<div class="city">
																	Pilih Stasiun
																</div>
																<div class="listWrap" id="listWrap1">
																	<ul class="searchList" id="searchList1" style="width:64%">
																	</ul>
																</div>
															</div>
														</td>
														<td></td>
													</tr>
													<tr>
														<td><h4 class="form-label">TUJUAN</h4></td>
														<td>
															<div style="padding:5px 0;">
																<input autocomplete="off" type="text" id="stasiun-to" name="stasiun-to" class="city" onkeyup="autoSuggestNew(this.id, 'listWrap2', 'searchList2', 'stasiun-to','stasiun-to','stasiun-to', event,'1');">
																<div class="city" >
																	Pilih Stasiun
																</div>
																<div class="listWrap" id="listWrap2">
																	<ul class="searchList" id="searchList2" style="width:64%">
																	</ul>
																</div>
															</div>
														</td>
														<td></td>
													</tr>
													<tr>
														<th valign="middle"><h4 class="form-label">PEMESAN</h4></th>
														<td>
															<select name="jumlah_pemesan" id="jumlah_pemesan">
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
															</select>
														</td>
														<td></td>
													</tr>
													<tr>
														<th valign="middle"><h4 class="form-label">TANGGAL</h4></th>
														<td>
															<div style="padding:5px 0;"><input type="text" id="datepicker" class="datepicker" name="date" ><div class="city"><img src="<?php echo base_url(); ?>public/images/calendar.png" style="margin-right:5px;vertical-align:middle;border:none">Pilih Tanggal</div></div>
															<div style="padding:5px 0;">
																<label class="label_radio" for="sample-check" alt="ekonomi">
																  <input type="radio" name="rad-kelas" value="ekonomi" checked="checked">Ekonomi
																</label>

																<label class="label_radio" for="sample-radio" alt="bisnis">
																  <input type="radio" name="rad-kelas" value="bisnis">Bisnis
																</label>
																<label class="label_radio" for="sample-radio" alt="eksekutif">
																	<input type="radio" name="rad-kelas" value="eksekutif">Eksekutif
																</label>

																<input type="hidden" name="kelas" id="kelas" value="ekonomi">
															</div>
														</td>
														<td></td>
													</tr>

													
													
													<tr>
														<td colspan="3">
															<div><span style="color:#FF0000 !important;font-size:12px;display:none;" id="error-tag"></span></div>
															<div class="right" style="margin:0 20px;"><button type="submit" id="submit-ticket" class="orange-button pointer">LIHAT JADWAL</button></div>
														</td>
													</tr>
												</table>
											</form>
											</div>
											
										</div>
									</div>
									
								</div>
								<div style="margin:10px 0">
									<div class="sch-wrapper">
										<table style="width:100%">
										<thead class="sch-item-head">
											<tr>
												<th class="strong left">Berangkat</th>
												<th class="strong left">Dari</th>
												<th class="strong left">Sampai</th>
												<th class="strong left">Tujuan</th>
												<th class="strong left"></th>
											</tr>
										</thead>
										<?php foreach ($kota as $key => $value) : ?>
											<tr class="sch-item">
												<td class="left"><?php echo ($value->JamBerangkat == '') ? 'N/A' : $value->JamBerangkat; ?> WIB</td>
												<td class="green left"><?php echo $value->NamaStasiun . ', '. $value->Kota; ?></td>
												<td class="left"><?php echo ($value->JamSampai == '') ? 'N/A' : $value->JamSampai; ?></td>
												<td class="left"><?php echo $value->Tujuan->NamaStasiun . ', ' . $value->Tujuan->Kota; ?></td>
												<td class="green left"><a class="green" style="color:#DD9C17" href="<?php echo base_url(); ?>order/agreement/<?php echo $value->IDJadwal; ?>">Pesan &raquo;</a></td>
											</tr>
										<?php endforeach; ?>
										
										</table>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="cols-3 float-right" style="width:27.5%;margin-left:20px;">
				<!-- right panel here -->
					<?php $this->load->view('rightpanel'); ?>
				</div>
			</div>
		</div>
		<!-- end main -->
		
		<div id="myModal" class="reveal-modal">
			<div id="ajax-response">
				<h1>Silahkan pilih kota berikut : </h1>
				<div style="margin:10px 0">
					<table style="width:100%">
						<?php $i=1;?>
						<tr>
						<?php foreach ($kota as $row) : ?>

						
							<td class="pilihkota" style="padding:5px 3px"><a href="javascript:void(0)" style="text-decoration:none;color:#333333" class="fs16 blue"><?php echo $row->Kota; ?></a></td>

						<?php if($i % 5 == 0) : ?>
						</tr><tr>
						<?php endif; ?>
					<?php $i++; ?>
					<?php endforeach; ?>
						
					</table>
					<div class="right"><a href="javascript:void(0)" class="myButton" id="myButton">next &raquo;</a></div>
					<div class="right"><span id="kotaclicked" alt=""></span></div>
				</div>
				<a class="close-reveal-modal">&#215;</a>
			</div>
		</div>

		<script>

			$(document).ready(function(){

				$('#error-tag').hide();
				$('#submit-ticket').click(function(){

					var dari = $('#stasiun-from').val();
					var tujuan = $('#stasiun-to').val();
					var tanggal = $('#datepicker').val();

					if(dari == ''){
						$('#error-tag').html('Erorr : Stasiun awal harus diisi').show();
						$('#stasiun-from').focus();
						return false;
					}
					if(tujuan == ''){
						$('#error-tag').html('Erorr : Stasiun awal harus diisi').show();
						$('#stasiun-to').focus();
						return false;
					}
					if(tanggal == ''){
						$('#error-tag').html('Erorr : Tanggal keberangkatan harus diisi').show();
						$('#datepicker').focus();
						return false;
					}
				});

				

			});

			$('.pilihkota').click(function(){
				$('.pilihkota').removeClass('clicked');
				$(this).addClass('clicked');
				var link = $(this).find('a').html();
				$('#kotaclicked').attr('alt',link);
				
			});

			$('.label_radio').click(function(){
				$('#kelas').val($(this).attr('alt'));
			});

			$('.myButton').click(function(){

				var kotaclicked = $('#kotaclicked').attr('alt');
				if(kotaclicked == ''){
					alert('Pilih kota dahulu');
					return false;
				}

				$.ajax({
					url: '<?php echo base_url(); ?>welcome/ajax_get_stasiun/'+kotaclicked,
					cache: false,
					success: function(response){
						$('#ajax-response').html(response);
						$('#myButton').hide("OK");
						return false;
					}
				});

			});

		</script>
<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
