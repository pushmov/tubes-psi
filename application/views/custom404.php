<?php 
	
	$title['title'] = 'PT KAI Sistem Informasi Tiket - 404';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/datepicker');
	$this->load->view('header/plugin/autosuggest');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');
?>	

		<!-- main -->
		<div id="main" class="main">
			<div class="wrapper clearfix center">
				<div style="margin:40px 0">
					<img src="<?php echo base_url(); ?>public/images/maintenance.jpg">
				</div>
				<h1 style="color:#E67815">Error : Halaman tidak ditemukan atau sedang maintenance.</h1>
				<h3>Cek <a style="color:#333333" href="<?php echo base_url(); ?>sitemap">sitemap</a>.</h3>
			</div>
		</div>
		<!-- end main -->
		
<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
