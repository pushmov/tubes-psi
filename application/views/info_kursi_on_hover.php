				<table class="maintable">
					<thead>
						<tr>
							<th>Nomor Order</th>
							<th>Nama</th>
							<th>Tanggal Berangkat</th>
							<th>Jenis Kelamin</th>
							<th>Umur</th>
						</tr>
					</thead>
					<tbody>
						<?php if(!empty($detail)) : ?>
						<tr>
							<td><?php echo $detail->OrderID; ?></td>
							<th><?php echo $detail->NamaPenumpang; ?></th>
							<td><?php echo date('D, d/M/Y',strtotime($detail->Pemberangkatan->PemberangkatanTanggal)); ?></td>
							<td>
								<?php if($detail->JenisKelamin == '') : ?>
								<?php echo 'N/A'; ?>
								<?php else : ?>
								<?php echo ($detail->JenisKelamin == 'P') ? 'Pria' : 'Wanita'; ?>
								<?php endif; ?>
							</td>
							<td><?php echo ($detail->DOB == '') ? 'N/A' : $detail->Age; ?></td>
						</tr>
						<?php else : ?>
						<tr>
							<td colspan="4">No record found</td>
						</tr>
						<?php endif; ?>
					</tbody>
				</table>
				<div class="center" style="color:#FF0000">Error: kursi telah dibooking.</div>