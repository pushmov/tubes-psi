<?php 
	
	$title['title'] = 'PT KAI Sistem Informasi Tiket - Index';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/datepicker');
	$this->load->view('header/plugin/autosuggest');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');
?>	

		<!-- main -->
		<div id="main" class="main">
			
			<div class="center" style="margin-bottom:30px;">
				<div id="crumbs" class="center">
					<ul>
						<li><a href="javascript:void(0)">1. Info Jadwal </a></li>
						<li><a href="javascript:void(0)">2. Pilih KA</a></li>
						<li><a href="javascript:void(0)">3. Info Booking</a></li>
						<li><a href="javascript:void(0)">4. Isi Data</a></li>
						<li><a class="current" href="javascript:void(0)">5. Pembayaran</a></li>
						<li><a href="javascript:void(0)">6. Konfirmasi</a></li>
					</ul>
				</div>
			</div>

			<div class="wrapper clearfix center">
				
				<div class="ticket-box" style="width:90%;margin:0 auto">
						<div class="clear">
							
							
							<div class="clearfix ticket-wrapper left">
								<table align="center" style="width:100%;">
									<tbody>
										<tr>
											<td colspan="3" style="padding:7px 0;color:#E57918" class="fs28 strong center">RESERVASI TIKET ONLINE</td>
										</tr>
										
										<tr>
											<td colspan="3" class="center"><div style="background:#eaeaea" class="kereta-api-result fs16">Info Perjalanan</div></td>
										</tr>
										<tr>
											<td colspan="3">
												<table style="width:100%">
													<tr>
														<td class="strong" style="padding:7px 20px">Tanggal</td>
														<td class="strong" style="padding:7px 20px">No. Kereta</td>
														<td class="strong" style="padding:7px 20px">Nama Kereta</td>
														<td class="strong" style="padding:7px 20px">Berangkat</td>
														<td class="strong" style="padding:7px 20px">Tiba</td>
													</tr>
													<tr>
														<td style="padding:7px 20px"><?php echo date('D, d/M/Y',strtotime($this->session->userdata('tanggal_berangkat'))); ?></td>
														<td style="padding:7px 20px"><?php echo $detail_kereta->IDKeretaApi; ?></td>
														<td style="padding:7px 20px"><?php echo $detail_kereta->NamaKeretaApi; ?></td>
														<td style="padding:7px 20px">
															<div><?php echo date('D, d/M/Y',strtotime($this->session->userdata('tanggal_berangkat'))) . ' ' . date('H:i',strtotime($detail_jadwal->JamBerangkat)); ?></div>
															<div>
															<?php echo $from->Kota; ?>
															</div>
														</td>
														<td style="padding:7px 20px">
															<div><?php echo (strtotime($detail_jadwal->JamSampai) < strtotime($detail_jadwal->JamBerangkat)) ? date('D, d/M/Y',strtotime($this->session->userdata('tanggal_berangkat') . "tomorrow")) . ' ' . date('H:i',strtotime($detail_jadwal->JamSampai)) : date('D, d/M/Y',strtotime($this->session->userdata('tanggal_berangkat'))) . ' ' . date('H:i',strtotime($detail_jadwal->JamSampai)); ?></div>
															<div>
															<?php echo $to->Kota; ?>
															</div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										
										
										<tr>
											<td colspan="3" class="center"><div style="background:#eaeaea" class="kereta-api-result fs16">Info penumpang</div></td>
										</tr>
										<tr>
											<td colspan="3">
												<table style="width:100%">
													<tr>
														<td class="strong" style="padding:7px 20px">Nama Penumpang</td>
														<td class="strong" style="padding:7px 20px">Nomor Identitas</td>
														<td class="strong" style="padding:7px 20px">Tipe Penumpang</td>
														<td class="strong" style="padding:7px 20px">Tempat Duduk Penumpang</td>
													</tr>

													<?php $iter = 0; ?>
													<?php foreach($namapenumpang as $penumpang) : ?>
													<tr>
														<td style="padding:7px 20px"><?php echo $penumpang; ?></td>
														<td style="padding:7px 20px"><?php echo $idpenumpang[$iter]; ?></td>
														<td style="padding:7px 20px">Dewasa</td>
														<td style="padding:7px 20px">
															<span style="margin:0 10px"><?php echo strtoupper(substr($kelas,0,3)) . substr($tempat_duduk[$iter+1],0,1) .'-'. substr($tempat_duduk[$iter+1],1,2); ?></span><a id="edit-kursi" class="underline strong pointer" href="<?php echo base_url(); ?>kursi/editkursi/<?php echo $idpenumpang[$iter]; ?>/<?php echo $iter+1; ?>/" style="color:#FF0000;margin:0 10px">Edit</a>
														</td>
													</tr>
													<?php $iter++; ?>
													<?php endforeach; ?>

												</table>
											</td>
										</tr>
										
										<tr>
											<td colspan="3" class="center"><div style="background:#eaeaea" class="kereta-api-result fs16">Info harga</div></td>
										</tr>
										<tr>
											<td colspan="3">
												<table style="width:100%">
													<tr>
														<td class="strong" style="padding:7px 20px">Jenis Penumpang</td>
														<td class="strong" style="padding:7px 20px">Jumlah Penumpang</td>
														<td class="strong" style="padding:7px 20px">Harga Satuan</td>
														<td class="strong" style="padding:7px 20px">Total Harga</td>
													</tr>

													<tr>
														<td style="padding:7px 20px">Dewasa</td>
														<td style="padding:7px 20px"><?php echo $input_jadwal['jmlpenumpang']; ?></td>
														<td style="padding:7px 20px"><?php echo 'Rp '.number_format(round($this->session->userdata('tarif')),0); ?></td>
														<td class="right" style="padding:7px 20px"><?php echo 'Rp '.number_format($this->session->userdata('tarif') * $input_jadwal['jmlpenumpang']); ?></td>
													</tr>
													<tr>
														<td style="padding:7px 20px">Biaya Layanan Pelanggan</td>
														<td style="padding:7px 20px"></td>
														<td style="padding:7px 20px"><?php echo 'Rp. '.number_format(7500,0); ?></td>
														<td class="right" style="padding:7px 20px"><?php echo 'Rp. '.number_format(7500,0); ?></td>
													</tr>

													<tr>
														<td colspan="3" class="right strong" style="padding:7px 20px">Total</td>
														<td class="right strong" style="padding:7px 20px"><?php echo 'Rp. '.number_format(7500 + ($this->session->userdata('tarif') * $input_jadwal['jmlpenumpang']),0); ?></td>
													</tr>

												</table>
											</td>
										</tr>
										
										
									</tbody>
								</table>
								<div class="clearfix" style="margin:50px 200px;">
									<div class="float-left"><button onclick="window.location='<?php echo base_url(); ?>order/discard/'" id="back" class="orange-button pointer">Batalkan Pemesanan</button></div>
									<div class="float-right"><button id="next" onclick="window.location='<?php echo base_url(); ?>order/konfirmasi/'" class="orange-button pointer">Lanjutkan &raquo;</button></div>
								</div>


							</div>
						</div>
					</div>
				
			</div>
		</div>
		<!-- end main -->
		
<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
