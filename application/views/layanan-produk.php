<?php

	$title['title'] = 'PT KAI Sistem Informasi Tiket - Gallery';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/prettyphoto');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');

?>
		<!-- main -->
		<div id="main" class="main">
			<div class="wrapper clearfix">
				<div class="float-left" style="width:90%">
					<div class="clearfix">
						<div class="cols-1 float-left" style="width:25%;margin-right:20px;">
						<!-- left panel here -->
							<?php $this->load->view('leftpanel'); ?>

						</div>
						<div class="cols-2" style="width:100%">
							<!-- center panel here -->
							<div style="margin:20px;">
								<h1 class="page-title">Layanan Produk</h1>
								<p style="color:#777777">Info Layanan dan Produk Perusahaan</p>
								
								<div style="margin:30px">

									<table style="width:80%" align="center">
										<tr>
											<td style="margin:0 15px;">
												<div style="width:90%;padding:0 20px;">
													<div class="fs18" style="margin:10px 0"><strong><a style="color:#000000;text-decoration:none;" href="http://tiket.kereta-api.co.id/">tiket.kereta-api.co.id</a></strong></div>
													<p style="text-align:justify">Sebagai perusahaan yang mengelola perkeretaapian di Indonesia, PT. Kereta Api Indonesia (Persero) telah banyak mengoperasikan KA penumpangnya, baik KA Utama (Komersil dan Non Komersil), maupun KA Lokal di Jawa dan Sumatera, yang terdiri dari :</p>
													<ul style="margin:10px 0">
														<li>- KA Eksekutif</li>
														<li>- KA Ekonomi AC</li>
														<li>- KA Bisnis</li>
														<li>- KA Ekonomi</li>
														<li>- KA Campuran</li>
														<li>- KA Lokal</li>
														<li>- KRL</li>
													</ul>

													<p>Pelayanan Penumpang</p>

													<ul style="margin:10px 0">
														<li>A. RESTORASI</li>
														<li>B. KRU KA</li>
														<li>C. MANAGER ON DUTTY</li>
														<li>D. ON TRAIN CLEANING</li>
													</ul>
													<div style="margin:15px 0"><button class="orange-button pointer">Selengkapnya &raquo;</button></div>
												</div>
											</td>
											<td style="margin:0 15px;">
												<div style="width:90%;padding:0 20px;">
													<div class="fs18" style="margin:10px 0"><strong><a style="color:#000000;text-decoration:none;" href="http://kargo.kereta-api.co.id/">kargo.kereta-api.co.id</a></strong></div>
													<p>Komoditi yang dapat dilayani pada angkutan barang, diantaranya :</p>

													<div>
														<h4>Petikemas</h4>
														<div>Paletisasi, Insulated and refrigerated containers, Standard containers, Hard-top containers, Open-top containers, Flatracks, Platforms (plats), Ventilated containers, Bulk containers, Tank containers</div>

														<h4>Barang curah Liquid/ Cair</h4>
														<div>BBM, CPO,Semua bahan kimia cair yang tidak korosif, Minyak goreng, air mineral dan lain-lain</div>

														<h4>Barang curah</h4>
														<div>Batubara, pasir, semen, gula pasir, pupuk, beras, kricak, aspalt, klinker dan lain-lain</div>

														<h4>Barang retail</h4>
														<div>Barang elektronik, hasil produksi pabrik yang sudah terpaket, barang kiriman hantaran, barang potogan</div>

														<h4>Barang packaging</h4>
														<div>Semen, pupuk, gula pasir, beras, paletisasi	</div>
														<div style="margin:15px 0"><button class="orange-button pointer">Selengkapnya &raquo;</button></div>
													</div>

											</td>
											<td style="margin:0 15px;">
												<div style="width:90%;padding:0 20px;">
													<div class="fs18" style="margin:10px 0"><strong><a style="color:#000000;text-decoration:none;" href="http://aset.kereta-api.co.id/">aset.kereta-api.co.id</a></strong></div>
													<p>Untuk melakukan sewa aset PT. Kereta Api Indonesia (Persero) anda mengikuti prosedur sewa sebagai berikut :</p>

													<div>Aset yang dapat disewa terdiri dari Aset Railway dan Aset Non Railway</div>

													<div>Mengajukan permohonan sewa ke Manager komersial di daerah operasi terdekat.</div>

													<div>Menunjukan SPPT PBB terbaru.</div>

													<div>Melakukan Pertemuan untuk Pembahasan rencana sewa-menyewa, Penentuan tarif dan Kesepakatan kerjasama.</div>

													<div>Peninjauan lokasi bersama.Proses kontrak</div>

													<div>Penandatanganan kontrak kerjasama </div>
													
													<div style="margin:15px 0"><button class="orange-button pointer">Selengkapnya &raquo;</button></div>
												</div>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<!-- end main -->

<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>