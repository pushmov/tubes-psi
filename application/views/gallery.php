<?php

	$title['title'] = 'PT KAI Sistem Informasi Tiket - Gallery';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/prettyphoto');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');

?>

		<!-- main -->
		<div id="main" class="main">
			<div class="wrapper clearfix">
				<div class="float-left" style="width:100%">
					<div class="clearfix">
						<div class="cols-1 float-left" style="width:25%;margin-right:20px;">
						<!-- left panel here -->
							<?php $this->load->view('leftpanel'); ?>

						</div>
						<div class="cols-2" style="width:100%">
							<!-- center panel here -->
							<h1 class="page-title">Gallery Foto</h1>
							<div class="gallery kai-gallery clearfix">

								<div style="display:inline-block">
									<div class="kai-gallery-wrapper">
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/gambar/galeri_kereta_argobromo3.jpg">
											<img width="220" height="140" class="radius5" src="<?php echo base_url(); ?>public/images/gambar/galeri_kereta_argobromo3.jpg" >
										</a>
									</div>

									<div class="kai-gallery-wrapper">
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/gambar/galeri_kereta_argojati2.jpg">
											<img width="220" height="140" class="radius5" src="<?php echo base_url(); ?>public/images/gambar/galeri_kereta_argojati2.jpg" >
										</a>
									</div>

									<div class="kai-gallery-wrapper">
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/gambar/galeri_kereta_bali1.jpg">
											<img width="220" height="140" class="radius5" src="<?php echo base_url(); ?>public/images/gambar/galeri_kereta_bali1.jpg" >
										</a>
									</div>
								</div>

								<div style="display:inline-block">
									<div class="kai-gallery-wrapper">
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/gambar/galeri_kereta_lodaya1.jpg">
											<img width="220" height="140" class="radius5" src="<?php echo base_url(); ?>public/images/gambar/galeri_kereta_lodaya1.jpg" >
										</a>
									</div>

									<div class="kai-gallery-wrapper">
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/gambar/galeri_lainnya_balaiyasa1.jpg">
											<img width="220" height="140" class="radius5" src="<?php echo base_url(); ?>public/images/gambar/galeri_lainnya_balaiyasa1.jpg" >
										</a>
									</div>

									<div class="kai-gallery-wrapper">
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/gambar/galeri_lok_cc2061.jpg">
											<img width="220" height="140" class="radius5" src="<?php echo base_url(); ?>public/images/gambar/galeri_lok_cc2061.jpg" >
										</a>
									</div>
								</div>

								<div style="display:inline-block">
									<div class="kai-gallery-wrapper">
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/gambar/galeri_lok_cc2053.jpg">
											<img width="220" height="140" class="radius5" src="<?php echo base_url(); ?>public/images/gambar/galeri_lok_cc2053.jpg" >
										</a>
									</div>

									<div class="kai-gallery-wrapper">
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/gambar/galeri_kereta_argojati2.jpg">
											<img width="220" height="140" class="radius5" src="<?php echo base_url(); ?>public/images/gambar/galeri_kereta_argojati2.jpg" >
										</a>
									</div>

									<div class="kai-gallery-wrapper">
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/gambar/galeri_stasiun_bandung2.jpg">
											<img width="220" height="140" class="radius5" src="<?php echo base_url(); ?>public/images/gambar/galeri_stasiun_bandung2.jpg" >
										</a>
									</div>
								</div>

								<div style="display:inline-block">
									<div class="kai-gallery-wrapper">
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/gambar/galeri_stasiun_jebres1.jpg">
											<img width="220" height="140" class="radius5" src="<?php echo base_url(); ?>public/images/gambar/galeri_stasiun_jebres1.jpg" >
										</a>
									</div>

									<div class="kai-gallery-wrapper">
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/gambar/galeri_stasiun_jebres2.jpg">
											<img width="220" height="140" class="radius5" src="<?php echo base_url(); ?>public/images/gambar/galeri_stasiun_jebres2.jpg" >
										</a>
									</div>

									<div class="kai-gallery-wrapper">
										<a rel="prettyPhoto[gallery2]" href="<?php echo base_url(); ?>public/images/gambar/galeri_stasiun_tugu2.jpg">
											<img width="220" height="140" class="radius5" src="<?php echo base_url(); ?>public/images/gambar/galeri_stasiun_tugu2.jpg" >
										</a>
									</div>
								</div>

								
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<!-- end main -->

<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
