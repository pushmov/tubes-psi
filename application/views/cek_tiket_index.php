<?php 
	
	$title['title'] = 'PT KAI Sistem Informasi Tiket - Cek Tiket';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/datepicker');
	$this->load->view('header/plugin/autosuggest');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');
?>	

		<!-- main -->
		<div id="main" class="main">

			<div class="wrapper clearfix center">
				
				<div class="ticket-box" style="width:90%;margin:0 auto">
						<div class="clear">
							
							
							<div class="clearfix ticket-wrapper left" style="min-height:400px;">
								<table align="center" style="width:100%;">
									<tbody>
										<tr>
											<td colspan="3" style="padding:7px 0;color:#E57918" class="fs28 strong center">CEK STATUS ORDER</td>
										</tr>
										
										<tr>
											<td colspan="3" class="center">
												<div style="background:#eaeaea" class="kereta-api-result fs16">
													<span style="margin:0 20px;">Input Kode Pemesanan</span>
													<input type="text" name="orderid" id="orderid" placeholder="Input kode disini" style="border:1px solid #CCCCCC;margin:0 20px;width:30%">
													<img id="searchnow" src="<?php echo base_url(); ?>public/images/search-icon.png" class="pointer" title="Search Now" style="border:none;vertical-align:middle;">
												</div>
											</td>
										</tr>
										<tr>
											<td colspan="3">
												<div id="preloader" class="center" style="display:none">
													<img src="<?php echo base_url(); ?>public/images/preloader.gif" title="loading" alt="loading" style="vertical-align:middle;width:20px;margin:0 8px;"><span class="fs14 strong">Loading ...</span>
												</div>
												<div id="ajax-response" style="padding:20px;">
												</div>
											</td>
										</tr>
										
									</tbody>
								</table>



							</div>
						</div>
					</div>
				
			</div>
		</div>
		<!-- end main -->

		<script>
			$('#searchnow').click(function(){
				var orderid = $('#orderid').val();
				if(orderid == ''){
					return false;
				}

				$('#preloader').show();

				setTimeout(function(){
					$.ajax({
						url: '<?php echo base_url(); ?>cek_tiket/ajax_search/'+encodeURIComponent(orderid)+'/' ,
						cache: false,
						success: function(response){
							$('#preloader').hide();
							$('#ajax-response').html(response);
						}
					});	
				},2000);

				
			});
		</script>
		
<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
