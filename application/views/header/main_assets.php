	
	
	
	<script src="<?php echo base_url(); ?>public/js/jquery-1.4.4.min.js"></script>
	<script src="<?php echo base_url(); ?>public/js/jquery-ui.js"></script>

	<link href="<?php echo base_url(); ?>public/css/styles.css" rel="stylesheet" type="text/css">
	
	
	<link rel="shortcut icon" href="<?php echo base_url(); ?>public/images/favicon.png" />
	<script>
	var d = document;
	var safari = (navigator.userAgent.toLowerCase().indexOf('safari') != -1) ? true : false;
	var gebtn = function(parEl,child) { return parEl.getElementsByTagName(child); };
	onload = function() {
	    
	    var body = gebtn(d,'body')[0];
	    body.className = body.className && body.className != '' ? body.className + ' has-js' : 'has-js';
	    
	    if (!d.getElementById || !d.createTextNode) return;
	    var ls = gebtn(d,'label');
	    for (var i = 0; i < ls.length; i++) {
	        var l = ls[i];
	        if (l.className.indexOf('label_') == -1) continue;
	        var inp = gebtn(l,'input')[0];
	        if (l.className == 'label_check') {
	            l.className = (safari && inp.checked == true || inp.checked) ? 'label_check c_on' : 'label_check c_off';
	            l.onclick = check_it;
	        };
	        if (l.className == 'label_radio') {
	            l.className = (safari && inp.checked == true || inp.checked) ? 'label_radio r_on' : 'label_radio r_off';
	            l.onclick = turn_radio;
	        };
	    };
	};
	var check_it = function() {
	    var inp = gebtn(this,'input')[0];
	    if (this.className == 'label_check c_off' || (!safari && inp.checked)) {
	        this.className = 'label_check c_on';
	        if (safari) inp.click();
	    } else {
	        this.className = 'label_check c_off';
	        if (safari) inp.click();
	    };
	};
	var turn_radio = function() {
	    var inp = gebtn(this,'input')[0];
	    if (this.className == 'label_radio r_off' || inp.checked) {
	        var ls = gebtn(this.parentNode,'label');
	        for (var i = 0; i < ls.length; i++) {
	            var l = ls[i];
	            if (l.className.indexOf('label_radio') == -1)  continue;
	            l.className = 'label_radio r_off';
	        };
	        this.className = 'label_radio r_on';
	        if (safari) inp.click();
	    } else {
	        this.className = 'label_radio r_off';
	        if (safari) inp.click();
	    };
	};
	</script>
	<script>
		$(document).ready(function(){
			$("#accordian h3").click(function(){
				//slide up all the link lists
				$("#accordian ul ul").slideUp();
				//slide down the link list below the h3 clicked - only if its closed
				if(!$(this).next().is(":visible"))
				{
					$(this).next().slideDown();
				}
			});

			$('#accordian ul ul li a').hover(function(){

				$(this).append('<span class="raquo">&raquo;</span>').fadeIn('slow');
			},

			function(){
				$('.raquo').remove();
			});
		});



		
	</script>