		
		
		<!-- header -->
		<div id="head">
			<div class="wrapper header clearfix">
				<div class="float-left"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>public/images/logo.png" title="PT Kereta Api Indonesia" alt="logo"></a></div>
				<div class="search-form">
					<form>
						<input type="text" placeholder="Enter keyword here" style="width:60%">
						<div class="search_btn pointer"><img src="<?php echo base_url(); ?>public/images/search.png" style="margin-right:5px;">Search</div>
						<a class="help_btn pointer">?</a>
					</form>
				</div>

				<?php if($this->session->userdata('logged_in_psi')) : ?>
				<div class="float-right">
					<ul class="rightnavi">
						<li class="underline">Logged in as user</li>
						<li class="underline show-confirm">Logout</li>
					</ul>
				</div>
				<?php else : ?>
				<div class="float-right">
					<ul class="rightnavi">
						<li><a href="<?php echo base_url(); ?>sitemap/" style="color:#333333;text-decoration:none">Sitemap</a></li>
						<li><a href="<?php echo base_url(); ?>login/" style="color:#333333;text-decoration:none;display:none">Login</a></li>
					</ul>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<div id="navigation" class="navigation">
			<div class="nav-1 center">
				<ul class="navi">
					<li class="pointer"><a href="<?php echo base_url(); ?>">Home</a></li>
					<li class="pointer">Tentang Kami</li>
					<li class="pointer"><a href="<?php echo base_url(); ?>layanan-produk/">Layanan Produk</a></li>
					<li class="pointer">Informasi Media</li>
					<li class="pointer"><a class="white" href="<?php echo base_url(); ?>gallery/">Gallery Foto</a></li>
					<li class="pointer">Heritage Railway</li>
					<li class="pointer"><a class="white" href="<?php echo base_url(); ?>cek_tiket/">Cek Tiket</a></li>
				</ul>
			</div>
			<div class="nav-2">
				<div class="wrapper">
					<p class="newsticker"><span class="fs12 newbox radius5">NEW!</span>News ticker</p>
				</div>
			</div>
		</div>
		<!-- end header -->