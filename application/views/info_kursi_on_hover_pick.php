				<div class="center" style="margin:20px 0;">
					<div class="strong" style="color:#46B525">Kursi tersedia.</div>
					<a href="javascript:void(0)" id="<?php echo $edit_id_penumpang; ?>" style="color:#FFFFFF !important" class="btn btn-success"><span class="icon-ok icon-white"></span> Pick</a>
					<div id="preloader"></div>
				</div>

				<script>
					$('.btn-success').click(function(){
						var id_penumpang = $('.btn-success').attr('id');
						var seat_pick = $('.seat-clicked').attr('alt');
						

						$(this).hide();
						$('#preloader').html('Updating ... <img src="<?php echo base_url(); ?>public/images/preloader.gif" style="height:16px;">');

						setTimeout(function(){

							$.ajax({
								url: '<?php echo base_url(); ?>order/ajax_edit_kursi/'+id_penumpang+'/'+seat_pick,
								cache: false,
								success: function(response){
									var data = JSON.parse(response);
									if(data.success == 'success'){
										$('#preloader').html('<span class="icon-ok"></span>success');
									}
								}
							});

							
							setTimeout(function(){
								$('.close-reveal-modal').click();
								setTimeout(function(){
									location.reload();
								},1000);
							},1000);
						},2000);
					});
				</script>