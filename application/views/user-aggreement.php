<?php 
	
	$title['title'] = 'PT KAI Sistem Informasi Tiket - Index';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/datepicker');
	$this->load->view('header/plugin/autosuggest');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');
?>	

		<!-- main -->
		<div id="main" class="main">

			<div class="center" style="margin-bottom:30px;">
				<div id="crumbs" class="center">
					<ul>
						<li><a href="javascript:void(0)">1. Info Jadwal </a></li>
						<li><a href="javascript:void(0)">2. Pilih KA</a></li>
						<li><a class="current" href="javascript:void(0)">3. Info Booking</a></li>
						<li><a href="javascript:void(0)">4. Isi Data</a></li>
						<li><a href="javascript:void(0)">5. Pembayaran</a></li>
						<li><a href="javascript:void(0)">6. Konfirmasi</a></li>
					</ul>
				</div>
			</div>

			<div class="wrapper clearfix center">
				<div class="ticket-box" style="width:90%;margin:0 auto">
						<div class="clear">
							<div class="clearfix ticket-wrapper left">
								<table align="center" style="width:100%;">
									<tbody>
										<tr>
											<td colspan="3" style="padding:7px 0;color:#E57918" class="fs28 strong center">RESERVASI KERETA API</td>
										</tr>
										
										<tr>
											<td colspan="3" class="center"><div style="background:#eaeaea" class="kereta-api-result fs16">Informasi Reservasi Kereta Api</div></td>
										</tr>
										<tr>
											<td style="width:20%;padding:8px 20px">Nama Kereta Api</td>
											<td style="width:1%">:</td>
											<td><?php echo $kereta_api->NamaKeretaApi; ?></td>
										</tr>
										<tr>
											<td style="width:15%;padding:8px 20px">Tanggal</td>
											<td style="width:1%">:</td>
											<td><?php echo date('D, d/M/Y',strtotime($tanggal_berangkat)); ?></td>
										</tr>
										<tr>
											<td style="width:15%;padding:8px 20px">Berangkat</td>
											<td style="width:1%">:</td>
											<td><?php echo $stasiun_from->NamaStasiun.', '.$stasiun_from->Kota; ?></td>
										</tr>
										<tr>
											<td style="width:15%;padding:8px 20px">Datang</td>
											<td style="width:1%">:</td>
											<td><?php echo $stasiun_to->NamaStasiun.', '.$stasiun_to->Kota; ?></td>
										</tr>
										<tr>
											<td style="width:15%;padding:8px 20px"></td>
											<td style="width:1%"></td>
											<td></td>
										</tr>
										<tr>
											<td style="width:15%;padding:8px 20px">Kelas</td>
											<td style="width:1%">:</td>
											<td><?php echo ucwords($kereta_api->Kelas); ?></td>
										</tr>
										<tr>
											<td style="width:15%;padding:8px 20px">Tarif</td>
											<td style="width:1%">:</td>
											<td><?php echo 'Rp '.number_format($this->session->userdata('tarif')); ?></td>
										</tr>
										<tr>
											<td style="width:15%;padding:8px 20px"></td>
											<td style="width:1%"></td>
											<td></td>
										</tr>
										<tr>
											<td style="width:15%;padding:8px 20px">Total Penumpang</td>
											<td style="width:1%">:</td>
											<td><?php echo $total_penumpang; ?></td>
										</tr>
										
										<tr>
											<td colspan="3" class="center"><div style="background:#eaeaea" class="kereta-api-result fs16">Ketentuan Pembayaran</div></td>
										</tr>

										<tr>
											<td colspan="3" style="padding:8px 20px">
												<div style="color:#FF0000">
													Reservasi dapat dilakukan 2x24 jam sebelum kereta berangkat
												</div>
											</td>
										</tr>
										<tr>
											<td colspan="3" style="padding:8px 20px">
												<div style="color:#FF0000">
													Harga dan ketersediaan tempat duduk sewaktu waktu dapat berubah
												</div>
											</td>
										</tr>
										<tr>
											<td colspan="3" style="padding:8px 20px">
												<div style="color:#FF0000">
													Pastikan anda telah menerima email konfirmasi pembayaran dari PT. Kereta Api Indonesia (Persero) untuk ditukarkan dengan tiket di stasiun online
												</div>
											</td>
										</tr>
										<tr>
											<td colspan="3" style="padding:8px 20px">
												<div>
													<label class="label_check" id="agreement-chck" for="sample-check">
													    <input name="sample-check" value="1" type="checkbox" />
													    Dengan ini saya setuju dan mematuhi persyaratan dan ketentuan Reservasi dari PT. Kereta Api Indonesia (Persero), termasuk pembayaran dan mematuhi semua aturan dan pembatasan mengenai ketersediaan tarif atau jasa
													</label>
												</div>
											</td>
										</tr>

										<tr>
											<td colspan="3" class="strong pointer center" data-reveal-id="myModal" style="padding:15px 0;text-decoration:underline;color:#000000">Detil Syarat dan Ketentuan Reservasi</td>
										</tr>

										
									</tbody>
								</table>
								<div class="center"><button id="lanjutkan" class="orange-button pointer">Lanjutkan &raquo;</button></div>

		<div id="myModal" class="reveal-modal">
			<h3 style="margin:15px 0">PERSYARATAN DAN KETENTUAN ANGKUTAN PENUMPANG KERETA API</h3>
			<table>
				<tr>
					<td style="width:1%;padding:0 5px;">1.</td>
					<td>Dengan ini Anda menyatakan persetujuan terhadap Persyaratan dan Ketentuan Angkutan Penumpang Kereta Api termasuk tidak terbatas ketentuan reservasi</td>
				</tr>
				<tr>
					<td style="width:1%;padding:0 5px;">2.</td>
					<td>Anda akan mematuhi persyaratan dan ketentuan Reservasi , termasuk pembayaran, mematuhi semua aturan dan pembatasan mengenai ketersediaan tarif serta bertanggung jawab untuk semua biaya yang timbul dari penggunaan fasilitas Reservasi Online Tiket Kereta Api</td>
				</tr>
				<tr>
					<td style="width:1%;padding:0 5px;">3.</td>
					<td>PT Kereta Api Indonesia (Persero) berhak atas kebijaksanaan untuk mengubah, menyesuaikan, menambah atau menghapus salah satu syarat dan kondisi yang tercantum di sini, dan / atau mengubah, menangguhkan atau menghentikan setiap aspek dari Reservasi  Online Tiket Kereta Api. PT Kereta Api Indonesia (Persero) tidak diwajibkan untuk menyediakan pemberitahuan sebelum memasukkan salah satu perubahan di atas dan / atau modifikasi ke dalam Reservasi  Online Tiket Kereta Api</td>
				</tr>
				<tr>
					<td style="width:1%;padding:0 5px;">4.</td>
					<td>PT Kereta Api Indonesia (Persero) akan menggunakan informasi pribadi yang Anda berikan melalui Fasilitas ini hanya untuk tujuan reservasi dan pencatatan database pelanggan PT Kereta Api Indonesia (Persero)  termasuk tidak terbatas untuk penghitungan poin dalam program Kereta Api Frequent Passenger</td>
				</tr>
				<tr>
					<td style="width:1%;padding:0 5px;">5.</td>
					<td>Anda tidak dibenarkan menggunakan fasilitas ini untuk tujuan yang melanggar hukum atau dilarang, termasuk tetapi tidak terbatas untuk membuat reservasi yang tidak sah, spekulatif, palsu atau penipuan atau menjualnya kembali secara tidak sah. PT Kereta Api Indonesia (Persero) dapat membatalkan atau menghentikan penggunaan Anda atas fasilitas ini setiap saat tanpa pemberitahuan jika dicurigai</td>
				</tr>
				<tr>
					<td style="width:1%;padding:0 5px;">6.</td>
					<td>PT Kereta Api Indonesia (Persero) tidak menjamin bahwa Reservasi  Online Tiket Kereta Api akan bebas kesalahan, bebas dari virus atau elemen lain yang berbahaya</td>
				</tr>
				<tr>
					<td style="width:1%;padding:0 5px;">7.</td>
					<td>Syarat dan ketentuan fasilitas ini diatur dan ditafsirkan sesuai peraturan internal PT Kereta Api Indonesia (Persero) dan peraturan perundang-undangan yang berlaku di Republik Indonesia</td>
				</tr>
			</table>
			<div>
				<a class="close-reveal-modal">&#215;</a>
			</div>
		</div>

							</div>
						</div>
					</div>
			</div>
		</div>
		<!-- end main -->
		<script>
		$(document).ready(function(){


		});

		$('#lanjutkan').click(function(){

			if(!$('#agreement-chck').hasClass('c_on')){
				alert('Anda Belum Menyatakan Persetujuan Mengenai Aturan');
				return false;
			} else {
				window.location = '<?php echo base_url(); ?>order/register/<?php echo $this->uri->segment(3); ?>';
			}
		});
		</script>
<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
