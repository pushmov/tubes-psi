<?php 
	
	$title['title'] = 'PT KAI Sistem Informasi Tiket - Index';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/datepicker-2');
	$this->load->view('header/plugin/autosuggest');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');
?>	

		<!-- main -->
		<div id="main" class="main">
			
			<div class="center" style="margin-bottom:30px;">
				<div id="crumbs" class="center">
					<ul>
						<li><a href="javascript:void(0)">1. Info Jadwal </a></li>
						<li><a href="javascript:void(0)">2. Pilih KA</a></li>
						<li><a href="javascript:void(0)">3. Info Booking</a></li>
						<li><a class="current" href="javascript:void(0)">4. Isi Data</a></li>
						<li><a href="javascript:void(0)">5. Pembayaran</a></li>
						<li><a href="javascript:void(0)">6. Konfirmasi</a></li>
					</ul>
				</div>
			</div>

			<div class="wrapper clearfix center">
				<form action="<?php echo base_url(); ?>order/process_pembayaran/" method="POST" id="pembayaran" name="pembayaran">
				<div class="ticket-box" style="width:90%;margin:0 auto">
						<div class="clear">
							
							
							<div class="clearfix ticket-wrapper left">
								<table align="center" style="width:100%;">
									<tbody>
										<tr>
											<td colspan="3" style="padding:7px 0;color:#E57918" class="fs28 strong center">RESERVASI TIKET ONLINE</td>
										</tr>
										
										<tr>
											<td colspan="3" class="center"><div style="background:#eaeaea" class="kereta-api-result fs16">Info Perjalanan</div></td>
										</tr>
										<tr>
											<td style="padding:8px 20px;width:25%">ID Kereta Api</td>
											<td style="width:1%">:</td>
											<td><?php echo $kereta->IDKeretaApi; ?></td>
											<input type="hidden" name="idkeretaapi" value="<?php echo $kereta->IDKeretaApi; ?>">
										</tr>
										<tr>
											<td style="padding:8px 20px;width:25%">Nama Kereta Api</td>
											<td style="width:1%">:</td>
											<td><?php echo $kereta->NamaKeretaApi; ?></td>
										</tr>
										<tr>
											<td style="padding:8px 20px;width:25%">Waktu Berangkat</td>
											<td style="width:1%">:</td>
											<td><?php echo date('D, d/M/Y',strtotime($tanggal_berangkat)). ' '. $jadwal->JamBerangkat; ?> WIB</td>
											<input type="hidden" name="idjadwal" value="<?php echo $jadwal->IDJadwal; ?>">
										</tr>
										<tr>
											<td style="padding:8px 20px;width:25%">Waktu Tiba</td>
											<td style="width:1%">:</td>
											<td><?php echo (strtotime($jadwal->JamSampai) < strtotime($jadwal->JamBerangkat)) ? date('D, d/M/Y',strtotime($tanggal_berangkat . "tomorrow")). ' '. $jadwal->JamSampai : date('D, d/M/Y',strtotime($tanggal_berangkat)). ' '. $jadwal->JamSampai; ?> WIB</td>
										</tr>
										<tr>
											<td style="padding:8px 20px;width:25%">Rute</td>
											<td style="width:1%">:</td>
											<td><?php echo $from->Kota . ' - '.$to->Kota; ?></td>
											
										</tr>
										<tr>
											<td style="padding:8px 20px;width:25%">Jumlah Penumpang</td>
											<td style="width:1%">:</td>
											<td><?php echo $total_penumpang; ?></td>
											<input type="hidden" name="jmlpenumpang" value="<?php echo $total_penumpang; ?>">
										</tr>
										<tr>
											<td style="padding:8px 20px;width:25%">Kelas</td>
											<td style="width:1%">:</td>
											<td><?php echo ucwords($kereta->Kelas); ?></td>
										</tr>
										
										<tr>
											<td colspan="3" class="center"><div style="background:#eaeaea" class="kereta-api-result fs16">Data Penumpang</div></td>
										</tr>

										<tr>
											<td colspan="3">
												<table style="width:100%">
													<tr>
														<td style="padding:8px 20px;width:4%" class="strong">No.</td>
														<td style="padding:8px 20px;width:48%" class="strong">Nama <span style="color:#FF0000">*</span></td>
														<td style="padding:8px 20px;width:48%" class="strong">ID KTP/SIM <span style="color:#FF0000">*</span></td>
													</tr>

													<?php for($i=1;$i<=$total_penumpang;$i++) : ?>
													<tr>
														<td style="padding:8px 20px;"><?php echo $i; ?>. </td>
														<td style="padding:8px 20px;"><input autocomplete="off" name="namapenumpang[]" class="penumpang" style="border:1px solid #989EA4;width:85%;" type="text"></td>
														<td style="padding:8px 20px;"><input autocomplete="off" name="idpenumpang[]" class="idpenumpang" style="border:1px solid #989EA4;width:85%;" type="text"></td>
													</tr>
													<tr>
														<td style="padding:8px 20px;"></td>
														<td style="padding:8px 20px;">
															<span style="margin:0 8px;"><input type="radio" name="jeniskelamin[]" value="P" checked><span style="margin:0 5px;">Pria</span></span>
															<input type="radio" name="jeniskelamin[]" value="W"><span style="margin:0 5px;">Wanita</span> <span style="color:#FF0000;margin:0 5px;">(optional)</span></td>
														<td style="padding:8px 20px;">
															<input type="text" style="border:1px solid #989EA4;width:85%" class="datepicker" name="dob[]" placeholder="Tanggal Lahir (optional)">
														</td>
													</tr>
													<?php endfor; ?>
												</table>
											</td>
										</tr>

										<tr>
											<td colspan="3" class="center">
												<div style="background:#eaeaea" class="kereta-api-result fs16">Data Pemesan & Registrasi Email</div>
												<?php if($this->session->flashdata('email') == 'registered') :?>
												
												<?php endif; ?>	
											</td>
										</tr>

										<tr>
											<td style="padding:8px 20px;width:25%">Nama Pemesan <span style="color:#FF0000">*</span></td>
											<td style="width:1%">:</td>
											<td><input autocomplete="off" name="nama_pemesan" id="nama_pemesan" style="border:1px solid #989EA4;width:85%;" type="text"></td>
										</tr>
										<tr>
											<td style="padding:8px 20px;width:25%">Email <span style="color:#FF0000">*</span></td>
											<td style="width:1%">:</td>
											<td><input onblur="checkemail()" autocomplete="off" name="email_pemesan" id="email_pemesan" style="border:1px solid #989EA4;width:85%;" type="text"></td>
										</tr>

										<tr id="password" style="display:none">
											<td style="padding:8px 20px;width:25%">Password <span style="color:#FF0000">*</span></td>
											<td style="width:1%">:</td>
											<td><input autocomplete="off" name="password_pemesan" id="password_pemesan" style="border:1px solid #989EA4;width:85%;" type="password"></td>
										</tr>
										<tr id="confirm" style="display:none">
											<td style="padding:8px 20px;width:25%">Konfirmasi Password <span style="color:#FF0000">*</span></td>
											<td style="width:1%">:</td>
											<td><input autocomplete="off" name="password_konfirm" id="password_konfirm" style="border:1px solid #989EA4;width:85%;" type="password"></td>
										</tr>


										<tr>
											<td style="padding:8px 20px;width:25%">No. Telp <span style="color:#FF0000">*</span></td>
											<td style="width:1%">:</td>
											<td><input autocomplete="off" name="telp_pemesan" id="telp_pemesan" style="border:1px solid #989EA4;width:85%;" type="text"></td>
										</tr>
										<tr>
											<td valign="center" style="padding:8px 20px;width:25%">Alamat <span style="color:#FF0000">*</span></td>
											<td style="width:1%">:</td>
											<td><textarea name="alamat_pemesan" id="alamat_pemesan" style="margin:8px 0;padding:5px;border:1px solid #989EA4;width: 620px; height: 122px;"></textarea></td>
										</tr>
										
									</tbody>
								</table>
								<div class="clearfix" style="margin:50px 200px;">
									<div class="float-left"><button id="back" class="orange-button pointer">&laquo; Kembali</button></div>
									<div class="float-right"><button id="next" onclick="return validate()" class="orange-button pointer">Lanjutkan &raquo;</button></div>
								</div>


							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- end main -->
		<script>
			function checkemail(){
				var email = $('#email_pemesan').val();
				$.ajax({
					url: '<?php echo base_url(); ?>member/checkemail/'+encodeURIComponent(email)+'/' ,
					cache: false,
					success: function(response){
						var json = response,
						obj = JSON.parse(response);

						if(obj.success == 'empty'){
							
							$('#password').show('slow');
							$('#confirm').show('slow');
						} else if(obj.success == 'registered'){
							$('#password_pemesan').val(obj.password);
							$('#password_konfirm').val(obj.password);
						}

					}
				});
			}
			$(document).ready(function(){



			});

			function validate(){
				var valid = true;
				var intRegex = /^\d+$/;
				$('.penumpang').each(function(){
					if($(this).val() == ''){
						alert('Data penumpang (nama dan ID pengenal) tidak boleh kosong');
						$(this).focus();

						valid = false;
						return false;
					}

				});
				if(!valid){
					return valid;
				}

				$('.idpenumpang').each(function(){
					if($(this).val() == ''){
						alert('Data penumpang (nama dan ID pengenal) tidak boleh kosong');
						$(this).focus();
						$(this).select();

						valid = false;
						return false;
					}
					if(!intRegex.test($(this).val()) || $(this).val().length < 16){
						alert('ID tidak valid');
						$(this).focus();
						valid = false;
						return false;
					}
				});

				if(valid){



				var nama = $('#nama_pemesan').val();
				var email = $('#email_pemesan').val();
				var password = $('#password_pemesan').val();
				var confirm_password = $('#password_konfirm').val();
				var telp = $('#telp_pemesan').val();
				var alamat = $('#alamat_pemesan').val();
				var reg1 = /(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/; // not valid    
				var reg2 = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/; // valid    

				if(nama == ''){
					alert('nama pemesan wajib diisi');
					$('#nama_pemesan').focus();
					return false;
				}
				if(email == ''){
					alert('email pemesan wajib diisi');
					valid = false;
					$('#email_pemesan').focus();
					return false;
				}
				if (!reg1.test(email) && reg2.test(email)){}
				else {
					alert("\"" + email + "\" bukan email yg valid"); // this is also optional
					$('#email_pemesan').focus();
					valid = false;
					return false;
				}

				if(password == ''){
					alert('password wajib diisi');
					$('#password_pemesan').focus();
					return false;
				}

				if(confirm_password == ''){
					alert('konfirmasi password anda');
					$('#password_konfirm').focus();
					return false;
				}

				if(password != confirm_password){
					alert('password tidak sama. ulangi lagi');
					$('#password_konfirm').focus();
					return false;
				}

				if(telp == ''){
					alert('nomor telpon pemesan wajib diisi');
					valid = false;
					$('#telp_pemesan').focus();
					return false;
				}

				if(!intRegex.test(telp)){
					alert('format nomor telpon salah');
					valid = false;
					$('#telp_pemesan').focus();
					return false;	
				}
				if(alamat == ''){
					alert('alamat pemesan wajib diisi');
					valid = false;
					$('#alamat_pemesan').focus();
					return false;
				}

				}
				return valid;
			};

			$('#back').click(function(){
				window.location = '<?php echo base_url(); ?>order/agreement/<?php echo $this->uri->segment(3); ?>/';
			})
		</script>

<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
