<?php 
	
	$title['title'] = 'PT KAI Sistem Informasi Tiket - Sitemap';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/datepicker');
	$this->load->view('header/plugin/autosuggest');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');
?>	

		<!-- main -->
		<div id="main" class="main">
			<div class="wrapper clearfix center">
				<div style="margin:40px auto;border:1px solid #CCCCCC;width:720px;padding:20px;">
					<h2 class="left" style="margin:20px 10px;color:#333333">Sitemap</h2>
					<table style="width:100%;">
						<tr>
							<td>a</td>
							<td>v</td>
							<td>c</td>
							<td>c</td>
						</tr>
						<tr>
							<td>s</td>
							<td>s</td>
							<td>s</td>
							<td>c</td>
						</tr>
						<tr>
							<td>a</td>
							<td>s</td>
							<td>c</td>
							<td>c</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<!-- end main -->
		
<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
