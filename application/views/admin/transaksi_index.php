<?php $this->load->view('admin/header'); ?>
		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>admin/">Home</a>
			</div>
			<div class="heading">
				Data Transaksi (completed)
			</div>

			
			<div class="main">
				<?php if($this->session->flashdata('message')) : ?>
				<div style="color:#2E842F" class="fs18"><?php echo $this->session->flashdata('message'); ?></div>
				<?php endif; ?>

				<div>
					<form action="<?php echo base_url(); ?>admin_transaksi/search/1" method="POST" name="filter" id="filter">
						Filter : 
						<input placeholder="From Date" class="datepicker" name="start_date" id="start_date" type="text">
							<span style="margin-right:8px" class="icon-calendar"></span> 
						<input placeholder="To Date" class="datepicker" name="end_date" id="end_date" type="text">
							<span style="margin-right:8px" class="icon-calendar"></span>
						<span class="icon-search pointer" id="search" ></span>
					</form>
				</div>
				<div style="display:none;color:#010EBF" id="loader">
					<img style="height:16px;margin:0 5px;" src="<?php echo base_url(); ?>public/images/preloader.gif">searching...
				</div>

				<div id="ajax-response">
					<div class="fs14" style="margin:10px 0;color:#FF0000">Total Records : <?php echo $total_record; ?></div>
					<table class="maintable">
						<thead>
							<tr>
								<th>Status</th>
								<th>Kode Transaksi</th>
								<th>Tanggal Order</th>
								<th>Penumpang</th>
								<th>Tarif Satuan</th>
								<th>Layanan Pelanggan</th>
								<th>Tarif Total</th>
							</tr>
						</thead>
						<tbody>

							<?php if(!empty($result)) : ?>
							<?php $total_rp = 0; ?>
							<?php foreach($result as $row) : ?>
							<?php $row_total = 0; ?>
							<?php $row_total += ($row->Tarif * $row->TotalPenumpang) + 7500; ?>
							<?php $total_rp += $row_total; ?>
							<tr>
								<td class="center" style="color:#71C39A"><span class="icon-ok"></span> verified</td>
								<td class="left"><?php echo $row->OrderNumber; ?></td>
								<td class="left"><?php echo date('d/M/Y  H:i:s',strtotime($row->TanggalOrder)); ?></td>
								<td class="center"><?php echo $row->TotalPenumpang; ?></td>
								<td class="right"><?php echo 'Rp '. number_format($row->Tarif); ?></td>
								<td class="right"><?php echo 'Rp '.number_format(7500); ?></td>
								<td class="right"><?php echo 'Rp '.number_format($row_total); ?></td>
							</tr>
							<?php endforeach; ?>
							<tr>
								<td colspan="6" class="right strong" style="padding:5px 8px">TOTAL</td>
								<td colspan="1" class="right strong" style="padding:5px 8px"><?php echo 'Rp '.number_format($total_rp); ?></td>
							</tr>
							<?php else : ?>
							<tr>
								<td colspan="6">No records found</td>
							</tr>
							<?php endif; 	?>
							
						</tbody>
					</table>
					<div class="pagination"><?php echo $page; ?></div>
				</div>
			</div>
			<script>
				$('#search').click(function(){
					$('#loader').show();
					var form = $('#filter');
					var serialize = form.serialize();

					setTimeout(function(){
						$.ajax({
							cache: false,
							url: form.attr('action'),
							data: serialize,
							type: "POST",
							success:function(response){
								$('#loader').hide();
								$('#ajax-response').html(response);							
							}
						});	
					},2000)
					


					return false;
				});
			</script>
<?php $this->load->view('admin/footer'); ?>