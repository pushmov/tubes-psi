				<table class="maintable">
					<thead>
						<tr>
							<th>Nomor KA</th>
							<th>Nama KA</th>
							<th>Kelas</th>
							<th>Jumlah Gerbong</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>

						<?php if(!empty($row)) : ?>
						<tr>
							<td class="center"><?php echo $row->IDKeretaApi; ?></td>
							<td class="center"><?php echo $row->NamaKeretaApi; ?></td>
							<td class="center"><?php echo $row->Kelas; ?></td>
							<td class="center"><?php echo $row->JumlahGerbong; ?></td>
							<td class="center">
								<a href="<?php echo base_url(); ?>admin_ka/edit/<?php echo $row->IDKeretaApi; ?>/"><span style="margin:0 8px" class="icon-edit"></span></a>
								<a href="<?php echo base_url(); ?>admin_ka/delete/<?php echo $row->IDKeretaApi; ?>/"><span style="margin:0 8px" class="icon-remove"></span></a>
							</td>
						</tr>
						<?php else : ?>
						<tr>
							<td colspan="5">No records found</td>
						</tr>
						<?php endif; 	?>
						
					</tbody>
				</table>