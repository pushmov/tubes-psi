				<table class="maintable">
					<thead>
						<tr>
							<th>Nomor Order</th>
							<th>Tanggal Order</th>
							<th>Tanggal Berangkat</th>
							<th>Jumlah Penumpang</th>
							<th>Verifikasi</th>
						</tr>
					</thead>
					<tbody>

						<?php if(!empty($result)) : ?>
						<?php foreach($result as $row) : ?>
						<tr>
							<td><?php echo $row->OrderNumber; ?></td>
							<td><?php echo date('D, d/M/Y H:i:s',strtotime($row->TanggalOrder)); ?></td>
							<td><?php echo date('D, d/M/Y',strtotime($row->TanggalBerangkat)); ?></td>
							<td class="center"><?php echo $row->TotalPenumpang; ?> <span class="tampilkan underline pointer" style="color:#0011FF;margin:0 5px;">Tampilkan penumpang</span></td>
							<td class="center">
								<?php if($row->IsVerified) : ?>
								Verified
								<?php else : ?>

									<?php if(strtotime(date('Y-m-d H:i:s')) >= strtotime($row->TanggalBerangkat)) : ?>
									<span style="color:#FF0000">Expired</span>
									<?php else : ?>
									<a href="<?php echo base_url(); ?>admin_order/setverified/<?php echo $row->OrderID; ?>/" style="color:#FFFFFF" class="btn btn-success"><span class="icon-ok"></span> Verifikasi Sekarang</a>
									<?php endif; ?>
									
								<?php endif; ?>	
							</td>
						</tr>
						<?php endforeach; ?>
						<?php else : ?>
						<tr>
							<td colspan="5"><span style="color:#FF0000">Result not found</span></td>
						</tr>
						<?php endif; 	?>
						
					</tbody>
				</table>

				<div id="detail-penumpang" style="display:none">
					<table class="maintable">
						<thead>
							<tr>
								<th>Nomor Order</th>
								<th>Nama Penumpang</th>
								<th>ID Pengenal</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($penumpang as $row) : ?>
							<tr>
								<td><?php echo $row->OrderID; ?></td>
								<td><?php echo $row->NamaPenumpang; ?></td>
								<td><?php echo $row->IDPengenal; ?></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>

				<script>
					$('.tampilkan').click(function(){
						$('#detail-penumpang').toggle();
					});
				</script>