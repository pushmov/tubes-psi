				<table class="maintable">
					<thead>
						<tr>
							<th>Nomor Order</th>
							<th>Tanggal Order</th>
							<th>Tanggal Berangkat</th>
							<th>Nama Penumpang</th>
							<th>ID Pengenal</th>
							<th>Nomer Kursi</th>
						</tr>
					</thead>
					<tbody>

						<?php if(!empty($result)) : ?>
						<?php foreach($result as $row) : ?>
						<?php foreach($row->Penumpang as $rowpenumpang) : ?>
						<tr>
							<td><?php echo $row->OrderNumber; ?></td>
							<td><?php echo date('D, d/M/Y H:i:s',strtotime($row->TanggalOrder)); ?></td>
							<td><?php echo date('D, d/M/Y',strtotime($row->TanggalBerangkat)); ?></td>
							<td class="center"><?php echo $rowpenumpang->NamaPenumpang; ?></td>
							<td class="center"><?php echo $rowpenumpang->IDPengenal; ?></td>
							<td class="center"><?php echo $rowpenumpang->NomorGerbong . $rowpenumpang->NomorKursi; ?> <a alt="<?php echo $rowpenumpang->IDPenumpang; ?>" href="javascript:void(0)" data-reveal-id="myModal" class="underline idpenumpang" style="color:#71C39A !important">edit</a></td>
						</tr>
						<?php endforeach; ?>
						<?php endforeach; ?>
						<?php else : ?>
						<tr>
							<td colspan="6"><span style="color:#FF0000">Result not found</span></td>
						</tr>
						<?php endif; 	?>
						
					</tbody>
				</table>

				<span id="edit-id-penumpang" alt=""></span>
				<div id="detail-penumpang" style="display:none">
					<table class="maintable">
						<thead>
							<tr>
								<th>Nomor Order</th>
								<th>Nama Penumpang</th>
								<th>ID Pengenal</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($penumpang as $row) : ?>
							<tr>
								<td><?php echo $row->OrderID; ?></td>
								<td><?php echo $row->NamaPenumpang; ?></td>
								<td><?php echo $row->IDPengenal; ?></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<span id="id_kereta_api" alt="<?php echo $id_kereta_api; ?>"></span>



				<script>
					$(function() {
						$( document ).tooltip();
					});
					$('.tampilkan').click(function(){
						$('#detail-penumpang').toggle();
					});

					$('.select-gerbong').click(function(){
						var nomergerbong = $(this).attr('alt');
							
						$('#display-gerbong').fadeOut('slow');
						$('#display-kursi-'+nomergerbong).fadeIn('slow');
						$('#back').show();
						$('#info-box').show();
					});

					$('.select-seat').click(function(){
						$('.select-seat').removeClass('seat-clicked');
						$(this).addClass('seat-clicked');
					})

					$('#tooltip').tipsy({delayIn: 500,fallback: getTitleData, html:true });

					function getTitleData(){
						var data;
						$.ajax({
							url: '<?php echo base_url(); ?>admin_order/load_tooltip/',
							cache: false,
							success: function(response){
								data = response;
							}
						});
						
						return data;
					}

					$('.idpenumpang').click(function(){
						$('#edit-id-penumpang').attr('alt',$(this).attr('alt'));
					});

					
				</script>
				

				<div id="myModal" class="reveal-modal">
					<h1>Pindah Kursi : </h1>
					<div id="display-gerbong">
						<table style="width:100%;" align="center">
							<tr>
								<?php for($i=1;$i<=$jumlahgerbong;$i++) : ?>
								<td class="center" style="padding:10px 8px">
									<div style="cursor:pointer" >
										<img src="<?php echo base_url(); ?>public/images/gerbong.jpg">
										<div alt="<?php echo $i; ?>" class="select-gerbong strong fs16"><?php echo substr(strtoupper($kelas),0,3).'-'.$i; ?></div>
									</div>
								</td>
								<?php if($i % 2 == 0) : ?>
								</tr><tr>
								<?php endif; ?>
								<?php endfor; ?>
						</table>
					</div>

					<div style="margin:10px 0">
						<?php for($i=1;$i<=$jumlahgerbong;$i++) : ?>
						<div class="center display-kursi" id="display-kursi-<?php echo $i; ?>" style="display:none">
							<div class="center" style="margin:0 auto">
								<img src="<?php echo base_url(); ?>public/images/gerbong.jpg">
								<div alt="<?php echo $i; ?>" class="select-gerbong strong fs16">
									<?php echo substr(strtoupper($kelas),0,3).'-'.$i; ?>
								</div>
								<table style="width:100%;" align="center">
									<?php $lontrong = 1; ?>
									<?php foreach(range('A',$max_range) as $alpha) : ?>
									<tr>
										<?php for($k=1;$k<=$block_horisontal;$k++) : ?>
										<td><div alt="<?php echo $i.$k.$alpha; ?>" class="pointer radius5 select-seat seats getinfo" style="padding:6px 0;color:#FFFFFF;background:<?php if(in_array($i.$k.$alpha,$taken_seat)) : ?>#FF0000<?php else : ?>#11BF36<?php endif; ?>;margin:5px;"><?php echo $k.$alpha; ?></div></td>
										<?php endfor; ?>
									</tr>
										
									<?php if($lontrong == 2) : ?>
									<tr>
										<td colspan="<?php echo $block_horisontal; ?>">&nbsp;</td>
									</tr>
									<?php endif; ?>

									<?php $lontrong++;endforeach; ?>
								</table>
							</div>
							
						</div>
						<?php endfor; ?>
						<div class="center"><button id="back" style="display:none" class="orange-button pointer">&laquo; back</button></div>
						<div id="info-box"></div>

					</div>
					<a class="close-reveal-modal">&#215;</a>
				</div>

				<script>
					$('#back').click(function(){
						$('.display-kursi').hide();
						$('#display-gerbong').show();
						$('#info-box').hide();
						$(this).hide();
					});
					$('.getinfo').click(
						
						function(){
							var id_kereta_api = $('#id_kereta_api').attr('alt');
							var kodekursi = $(this).attr('alt');
							var id_penumpang = $('#edit-id-penumpang').attr('alt');
							$('#info-box').html('loading...');

							setTimeout(function(){
								$.ajax({
									url: '<?php echo base_url(); ?>order/get_info_kursi_on_hover/'+kodekursi+'/'+id_kereta_api+'/'+id_penumpang,
									cache: false,
									success: function(response){
										$('#info-box').html(response);
									}
								});

							},2000)

						}

					);
				</script>