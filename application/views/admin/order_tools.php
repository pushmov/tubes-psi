<?php $this->load->view('admin/header'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img src="<?php echo base_url(); ?>public/images/home-icon.png" style="border:none;vertical-align:middle;margin:0 5px;">
				<a href="<?php echo base_url(); ?>admin/">Home</a>
			</div>

			<div class="clearfix">
				<div class="heading">
					Manage Kursi Penumpang
				</div>
				
			</div>

			<div style="margin:20px 0">
				<div style="width:100%;position:relative">
					Nomer ID : <input type="text" name="search" id="search-form" class="search" placeholder="Search by Nomer Order">
					<span class="searchbox icon-search pointer" id="search" style="margin:0 10px;"></span>
					<div id="loader" style="display:none;position:absolute;bottom:-15px;left:0;color:#010EBF"><img src="<?php echo base_url(); ?>public/images/preloader.gif" style="height:16px;margin:0 5px;">searching...</div>
				</div>
			</div>

			<?php if($this->session->flashdata('message')) : ?>
			<div class="fs14" style="color:#039B03"><?php echo $this->session->flashdata('message'); ?></div>
			<?php endif; ?>

			<div id="ajax-response">
				
			</div>
		</div>

		<script>
			$('#search').click(function(){
				var params = $('#search-form').val();
				if(params == ''){
					return false;
				}

				$('#loader').show();
				setTimeout(function(){

					$.ajax({
						url: '<?php echo base_url(); ?>admin_order/tools_search/'+encodeURIComponent(params),
						success:function(response){
							$('#loader').hide();
							$('#ajax-response').html(response).fadeIn('slow');
						}
					});

				},2000);
				
			});
		</script>

<?php $this->load->view('admin/footer'); ?>