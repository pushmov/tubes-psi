<!DOCTYPE html>
<html lang="en">
  <head>
  	<title>Admin KAI</title>	
	
		<link rel="shortcut icon" href="http://localhost/tubes-psi/public/images/favicon.png" />
		<script src="<?php echo base_url(); ?>public/js/jquery-1.4.4.min.js"></script>
		<script src="<?php echo base_url(); ?>public/js/jquery-ui.js"></script>

		<?php $this->load->view('header/plugin/modal'); ?>
		<?php $this->load->view('header/plugin/tipsy'); ?>
		<?php $this->load->view('header/plugin/modal_confirm'); ?>
		<?php $this->load->view('header/plugin/datepicker-3'); ?>

		<link href="<?php echo base_url(); ?>public/css/admin.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url(); ?>public/css/bootstrap.css" rel="stylesheet" type="text/css">
  </head>
<body>
	<div class="full clearfix">
		<div class="top-border right">
			<div style="width:1080px;margin:0 auto;">
			<ul>
				<li><?php echo date('D, d/M/Y'); ?></li>
				<li id="togglepanel">Welcome, Admin</li>
				<li>
					<a style="color:#FFFFFF !important" href="<?php echo base_url(); ?>admin/logout/" onclick="return logoutConfirm()">
						<img src="<?php echo base_url(); ?>public/images/logout-white.png" style="border:none;width:16px;vertical-align:middle;margin:0 5px;">Logout
					</a>
				</li>
			</ul>
			</div>
		</div>
		<div class="left-navigation float-left" id="left-panel">
			<div id="accordian">
				<ul>
					<li class="active">
						<h3>DASHBOARD</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>admin/">Dashboard</a></li>
							<li><a href="<?php echo base_url(); ?>admin/logout/">Logout</a></li>
						</ul>
					</li>
					<!-- we will keep this LI open by default -->
					<li class="active">
						<h3>Manage Order</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>admin_order/">Lihat Order</a></li>
							<li><a href="<?php echo base_url(); ?>admin_order/verifikasi/">Verifikasi Order</a></li>
							<li><a href="<?php echo base_url(); ?>admin_order/tools/">Manage Kursi Penumpang</a></li>
						</ul>
					</li>
					<li>
						<h3>Manage Posts</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>admin_posts/">Lihat Posts</a></li>
							<li><a href="<?php echo base_url(); ?>admin_posts/add/">Tambah Post</a></li>
						</ul>
					</li>
					<li>
						<h3>Members</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>admin_members/">Lihat Member</a></li>
							<li><a href="<?php echo base_url(); ?>admin_members/add/">Add Member</a></li>
							<li><a href="<?php echo base_url(); ?>admin_members/profile/">Your Profile</a></li>
						</ul>
					</li>
					<li>
						<h3>Pages</h3>
						<ul>
							<li><a href="#">All Pages</a></li>
							<li><a href="#">Add Pages</a></li>
						</ul>
					</li>
					<li>
						<h3>Data Kereta Api</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>admin_ka/">Lihat Data Kereta Api</a></li>
							<li><a href="<?php echo base_url(); ?>admin_ka/add/">Tambah Data</a></li>
						</ul>
					</li>
					<li>
						<h3>Data Rute Kereta Api</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>admin_rute_ka/">Lihat Rute Kereta Api</a></li>
							<li><a href="<?php echo base_url(); ?>admin_rute_ka/add/">Tambah Data</a></li>
						</ul>
					</li>
					<li>
						<h3>Data Stasiun</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>admin_stasiun_ka/">Lihat Data Stasiun</a></li>
							<li><a href="<?php echo base_url(); ?>admin_stasiun_ka/add/">Tambah Data</a></li>
						</ul>
					</li>
					<li>
						<h3>Data Transaksi</h3>
						<ul>
							<li><a href="<?php echo base_url(); ?>admin_transaksi/">Completed</a></li>
							<li><a href="<?php echo base_url(); ?>admin_transaksi/pending/">Pending</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>