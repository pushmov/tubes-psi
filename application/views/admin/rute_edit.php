<?php $this->load->view('admin/header'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>admin/">Home</a>
			</div>
			<div class="heading">
				Edit Data Rute Kereta Api
			</div>

			<h4 style="color:#71C39A"><?php echo $row->Kereta->NamaKeretaApi; ?></h4>

			<div class="main">
				<form id="editform" name="editform" action="<?php echo base_url(); ?>admin_ka/edit/<?php echo $row->IDKeretaApi; ?>/" method="POST">
				<table style="width:100%">
					<tr>
						<td>ID Kereta : </td>
						<td><?php echo $row->IDKeretaApi; ?></td>
					</tr>
					<tr>
						<td>Nama Kereta : </td>
						<td><input type="text" name="nama" placeholder="Nama Kereta" value="<?php echo $row->Kereta->NamaKeretaApi; ?>"></td>
					</tr>
					<tr>
						<td>Rute : </td>
						<td>
							<input type="text" name="add-list" class="add-list" id="add-list-val" placeholder="Input Nama Stasiun"><span class="icon-plus-sign pointer" id="add-list" style="margin:0 5px;"></span>
							<div class="stasiun-list">
									<?php foreach($data_stasiun as $stasiun) : ?>
									<?php if(in_array($stasiun->IDStasiun, $row->ArrRute)) : ?>
										<div style="padding:4px 8px"><span style="margin: 0 5px;" class="icon-remove"></span><?php echo $stasiun->NamaStasiun; ?></div>
									<?php endif; ?>
									<?php endforeach; ?>
									
							</div>
						</td>
					</td>
					
					
				</table>
				</form>

				<div class="center clearfix" style="margin:20px 0;position:relative;">
					<div id="preloader" style="position:absolute;top:0;left:0;display:none">
						<img style="height:16px;margin:0 5px;" src="<?php echo base_url(); ?>public/images/preloader.gif">
					</div>
					<div>
						<a id="submit" href="javascript:void(0)" class="btn btn-sm btn-primary" style="color:#FFFFFF !important"><span class="glyphicon icon-edit icon-white"></span> Simpan</a>
						<a href="<?php echo base_url(); ?>admin_ka/" class="btn btn-sm btn-danger" style="color:#FFFFFF !important"><span class="glyphicon icon-remove icon-white"></span> Cancel</a>
					</div>
				</div>
			</div>

		</div>

		<script>
			$(document).ready(function(){
				
				
			});

			$('#add-list').click(function(){
				var val = $('#add-list-val').val();
				if(val != ''){
					alert('a');
					$('.stasiun-list').append('<div style="padding:4px 8px"><span style="margin: 0 5px;" class="icon-remove"></span>'+val+'</div>');
				}
			});

			$('#submit').click(function(){
				$('#preloader').show();
				setTimeout(function(){
					$('#editform').submit();
				},2000);
			});
		</script>

<?php $this->load->view('admin/footer'); ?>