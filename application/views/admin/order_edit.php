<?php $this->load->view('admin/header'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="http://localhost/tubes-psi/public/images/home-icon.png">
				<a href="http://localhost/tubes-psi/admin/">Home</a>
			</div>
			<div class="heading">
				Manage Order
			</div>

			<div class="main">
				<h4>Edit Order ID : <span style="color:#71C39A"><?php echo $order_row->OrderNumber; ?></span></h4>
				<form id="editform" name="editform" action="<?php echo base_url(); ?>admin_order/edit/<?php echo $order_row->OrderID; ?>" method="POST">
				<table style="width:100%">
					<tr>
						<td>Tanggal Berangkat : </td>
						<td><input type="text" name="tanggalberangkat" id="datepicker" placeholder="YYYY-MM-DD" value="<?php echo $order_row->TanggalBerangkat; ?>"></td>
					</tr>
					<tr>
						<td>Kereta Api : </td>
						<td><input type="text" name="keretaapi" id="keretaapi" placeholder="Nama Kereta" value="<?php echo $order_row->Kereta->NamaKeretaApi; ?>"></td>
					</tr>
					<tr>
						<td>Asal : </td>
						<td><input type="text" name="asal" id="asal" placeholder="Asal" value="<?php echo $order_row->Asal->NamaStasiun; ?>"></td>
					</tr>
					<tr>
						<td>Tujuan : </td>
						<td><input type="text" name="tujuan" id="tujuan" placeholder="Tujuan" value="<?php echo $order_row->Tujuan->NamaStasiun; ?>"></td>
					</tr>
					<tr>
						<td>Status : </td>
						<td>
							<input type="radio" name="status" value="0" <?php if(!$order_row->IsVerified) : ?>checked="checked"<?php endif; ?> > Belum Bayar
							<input type="radio" name="status" value="1" <?php if($order_row->IsVerified) : ?>checked="checked"<?php endif; ?> > Sudah Bayar
						</td>
					</tr>
				</table>
				</form>

				<div class="center clearfix" style="margin:20px 0;position:relative;">
					<div id="preloader" style="position:absolute;top:0;left:0;display:none">
						<img style="height:16px;margin:0 5px;" src="<?php echo base_url(); ?>public/images/preloader.gif">
					</div>
					<div>
						<a id="submit" href="javascript:void(0)" class="btn btn-sm btn-primary" style="color:#FFFFFF !important"><span class="glyphicon icon-edit icon-white"></span> Simpan</a>
						<a href="<?php echo base_url(); ?>admin_order/" class="btn btn-sm btn-danger" style="color:#FFFFFF !important"><span class="glyphicon icon-remove icon-white"></span> Cancel</a>
					</div>
				</div>
			</div>

		</div>

		<script>
		$(function() {
			$( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
		});
			$('#submit').click(function(){
				$('#preloader').show();
				setTimeout(function(){
					$('#editform').submit();
				},2000);
			});
		</script>

<?php $this->load->view('admin/footer'); ?>