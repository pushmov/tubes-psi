					<div class="fs14" style="margin:10px 0;color:#FF0000">Total Records : <?php echo $total_record; ?></div>
					<table class="maintable">
						<thead>
							<tr>
								<th>Status</th>
								<th>Kode Transaksi</th>
								<th>Tanggal Order</th>
								<th>Penumpang</th>
								<th>Tarif Satuan</th>
								<th>Layanan Pelanggan</th>
								<th>Tarif Total</th>
							</tr>
						</thead>
						<tbody>

							<?php if(!empty($result)) : ?>
							<?php $total_rp = 0; ?>
							<?php foreach($result as $row) : ?>
							<?php $row_total = 0; ?>
							<?php $row_total += ($row->Tarif * $row->TotalPenumpang) + 7500; ?>
							<?php $total_rp += $row_total; ?>
							<tr>
								<td class="center" style="color:#71C39A">
									<?php if($row->IsVerified) : ?>
									<span class="icon-ok"></span> verified
									<?php else : ?>
										<?php if(strtotime(date('Y-m-d H:i:s')) > strtotime($row->TanggalOrder."+1 days")) : ?>
											<div style="color:#FF0000">Expired</div>
										<?php endif; ?>
									<?php endif; ?>
								</td>
								<td class="left"><?php echo $row->OrderNumber; ?></td>
								<td class="left"><?php echo date('d/M/Y  H:i:s',strtotime($row->TanggalOrder)); ?></td>
								<td class="center"><?php echo $row->TotalPenumpang; ?></td>
								<td class="right"><?php echo 'Rp '. number_format($row->Tarif); ?></td>
								<td class="right"><?php echo 'Rp '.number_format(7500); ?></td>
								<td class="right"><?php echo 'Rp '.number_format($row_total); ?></td>
							</tr>
							<?php endforeach; ?>
							<tr>
								<td colspan="6" class="right strong" style="padding:5px 8px">TOTAL</td>
								<td colspan="1" class="right strong" style="padding:5px 8px"><?php echo 'Rp '.number_format($total_rp); ?></td>
							</tr>
							<?php else : ?>
							<tr>
								<td colspan="6">No records found</td>
							</tr>
							<?php endif; 	?>
							
						</tbody>
					</table>