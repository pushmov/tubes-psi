<?php $this->load->view('admin/header'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img src="<?php echo base_url(); ?>public/images/home-icon.png" style="border:none;vertical-align:middle;margin:0 5px;">
				<a href="<?php echo base_url(); ?>admin/">Home</a>
			</div>

			<div class="clearfix">
				<div class="heading float-left">
					Rute Kereta Api
				</div>
				<div class="float-right">
					<div style="width:100%;position:relative">
						<input type="text" name="search" id="search-form" class="search" placeholder="Search by Nama KA">
						<span class="searchbox icon-search pointer" id="search" style="margin:0 10px;"></span>
						<div id="loader" style="display:none;position:absolute;bottom:-15px;right:0;color:#010EBF"><img src="<?php echo base_url(); ?>public/images/preloader.gif" style="height:16px;margin:0 5px;">searching...</div>
					</div>
				</div>
			</div>

			<?php if($this->session->flashdata('message')) : ?>
			<div style="color:#2E842F" class="fs18"><?php echo $this->session->flashdata('message'); ?></div>
			<?php endif; ?>

			<div id="ajax-response">
				<table class="maintable">
					<thead>
						<tr>
							<th>Nomor KA</th>
							<th>Nama KA</th>
							<th>Total Checkpoint</th>
							<th>Rute</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>

						<?php if(!empty($result)) : ?>
						<?php foreach($result as $row) : ?>
						<tr>
							<td class="center"><?php echo $row->IDKeretaApi; ?></td>
							<td class="left"><?php echo $row->NamaKeretaApi; ?></td>
							<td class="center"><?php echo $row->TotalRute; ?></td>
							<td class="left" style="width:60%"><?php echo $row->ListRute; ?></td>
							<td class="center">
								<a href="<?php echo base_url(); ?>admin_rute_ka/edit/<?php echo $row->IDRoute; ?>/"><span style="margin:0 8px" class="icon-edit"></span></a>
								<a href="<?php echo base_url(); ?>admin_rute_ka/delete/<?php echo $row->IDRoute; ?>/"><span style="margin:0 8px" class="icon-remove"></span></a>
							</td>
						</tr>
						<?php endforeach; ?>
						<?php else : ?>
						<tr>
							<td colspan="4">No records found</td>
						</tr>
						<?php endif; 	?>
						
					</tbody>
				</table>
				<div class="pagination"><?php echo $page; ?></div>
			</div>
		</div>

		<script>
			$('#search').click(function(){
				var params = $('#search-form').val();
				if(params == ''){
					return false;
				}
				$('#loader').show();
				setTimeout(function(){

					$.ajax({
						url: '<?php echo base_url(); ?>admin_rute_ka/search/'+encodeURIComponent(params),
						success:function(response){
							$('#loader').hide();
							$('#ajax-response').html(response).fadeIn('slow');
						}
					});

				},2000);
				
			});
		</script>

<?php $this->load->view('admin/footer'); ?>