<?php $this->load->view('admin/header'); ?>

		<div class="content">
			<div class="heading">
				Dashboard
			</div>

			<div class="main">
				<table style="width:100%">
					<tr>
						<td style="width:45%;">
							<div style="margin:5px 10px;padding:10px 8px;color:#FFFFFF;background:#32C17A" class="fs16 strong">ORDER : </div>
						</td>
						<td style="width:45%"><div style="margin:5px 10px;padding:10px 8px;color:#FFFFFF;background:#32C17A" class="fs16 strong">TOTAL RUTE : </div></td>
					</tr>
					<tr>
						<td>
							<div style="margin:5px 10px;padding:10px 8px;color:#333333;background:#EAEAEA">Total : <a style="color:#32C17A !important" href="<?php echo base_url(); ?>admin_order/"><?php echo $total_order; ?> Order </a></div>
							<div style="margin:5px 10px;padding:10px 8px;color:#333333;background:#EAEAEA">Pending : <a style="color:#32C17A !important" href="<?php echo base_url(); ?>admin_order/"><?php echo $pending_order; ?> Order</a> </div>
						</td>
						<td>
							<div style="margin:5px 10px;padding:10px 8px;color:#333333;background:#EAEAEA">Total : <a style="color:#32C17A !important" href="<?php echo base_url(); ?>admin_rute_ka/"><?php echo $total_rute; ?> Rute </a></div>
						</td>
					</tr>
				</table>

				<table style="width:100%;margin:20px 0">
					<tr>
						<td style="width:45%;">
							<div style="margin:5px 10px;padding:10px 8px;color:#FFFFFF;background:#32C17A" class="fs16 strong">DATA STASIUN : </div>
						</td>
						<td style="width:45%">
							<div style="margin:5px 10px;padding:10px 8px;color:#FFFFFF;background:#32C17A" class="fs16 strong">DATA KERETA API : </div>
						</td>
					</tr>
					<tr>
						<td>
							<div style="margin:5px 10px;padding:10px 8px;color:#333333;background:#EAEAEA">Total : <a style="color:#32C17A !important" href="<?php echo base_url(); ?>admin_stasiun_ka/"><?php echo $total_stasiun; ?> Records </a></div>
						</td>
						<td>
							<div style="margin:5px 10px;padding:10px 8px;color:#333333;background:#EAEAEA">Total : <a style="color:#32C17A !important" href="<?php echo base_url(); ?>admin_ka/"><?php echo $total_kereta; ?> Records </a></div>
						</td>
					</tr>
				</table>

				<table style="width:100%;margin:20px 0">
					<tr>
						<td style="width:45%;">
							<div style="margin:5px 10px;padding:10px 8px;color:#FFFFFF;background:#32C17A" class="fs16 strong">PENJADWALAN : </div>
						</td>
						<td style="width:45%"></td>
					</tr>
					<tr>
						<td>
							<div style="margin:5px 10px;padding:10px 8px;color:#333333;background:#EAEAEA">Total : <a style="color:#32C17A !important" href="<?php echo base_url(); ?>admin_order/"><?php echo $total_jadwal; ?> Records </a></div>
						</td>
						<td></td>
					</tr>
				</table>				
				
			</div>

		</div>

<?php $this->load->view('admin/footer'); ?>