				<table class="maintable">
					<thead>
						<tr>
							<th>Nomor KA</th>
							<th>Nama KA</th>
							<th>Total Checkpoint</th>
							<th>Rute</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>

						<?php if(!empty($result)) : ?>
						<?php foreach($result as $row) : ?>
						<tr>
							<td class="center"><?php echo $row->IDKeretaApi; ?></td>
							<td class="left"><?php echo $row->NamaKeretaApi; ?></td>
							<td class="center"><?php echo $row->TotalRute; ?></td>
							<td class="left" style="width:60%"><?php echo $row->ListRute; ?></td>
							<td class="center">
								<a href="<?php echo base_url(); ?>admin_rute_ka/edit/<?php echo $row->IDRoute; ?>/"><span style="margin:0 8px" class="icon-edit"></span></a>
								<a href="<?php echo base_url(); ?>admin_rute_ka/delete/<?php echo $row->IDRoute; ?>/"><span style="margin:0 8px" class="icon-remove"></span></a>
							</td>
						</tr>
						<?php endforeach; ?>
						<?php else : ?>
						<tr>
							<td colspan="4">No records found</td>
						</tr>
						<?php endif; 	?>
						
					</tbody>
				</table>