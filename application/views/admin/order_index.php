<?php $this->load->view('admin/header'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img src="<?php echo base_url(); ?>public/images/home-icon.png" style="border:none;vertical-align:middle;margin:0 5px;">
				<a href="<?php echo base_url(); ?>admin/">Home</a>
			</div>

			<div class="clearfix">
				<div class="heading float-left">
					Manage Order
				</div>
				<div class="float-right">
					<div style="width:100%;position:relative">
						<input type="text" name="search" id="search-form" class="search" placeholder="Search by Nomer Order">
						<span class="searchbox icon-search pointer" id="search" style="margin:0 10px;"></span>
						<div id="loader" style="display:none;position:absolute;bottom:-15px;right:0;color:#010EBF"><img src="<?php echo base_url(); ?>public/images/preloader.gif" style="height:16px;margin:0 5px;">searching...</div>
					</div>
				</div>
			</div>

			<?php if($this->session->flashdata('message')) : ?>
			<div style="color:#2E842F" class="fs18"><?php echo $this->session->flashdata('message'); ?></div>
			<?php endif; ?>

			<div id="ajax-response">
				<table class="maintable">
					<thead>
						<tr>
							<th>Nomor Order</th>
							<th>Tanggal Order</th>
							<th>Tanggal Berangkat</th>
							<th>Nama Kereta</th>
							<th>Asal</th>
							<th>Tujuan</th>
							<th>Status</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>

						<?php if(!empty($result)) : ?>
						<?php foreach($result as $row) : ?>
						<tr>
							<td><?php echo $row->OrderNumber; ?></td>
							<td><?php echo date('D, d/M/Y H:i:s',strtotime($row->TanggalOrder)); ?></td>
							<td><?php echo date('D, d/M/Y',strtotime($row->TanggalBerangkat)); ?></td>
							<td><?php echo $row->DetailKereta->NamaKeretaApi; ?></td>
							<td><?php echo $row->DetailAsal->NamaStasiun . ', '. $row->DetailAsal->Kota; ?></td>
							<td><?php echo $row->DetailTujuan->NamaStasiun . ', '. $row->DetailTujuan->Kota; ?></td>
							<td><?php echo ($row->IsVerified) ? "<span style='color:#00FF00'>Verified</span>" : "<span style='color:#FF0000'>Unverified</span>"; ?></td>
							<td>
								<?php if(strtotime(date('Y-m-d H:i:s')) >= strtotime($row->TanggalBerangkat)) : ?>
								<span style="color:#FF0000">Expired</span>
								<?php else : ?>
								<a href="<?php echo base_url(); ?>admin_order/edit/<?php echo $row->OrderID; ?>/">Edit</a>
								<?php endif; ?>
							</td>
						</tr>
						<?php endforeach; ?>
						<?php else : ?>
						<tr>
							<td colspan="8">No records found</td>
						</tr>
						<?php endif; 	?>
						
					</tbody>
				</table>
			</div>
		</div>

		<script>
			$('#search').click(function(){
				var params = $('#search-form').val();
				if(params == ''){
					return false;
				}
				$('#loader').show();
				setTimeout(function(){

					$.ajax({
						url: '<?php echo base_url(); ?>admin_order/search/'+encodeURIComponent(params),
						success:function(response){
							$('#loader').hide();
							$('#ajax-response').html(response).fadeIn('slow');
						}
					});

				},2000);
				
			});
		</script>

<?php $this->load->view('admin/footer'); ?>