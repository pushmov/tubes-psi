<?php $this->load->view('admin/header'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="http://localhost/tubes-psi/public/images/home-icon.png">
				<a href="http://localhost/tubes-psi/admin/">Home</a>
			</div>
			<div class="heading">
				Edit Stasiun
			</div>

			<div class="main">
				<h4>Edit Stasiun : <span style="color:#71C39A"><?php echo $detail->NamaStasiun; ?></span></h4>
				<form id="editform" name="editform" action="<?php echo base_url(); ?>admin_stasiun_ka/edit/<?php echo $detail->IDStasiun; ?>" method="POST">
				<table style="width:100%">
					<tr>
						<td>Nama Stasiun : </td>
						<td><input type="text" name="nama" placeholder="Nama Stasiun" value="<?php echo $detail->NamaStasiun; ?>"></td>
					</tr>
					<tr>
						<td>Kota : </td>
						<td><input type="text" name="kota" id="kota" placeholder="Nama Kota" value="<?php echo $detail->Kota; ?>"></td>
					</tr>
					
				</table>
				</form>

				<div class="center clearfix" style="margin:20px 0;position:relative;">
					<div id="preloader" style="position:absolute;top:0;left:0;display:none">
						<img style="height:16px;margin:0 5px;" src="<?php echo base_url(); ?>public/images/preloader.gif">
					</div>
					<div>
						<a id="submit" href="javascript:void(0)" class="btn btn-sm btn-primary" style="color:#FFFFFF !important"><span class="glyphicon icon-edit icon-white"></span> Simpan</a>
						<a href="<?php echo base_url(); ?>admin_stasiun_ka/" class="btn btn-sm btn-danger" style="color:#FFFFFF !important"><span class="glyphicon icon-remove icon-white"></span> Cancel</a>
					</div>
				</div>
			</div>

		</div>

		<script>
			$('#submit').click(function(){
				$('#preloader').show();
				setTimeout(function(){
					$('#editform').submit();
				},2000);
			});
		</script>

<?php $this->load->view('admin/footer'); ?>