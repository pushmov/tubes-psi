<!DOCTYPE html>
<html>
<head>
	<title>PT KAI Sistem Informasi Tiket - Index</title>	
	
	<link rel="shortcut icon" href="http://localhost/tubes-psi/public/images/favicon.png" />
	<script src="<?php echo base_url(); ?>public/js/jquery-1.4.4.min.js"></script>
	<script src="<?php echo base_url(); ?>public/js/jquery-ui.js"></script>

	<link href="<?php echo base_url(); ?>public/css/admin.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/css/bootstrap.css" rel="stylesheet" type="text/css">
</head>
<body style="background:#000000">
	<div class="full">
		<div class="wrapper adminlogin">
			<div class="center" style="padding:150px 0">
				<div style="padding:20px;width:50%;margin:0 auto;border:1px solid #CCCCCC" class="radius5">
					<h3 style="padding:20px 0;">Administrator Login</h3>
					<form action="<?php echo base_url(); ?>admin/validate/" method="post" name="validate" >
						<table style="width: 100%;" align="center">
							<tr>
								<td style="padding:4px 0;width:30%">Email</td>
								<td style="padding:4px 0;width:70%"><input type="text" name="username" id="username" style="width:70%"></td>
							</tr>
							<tr>
								<td>Password</td>
								<td><input type="password" name="password" id="password" style="width:70%"></td>
							</tr>
							<tr>
								<td colspan="2">
									<div style="padding:20px 80px" class="right">
										<button type="submit" class="css3button pointer">Login</button>
									</div>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>