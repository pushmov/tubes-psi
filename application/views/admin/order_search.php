				<table class="maintable">
					<thead>
						<tr>
							<th>Nomor Order</th>
							<th>Tanggal Order</th>
							<th>Tanggal Berangkat</th>
							<th>Nama Kereta</th>
							<th>Asal</th>
							<th>Tujuan</th>
							<th>Status</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>

						<?php if(!empty($result)) : ?>
						<?php foreach($result as $row) : ?>
						<tr>
							<td><?php echo $row->OrderNumber; ?></td>
							<td><?php echo date('D, d/M/Y H:i:s',strtotime($row->TanggalOrder)); ?></td>
							<td><?php echo date('D, d/M/Y',strtotime($row->TanggalBerangkat)); ?></td>
							<td><?php echo $row->DetailKereta->NamaKeretaApi; ?></td>
							<td><?php echo $row->DetailAsal->NamaStasiun . ', '. $row->DetailAsal->Kota; ?></td>
							<td><?php echo $row->DetailTujuan->NamaStasiun . ', '. $row->DetailTujuan->Kota; ?></td>
							<td><?php echo ($row->IsVerified) ? "<span style='color:#00FF00'>Verified</span>" : "<span style='color:#FF0000'>Unverified</span>"; ?></td>
							<td>
								<?php if(strtotime(date('Y-m-d H:i:s')) >= strtotime($row->TanggalBerangkat)) : ?>
								<span style="color:#FF0000">Expired</span>
								<?php else : ?>
								<a href="<?php echo base_url(); ?>admin_order/edit/<?php echo $row->OrderID; ?>">Edit</a>
								<?php endif; ?>
							</td>
						</tr>
						<?php endforeach; ?>
						<?php else : ?>
						<tr>
							<td colspan="8">No records found</td>
						</tr>
						<?php endif; 	?>
						
					</tbody>
				</table>