				<table class="maintable">
					<thead>
						<tr>
							<th>Nama Stasiun</th>
							<th>Kota</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>

						<?php if(!empty($row)) : ?>
						<tr>
							<td class="center"><?php echo $row->NamaStasiun; ?></td>
							<td class="center"><?php echo $row->Kota; ?></td>
							<td class="center">
								<a href="<?php echo base_url(); ?>admin_stasiun_ka/edit/<?php echo $row->IDStasiun; ?>/"><span style="margin:0 8px" class="icon-edit"></span></a>
								<a href="<?php echo base_url(); ?>admin_stasiun_ka/delete/<?php echo $row->IDStasiun; ?>/"><span style="margin:0 8px" class="icon-remove"></span></a>
							</td>
						</tr>
						<?php else : ?>
						<tr>
							<td colspan="3">No records found</td>
						</tr>
						<?php endif; 	?>
						
					</tbody>
				</table>