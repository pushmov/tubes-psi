<?php $this->load->view('admin/header'); ?>

		<div class="content">
			<div style="padding:20px 0">
				<img style="border:none;vertical-align:middle;margin:0 5px;" src="<?php echo base_url(); ?>public/images/home-icon.png">
				<a href="<?php echo base_url(); ?>admin/">Home</a>
			</div>
			<div class="heading">
				Edit Data Kereta Api
			</div>

			<h4 style="color:#71C39A"><?php echo $row->NamaKeretaApi; ?></h4>

			<div class="main">
				<form id="editform" name="editform" action="<?php echo base_url(); ?>admin_ka/edit/<?php echo $row->IDKeretaApi; ?>/" method="POST">
				<table style="width:100%">
					<tr>
						<td>ID Kereta : </td>
						<td><?php echo $row->IDKeretaApi; ?></td>
					</tr>
					<tr>
						<td>Nama Kereta : </td>
						<td><input type="text" name="nama" placeholder="Nama Kereta" value="<?php echo $row->NamaKeretaApi; ?>"></td>
					</tr>
					<tr>
						<td>Kelas : </td>
						<td>
							<div style="margin:8px 0">
								<input type="radio" name="kelas" class="kelas" <?php echo ($row->Kelas == 'ekonomi') ? 'checked' : NULL; ?> value="ekonomi" ><span style="margin:0 5px;">Ekonomi</span>
								<input type="radio" name="kelas" class="kelas" <?php echo ($row->Kelas == 'bisnis') ? 'checked' : NULL; ?> value="bisnis" ><span style="margin:0 5px;">Bisnis</span>
								<input type="radio" name="kelas" class="kelas" <?php echo ($row->Kelas == 'eksekutif') ? 'checked' : NULL; ?> value="eksekutif" ><span style="margin:0 5px;">Eksekutif</span>
							</div>
						</td>
					</tr>	
					<tr>
						<td>Jumlah Gerbong : </td>
						<td>
							<select name="jmlgerbong" id="jmlgerbong">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
						</td>
					</tr>
					
				</table>
				</form>

				<div class="center clearfix" style="margin:20px 0;position:relative;">
					<div id="preloader" style="position:absolute;top:0;left:0;display:none">
						<img style="height:16px;margin:0 5px;" src="<?php echo base_url(); ?>public/images/preloader.gif">
					</div>
					<div>
						<a id="submit" href="javascript:void(0)" class="btn btn-sm btn-primary" style="color:#FFFFFF !important"><span class="glyphicon icon-edit icon-white"></span> Simpan</a>
						<a href="<?php echo base_url(); ?>admin_ka/" class="btn btn-sm btn-danger" style="color:#FFFFFF !important"><span class="glyphicon icon-remove icon-white"></span> Cancel</a>
					</div>
				</div>
			</div>

		</div>

		<script>
			$(document).ready(function(){
				$('#jmlgerbong').val('<?php echo $row->JumlahGerbong; ?>');
				$('#Kelas').val('<?php echo $row->Kelas; ?>');
			});
			$('#submit').click(function(){
				$('#preloader').show();
				setTimeout(function(){
					$('#editform').submit();
				},2000);
			});
		</script>

<?php $this->load->view('admin/footer'); ?>