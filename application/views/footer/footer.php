		
		
		<div id="footer">
			<div class="center">
				<div class="wrapper socmed-wrapper">
					<div class="socmed" id="rss">
						<img src="<?php echo base_url(); ?>public/images/socmed/colour/rss.png" class="bottom">
						<img src="<?php echo base_url(); ?>public/images/socmed/grey/rss.png" class="top">
					</div>
					
					<div class="socmed" id="facebook" onclick="OpenInNewTab('http://facebook.com/keretaapikita')">
						<img src="<?php echo base_url(); ?>public/images/socmed/colour/facebook.png" class="bottom">
						<img src="<?php echo base_url(); ?>public/images/socmed/grey/facebook.png" class="top">
					</div>
					
					<div class="socmed" id="twitter" onclick="OpenInNewTab('http://twitter.com/keretaapikita')">
						<img src="<?php echo base_url(); ?>public/images/socmed/colour/twitter.png" class="bottom">
						<img src="<?php echo base_url(); ?>public/images/socmed/grey/twitter.png" class="top">
					</div>
					
					<div class="socmed" id="youtube" onclick="OpenInNewTab('http://youtube.com/keretaapikita')">
						<img src="<?php echo base_url(); ?>public/images/socmed/colour/you_tube.png" class="bottom">
						<img src="<?php echo base_url(); ?>public/images/socmed/grey/you_tube.png" class="top">
					</div>
					
					<div class="socmed" id="gplus" onclick="OpenInNewTab('https://plus.google.com/107994068759416255362/posts')">
						<img src="<?php echo base_url(); ?>public/images/socmed/colour/google+.png" class="bottom">
						<img src="<?php echo base_url(); ?>public/images/socmed/grey/google+.png" class="top">
					</div>
				</div>
			</div>
			
			<div class="footer fs16" style="color:#474747;padding:30px 0">
				<div class="wrapper clearfix">
					<div class="float-left" style="width:100%">
						<div class="clearfix">
							<div class="cols-1 float-left" style="width:22%;margin-right:20px;">
							<!-- left panel here -->
								<h3>Menu Navigasi</h3>
								<ul class="footer-menu">
									<li>Tentang Kami</li>
									<li>Informasi Media</li>
									<li>Gallery</li>
									<li>Layanan Produk</li>
								</ul>
							</div>
							<div class="cols-1 float-left" style="width:22%;margin-right:20px;">
								<h3>Menu Khusus</h3>
								<ul class="footer-menu">
									<li>E-Procurement</li>
									<li>Rekrutment</li>
									<li>Webmail</li>
									<li>KRL Commuter</li>
								</ul>
							</div>
							<div class="cols-2 float-left" style="width:22%">
								<!-- center panel here -->
								<h3>About Us</h3>
								<ul class="footer-menu">
									<li>Sejarah</li>
									<li>Visi & Misi</li>
									<li>Budaya</li>
									<li>Komisaris</li>
									<li>Direksi</li>
									<li>Anak Perusahaan</li>
								</ul>
							</div>
							<div class="cols-2 float-left" style="width:22%">
								<!-- center panel here -->
								<h3>Contact Us</h3>
								<div>Jl Perintis Kemerdekaan No.1 Bandung 40117</div>
								<div>(022) 798779667</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="copy">
				<div class="wrapper center">
					PT. Kereta Api Indonesia (Persero) &copy; 2013 Authorised by Team PSI 2013
				</div>
			</div>
		</div>
		<script>
			function OpenInNewTab(url){
				var win = window.open(url, '_blank');
				win.focus();
			}
		</script>