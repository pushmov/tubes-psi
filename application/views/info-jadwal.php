<?php 
	
	$title['title'] = 'PT KAI Sistem Informasi Tiket - Index';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/datepicker');
	$this->load->view('header/plugin/autosuggest');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');
?>	

		<!-- main -->
		<div id="main" class="main">
			<div class="wrapper clearfix">
				<div class="float-left" style="width:100%">
					<div class="clearfix">
						
						<div class="cols-2">
							<!-- center panel here -->
							<div class="center">
								
								<div class="float-left">
								<div class="ticket-box" style="width: 155%">
									<div class="train-white circle-radius"></div>
									<div class="clear">
										<div class="clearfix">
											<h3 style="padding:10px 0;color:#F4B139">Info Jadwal dan Reservasi</h3>
											<div style="color:#FFFFFF;padding:8px 0"><?php echo date('D, d/M/Y',strtotime($tanggal_berangkat)); ?></div>
											<div style="color:#FFFFFF;padding:8px 0"><?php echo 'Asal : '.$dari.', Tujuan : '.$to; ?></div>
										</div>
										<div class="clearfix ticket-wrapper left">
											<div>
												<?php if($this->session->flashdata('stasiun') == 'error') : ?>
												<div style="color:#FF0000" class="fs12">Error. Nama stasiun sama</div>
												<?php endif; ?>
												
											</div>

											<table style="width:100%;" align="center">
												<tr>
													<td style="padding:7px 0" colspan="4">Tersedia <?php echo $jumlah_kereta; ?> kereta api dengan asal <?php echo $dari; ?> dan tujuan <?php echo $to; ?></td>
												</tr>
												<tr>
													<td class="strong">Jam Berangkat</td>
													<td class="strong">Jam Sampai</td>
													<td class="strong">Harga</td>
													<td class="right strong" style="padding:0 10px">Booking</td>
												</tr>

												<?php if(!empty($jadwal)) : ?>
												<?php foreach($jadwal as $row) : ?>
												<tr>
													<td class="center" colspan="5">
														<div class="kereta-api-result fs16" style="background:#eaeaea"><?php echo $row->NamaKeretaApi; ?></div>
													</td>
												</tr>

												
												<?php foreach($row->Jadwal as $jadwal): ?>
												<tr>
													<td class="left" style="padding:3px 0"><?php echo $jadwal->JamBerangkat; ?> WIB</td>
													<td class="left" style="padding:3px 0"><?php echo $jadwal->JamSampai; ?> WIB</td>
													<td class="center" style="padding:3px 0"><?php echo 'Rp. '.number_format($row->Price); ?></td>
													<td class="right" style="padding:3px 0">
														<?php if($jadwal->FullyBooked) : ?>
														<span class="fs14" style="margin:0 15px;color:#FF0000">Habis</span>
														<?php else : ?>
														<button onclick="window.location='<?php echo base_url(); ?>order/agreement/<?php echo $jadwal->IDJadwal; ?>'" class="orange-button pointer">Booking &raquo;</button>
														<?php endif; ?>
													</td>
												</tr>
												<?php endforeach; ?>
												

											<?php endforeach; ?>
											<?php else : ?>
												<tr>
													<td colspan="5">Record not found</td>
												</tr>
											<?php endif; ?>

											</table>
											
										</div>
									</div>
									
								</div>
							</div>

								<div class="float-right">
								<div class="ticket-box">
									<div class="train-white circle-radius"><img src="<?php echo base_url(); ?>public/images/train-white.png"></div>
									<div class="clear">
										<div class="clearfix">
											<ul class="ticket-tabs">
												<li><a class="strong" href="javascript:void(0)">PESAN TIKET</a></li>
												<!--
												<li><a class="strong" href="#tab2">Tab 2</a></li>
												<li><a class="strong" href="#tab3">Tab 3</a></li>
											-->
											</ul>
										</div>
										<div class="clearfix ticket-wrapper left">
											<div>
												<?php if($this->session->flashdata('stasiun') == 'error') : ?>
												<div style="color:#FF0000" class="fs12">Error. Nama stasiun sama</div>
												<?php endif; ?>
												<form action="<?php echo base_url(); ?>order/info_jadwal/" method="POST" name="pesantiket" id="pesantiket">
												<table style="width:100%">
													<tr>
														<td><h4 class="form-label">DARI</h4></td>
														<td>
															<div style="padding:5px 0;">
																<input type="text" autocomplete="off" name="stasiun-from" id="stasiun-from" class="city" onkeyup="autoSuggestNew(this.id, 'listWrap1', 'searchList1', 'stasiun-from','stasiun-from','stasiun-from', event,'1');" value="<?php echo $dari; ?>">
																<div class="city">
																	Pilih Stasiun
																</div>
																<div class="listWrap" id="listWrap1">
																	<ul class="searchList" id="searchList1" style="width:64%">
																	</ul>
																</div>
															</div>
														</td>
														<td></td>
													</tr>
													<tr>
														<td><h4 class="form-label">TUJUAN</h4></td>
														<td>
															<div style="padding:5px 0;">
																<input autocomplete="off" type="text" id="stasiun-to" name="stasiun-to" class="city" onkeyup="autoSuggestNew(this.id, 'listWrap2', 'searchList2', 'stasiun-to','stasiun-to','stasiun-to', event,'1');" value="<?php echo $to; ?>">
																<div class="city" >
																	Pilih Stasiun
																</div>
																<div class="listWrap" id="listWrap2">
																	<ul class="searchList" id="searchList2" style="width:64%">
																	</ul>
																</div>
															</div>
														</td>
														<td></td>
													</tr>
													<tr>
														<th valign="middle"><h4 class="form-label">PEMESAN</h4></th>
														<td>
															<select name="jumlah_pemesan" id="jumlah_pemesan">
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
															</select>
														</td>
														<td></td>
													</tr>
													<tr>
														<th valign="middle"><h4 class="form-label">TANGGAL</h4></th>
														<td>
															<div style="padding:5px 0;"><input type="text" id="datepicker" class="datepicker" name="date" value="<?php echo $tanggal; ?>" ><div class="city"><img src="<?php echo base_url(); ?>public/images/calendar.png" style="margin-right:5px;vertical-align:middle;border:none">Pilih Tanggal</div></div>
															<div style="padding:5px 0;">
																<label class="label_radio" for="sample-check" alt="ekonomi">
																  <input type="radio" name="rad-kelas" value="ekonomi" <?php echo ($this->input->post('kelas') == 'ekonomi') ? 'checked="checked"' : ''; ?>>Ekonomi
																</label>

																<label class="label_radio" for="sample-radio" alt="bisnis">
																  <input type="radio" name="rad-kelas" value="bisnis" <?php echo ($this->input->post('kelas') == 'bisnis') ? 'checked="checked"' : ''; ?>>Bisnis
																</label>
																<label class="label_radio" for="sample-radio" alt="eksekutif">
																	<input type="radio" name="rad-kelas" value="eksekutif" <?php echo ($this->input->post('kelas') == 'eksekutif') ? 'checked="checked"' : ''; ?>>Eksekutif
																</label>
																<input type="hidden" name="kelas" id="kelas" value="<?php echo $this->input->post('kelas');?>">
															</div>
														</td>
														<td></td>
													</tr>
													
													<tr>
														<td colspan="3">
															<div><span style="color:#FF0000 !important;font-size:12px;display:none;" id="error-tag"></span></div>
															<div class="right" style="margin:0 20px;"><button type="submit" id="submit-ticket" class="orange-button pointer">LIHAT JADWAL</button></div>
														</td>
													</tr>
												</table>
											</form>
											</div>
											
										</div>
									</div>
									
								</div>
							</div>
								
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<!-- end main -->
		<script>
			$('.label_radio').click(function(){
				$('#kelas').val($(this).attr('alt'));
			});
		</script>
		
<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
