<?php 
	
	$title['title'] = 'PT KAI Sistem Informasi Tiket - Index';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/datepicker');
	$this->load->view('header/plugin/autosuggest');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');
?>	

		<!-- main -->
		<div id="main" class="main">
			
			<div class="center" style="margin-bottom:30px;">
				<div id="crumbs" class="center">
					<ul>
						<li><a href="javascript:void(0)">1. Info Jadwal </a></li>
						<li><a href="javascript:void(0)">2. Pilih KA</a></li>
						<li><a href="javascript:void(0)">3. Info Booking</a></li>
						<li><a href="javascript:void(0)">4. Isi Data</a></li>
						<li><a href="javascript:void(0)">5. Pembayaran</a></li>
						<li><a class="current" href="javascript:void(0)">6. Konfirmasi</a></li>
					</ul>
				</div>
			</div>

			<div class="wrapper clearfix center">
				
				<div class="ticket-box" style="width:90%;margin:0 auto">
						<div class="clear">
							
							
							<div class="clearfix ticket-wrapper left">
								<table align="center" style="width:100%;">
									<tbody>
										<tr>
											<td colspan="3" style="padding:7px 0;color:#E57918" class="fs28 strong center">RESERVASI TIKET ONLINE</td>
										</tr>
										
										<tr>
											<td colspan="3" class="center"><div style="background:#eaeaea" class="kereta-api-result fs16">Kode Pemesanan</div></td>
										</tr>
										<tr>
											<td colspan="3">
												<table style="width:100%;">
													<tr>
														<td class="strong" style="padding:7px 20px;width:50%">Kode Pembayaran</td>
														<td class="strong" style="padding:7px 20px;width:50%">Batas Waktu Pembayaran</td>
													</tr>
													<tr>
														<td style="padding:7px 20px;"><?php echo $random_id; ?></td>
														<td style="padding:7px 20px;"><span style="color:#FF0000"><?php echo date('D, d/M/Y H:i:s',strtotime($order_detail->TanggalOrder . "+1 days")); ?></span></td>
													</tr>
												</table>
											</td>
										</tr>

										<tr>
											<td colspan="3" class="center"><div style="background:#eaeaea" class="kereta-api-result fs16">Info Perjalanan</div></td>
										</tr>
										<tr>
											<td colspan="3">
												<table style="width:100%">
													<tr>
														<td style="padding:7px 20px;" class="strong">Tanggal</td>
														<td style="padding:7px 20px;" class="strong">No. Kereta</td>
														<td style="padding:7px 20px;" class="strong">Nama Kereta</td>
														<td style="padding:7px 20px;" class="strong">Berangkat</td>
														<td style="padding:7px 20px;" class="strong">Tiba</td>
													</tr>

													<tr>
														<td style="padding:7px 20px;"><?php echo date('D, d/M/Y',strtotime($order_detail->TanggalBerangkat)); ?></td>
														<td style="padding:7px 20px;"><?php echo $order_detail->IDKeretaApi; ?></td>
														<td style="padding:7px 20px;"><?php echo $detail_kereta->NamaKeretaApi; ?></td>
														<td style="padding:7px 20px;">
															<div><?php echo date('D, d/M/Y',strtotime($order_detail->TanggalBerangkat)); ?> <?php echo $detail_jadwal->JamBerangkat; ?></div>
															<div><?php echo $from->NamaStasiun .', '. $from->Kota; ?></div>
														</td>
														<td style="padding:7px 20px;">
															<div><?php echo (strtotime($detail_jadwal->JamSampai) < strtotime($detail_jadwal->JamBerangkat)) ? date('D, d/M/Y',strtotime($order_detail->TanggalBerangkat . "tomorrow")) : date('D, d/M/Y',strtotime($order_detail->TanggalBerangkat)); ?> <?php echo $detail_jadwal->JamSampai; ?></div>
															<div><?php echo $to->NamaStasiun .', '. $to->Kota; ?></div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										
										
										<tr>
											<td colspan="3" class="center"><div style="background:#eaeaea" class="kereta-api-result fs16">Info Data Penumpang</div></td>
										</tr>
										<tr>
											<td colspan="3">
												<table style="width:100%">
													<tr>
														<td style="padding:7px 20px;" class="strong">Nama Penumpang</td>
														<td style="padding:7px 20px;" class="strong">No. Identitas</td>
														<td style="padding:7px 20px;" class="strong">No. Kursi</td>
														<td style="padding:7px 20px;" class="strong">Kontak</td>
													</tr>

													<?php $iter = 0; ?>
													<?php foreach($penumpang as $row) : ?>
													<tr>
														<td style="padding:7px 20px;" ><?php echo $row->NamaPenumpang; ?></td>
														<td style="padding:7px 20px;" ><?php echo $row->IDPengenal; ?></td>
														<td style="padding:7px 20px;" ><?php echo strtoupper(substr($detail_kereta->Kelas,0,3)) . substr($kursi[$iter],0,1) .'-'. substr($kursi[$iter],1,2); ?></td>
														<?php if($iter == 0) : ?>
														<td style="padding:7px 20px;" rowspan="<?php echo sizeof($penumpang); ?>">
															<div><?php echo $pemesan->NamaUser; ?></div>
															<div><?php echo $pemesan->Email; ?></div>
															<div><?php echo $pemesan->NomorHandphone; ?></div>
															<div><?php echo $pemesan->Alamat; ?></div>
														</td>
														<?php endif; ?>
													</tr>
													<?php $iter++; ?>
													<?php endforeach; ?>
													
												</table>
											</td>
										</tr>
										
										<tr>
											<td colspan="3" class="center"><div style="background:#eaeaea" class="kereta-api-result fs16">Info harga</div></td>
										</tr>
										<tr>
											<td colspan="3">
												<table style="width:100%">
													<tr>
														<td class="strong" style="padding:7px 20px">Jenis Penumpang</td>
														<td class="strong" style="padding:7px 20px">Jumlah Penumpang</td>
														<td class="strong" style="padding:7px 20px">Harga Satuan</td>
														<td class="strong" style="padding:7px 20px">Total Harga</td>
													</tr>

													<tr>
														<td style="padding:7px 20px">Dewasa</td>
														<td style="padding:7px 20px"><?php echo sizeof($penumpang); ?></td>
														<td style="padding:7px 20px"><?php echo 'Rp '.number_format($this->session->userdata('tarif')); ?></td>
														<td class="right" style="padding:7px 20px"><?php echo 'Rp '.number_format($this->session->userdata('tarif') * sizeof($penumpang)); ?></td>
													</tr>
													<tr>
														<td style="padding:7px 20px">Biaya Layanan Pelanggan</td>
														<td style="padding:7px 20px"></td>
														<td style="padding:7px 20px"><?php echo 'Rp. '.number_format(7500,0); ?></td>
														<td class="right" style="padding:7px 20px"><?php echo 'Rp. '.number_format(7500,0); ?></td>
													</tr>

													<tr>
														<td colspan="3" class="right strong" style="padding:7px 20px">Total</td>
														<td class="right strong" style="padding:7px 20px"><?php echo 'Rp. '.number_format(7500 + ($this->session->userdata('tarif') * sizeof($penumpang)),0); ?></td>
													</tr>

												</table>
											</td>
										</tr>
										
										
									</tbody>
								</table>
								<div class="clearfix" style="margin:50px 200px;">
									
									<div class="float-right">
										<button class="orange-button pointer" onclick="window.location='<?php echo base_url(); ?>pdfgenerator/pemesanan/<?php echo $random_id; ?>'">Print Bukti Pemesanan &raquo;</button>
										<div class="fs16 underline pointer" style="display:inline;margin:0 10px" onclick="window.location='<?php echo base_url(); ?>pdfgenerator/pemesanan/<?php echo $random_id; ?>/D'"><img src="<?php echo base_url(); ?>public/images/pdf-icon.gif" style="height:24px;margin:0 5px;vertical-align:middle;border:none;">Download as PDF</div>
									</div>
								</div>


							</div>
						</div>
					</div>
				
			</div>
		</div>
		<!-- end main -->
		
<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
