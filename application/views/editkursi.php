<?php 
	
	$title['title'] = 'PT KAI Sistem Informasi Tiket - Index';
	
	$this->load->view('header/doctype');
	$this->load->view('header/title',$title);
	$this->load->view('header/main_assets');
	
	$this->load->view('header/plugin/modal_confirm');
	$this->load->view('header/plugin/modal');
	$this->load->view('header/plugin/datepicker');
	$this->load->view('header/plugin/autosuggest');
	
	$this->load->view('header/head_end');
	$this->load->view('header/header');
?>	

		<!-- main -->
		<div id="main" class="main">
			
			<div class="center" style="margin-bottom:30px;">
				<div id="crumbs" class="center">
					<ul>
						<li><a href="javascript:void(0)">1. Info Jadwal </a></li>
						<li><a href="javascript:void(0)">2. Pilih KA</a></li>
						<li><a href="javascript:void(0)">3. Info Booking</a></li>
						<li><a href="javascript:void(0)">4. Isi Data</a></li>
						<li><a class="current" href="javascript:void(0)">5. Pembayaran</a></li>
						<li><a href="javascript:void(0)">6. Konfirmasi</a></li>
					</ul>
				</div>
			</div>

			<div class="wrapper clearfix center">
				
				<div class="ticket-box" style="width:90%;margin:0 auto">
						<div class="clear">
							
							
							<div class="clearfix ticket-wrapper left">
								<table align="center" style="width:100%;">
									<tbody>
										<tr>
											<td colspan="3" style="padding:7px 0;color:#E57918" class="fs28 strong center">RESERVASI TIKET ONLINE</td>
										</tr>
										
										<tr>
											<td colspan="3" class="center"><div style="background:#eaeaea" class="kereta-api-result fs16">Edit Kursi (<?php echo $my_kursi; ?>)</div></td>
										</tr>
										<tr>
											<td colspan="3">

												<div id="display-gerbong">
												
												<table style="width:100%;" align="center">
													<tr>
														<?php for($i=1;$i<=$jumlahgerbong;$i++) : ?>
														<td class="center" style="padding:10px 8px">
															<div style="cursor:pointer" >
																<img src="<?php echo base_url(); ?>public/images/gerbong.jpg">
																<div alt="<?php echo $i; ?>" class="select-gerbong strong fs16"><?php echo substr(strtoupper($kelas),0,3).'-'.$i; ?></div>
															</div>
														</td>
														<?php if($i % 2 == 0) : ?>
														</tr><tr>
														<?php endif; ?>
														<?php endfor; ?>
													
												</table>

												<div class="clearfix" style="margin:50px 200px;">
													<div class="float-left"><button onclick="window.location='<?php echo base_url(); ?>/order/pembayaran/'" id="back" class="orange-button pointer">&laquo; Back</button></div>
													<div id="done-button" style="display:none" class="float-right"><button id="next" onclick="window.location='<?php echo base_url(); ?>order/konfirmasi/'" class="orange-button pointer">Done &raquo;</button></div>
												</div>
											</div>

											<?php for($i=1;$i<=$jumlahgerbong;$i++) : ?>
											<div class="center display-kursi" id="display-kursi-<?php echo $i; ?>" style="display:none">
												<div class="center" style="margin:0 auto">
													<img src="<?php echo base_url(); ?>public/images/gerbong.jpg">
													<div alt="<?php echo $i; ?>" class="select-gerbong strong fs16"><?php echo substr(strtoupper($kelas),0,3).'-'.$i; ?></div>
												

												<table style="width:100%;" align="center">

													<?php $lontrong = 1; ?>
													<?php foreach(range('A',$max_range) as $alpha) : ?>
													<tr>
														<?php for($k=1;$k<=$block_horisontal;$k++) : ?>
															
															<td><div <?php if($i.$k.$alpha == $my_kursi) : ?>title="You Here"<?php endif; ?> alt="<?php echo $i.$k.$alpha; ?>" class="pointer radius5 select-seat seats getinfo" style="padding:6px 0;color:#FFFFFF;background:<?php if(in_array($i.$k.$alpha,$taken_seat)) : ?>#FF0000<?php elseif(in_array($i.$k.$alpha, $tempat_duduk)) : ?>#E8E114<?php else : ?>#11BF36<?php endif; ?>;margin:5px;"><?php echo $k.$alpha; ?></div></td>
															
														<?php endfor; ?>
													</tr>
													<?php if($lontrong == 2) : ?>
													<tr>
														<td colspan="<?php echo $block_horisontal; ?>">&nbsp;</td>
													</tr>
													<?php endif; ?>

													<?php $lontrong++;endforeach; ?>
												</table>
												</div>

												

											</div>
											<?php endfor; ?>
											<div id="preloader" class="center" style="display:none">
												<img src="<?php echo base_url(); ?>public/images/preloader.gif" title="loading" alt="loading" style="vertical-align:middle;width:20px;margin:0 8px;"><span class="fs14 strong">Loading ...</span>
											</div>
											<div id="ajax-response" style="padding:20px;">
											</div>
											<div class="clearfix" id="done-control" style="margin:50px 200px;display:none">
												<div class="float-left"><button id="back-to-gerbong" class="orange-button pointer">&laquo; Cancel</button></div>
												<div id="done-button" class="float-right set-kursi-btn"><button id="next" onclick="return setKursi()" class="orange-button pointer">Done &raquo;</button></div>
											</div>

											<div class="fs16 strong center" id="preloader" style="display:none;"><img src="<?php echo base_url(); ?>public/images/preloader.gif" style="border:none;vertical-align:middle;margin:0 10px;height:16px;">Loading</div>
											<span id="pick-kursi" alt="<?php echo $my_kursi; ?>"></span>
											<span id="person" alt="<?php echo $person; ?>"></span>
											<span id="idperson" alt="<?php echo $idpengenal; ?>"></span>

											</td>
										</tr>
										
										
									</tbody>
								</table>
								


							</div>
						</div>
					</div>
				
			</div>
		</div>
		<!-- end main -->
		<script>
			$('.getinfo').click(function(){
				var kursi = $(this).attr('alt');
				$('#preloader').show();
				setTimeout(function(){
					$.ajax({
						url: '<?php echo base_url(); ?>order/get_info_kursi_on_hover/'+kursi+'/<?php echo $idkeretaapi; ?>/6' ,
						cache: false,
						success: function(response){
							if(response.length < 1246){
								$('.set-kursi-btn').hide();
							}
							else
							{
								$('.set-kursi-btn').show();
							}
							$('#preloader').hide();
							$('#ajax-response').html(response);
						}
					});	
				},2000);
			});

			function setKursi(){
				var pick = $('#pick-kursi').attr('alt');
				var person = $('#person').attr('alt');
				var idperson = $('#idperson').attr('alt');
				
				var con = confirm('Apa anda yakin dengan ini ?',
					function(){
						$('#preloader').fadeIn('fast');
						setTimeout(function(){
							window.location = '<?php echo base_url(); ?>kursi/updatekursi/'+idperson+'/'+person+'/'+pick+'/';
						},3000)
					}
					
				);
				
			}

			$('.seats').click(function(){
				
				$('#pick-kursi').attr('alt',$(this).attr('alt'));
				$('.seats').removeClass('kursiclicked');
				$(this).addClass('kursiclicked');
			});

			$(document).ready(function(){

				$('.select-gerbong').click(function(){
					var nomergerbong = $(this).attr('alt');
					$('#done-control').show();
					$('#display-gerbong').fadeOut('slow');
					$('#display-kursi-'+nomergerbong).fadeIn('slow');
				});


				

			});

			$('#back-to-gerbong').click(function(){
				$('.display-kursi')	.hide();
				$('#ajax-response').hide();
				$('#done-control').hide();
				$('#display-gerbong').show();

			});

		</script>
<?php 
	$this->load->view('footer/footer'); 
	$this->load->view('footer/footer_end');
	$this->load->view('footer/html_end');
?>
