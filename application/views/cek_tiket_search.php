								<table align="left" style="width:70%;">
									<tbody>
										<tr>
											<td style="padding:5px 0">Order ID</td>
											<td style="padding:5px 0">:</td>
											<td style="padding:5px 10px"><span class="strong"><?php echo $orderid; ?></span></td>
										</tr>
										<tr>
											<td style="padding:5px 0">Tanggal Order</td>
											<td style="padding:5px 0">:</td>
											<td style="padding:5px 10px"><?php echo date('D, d/M/Y H:i:s',strtotime($detail->TanggalOrder)); ?></td>
										</tr>
										<tr>
											<td style="padding:5px 0">Tanggal Keberangkatan</td>
											<td style="padding:5px 0">:</td>
											<td style="padding:5px 10px"><?php echo date('D, d/M/Y',strtotime($detail->TanggalBerangkat)); ?></td>
										</tr>
										<tr>
											<td style="padding:5px 0">Jumlah Penumpang</td>
											<td style="padding:5px 0">:</td>
											<td style="padding:5px 10px"><?php echo sizeof($penumpang);?></td>
										</tr>
										<tr>
											<td style="padding:5px 0">Asal</td>
											<td style="padding:5px 0">:</td>
											<td style="padding:5px 10px"><?php echo $asal->NamaStasiun . ', '. $asal->Kota; ?></td>
										</tr>
										<tr>
											<td style="padding:5px 0">Tujuan</td>
											<td style="padding:5px 0">:</td>
											<td style="padding:5px 10px"><?php echo $tujuan->NamaStasiun . ', '.$tujuan->Kota; ?></td>
										</tr>
										<tr>
											<td style="padding:5px 0">Kereta</td>
											<td style="padding:5px 0">:</td>
											<td style="padding:5px 10px"><?php echo $kereta->NamaKeretaApi; ?></td>
										</tr>
										<tr>
											<td style="padding:5px 0">Tarif</td>
											<td style="padding:5px 0">:</td>
											<td style="padding:5px 10px" class="strong"><?php echo 'Rp '.number_format($tarif); ?></td>
										</tr>
										<tr>
											<td style="padding:5px 0">Status</td>
											<td style="padding:5px 0">:</td>
											<td style="padding:5px 10px">
												<?php if($detail->IsVerified) : ?>
												<span style="color:#46B525" class="strong">Verified</span>
												<?php else : ?>
													<?php if(strtotime($detail->TanggalOrder . "+1 days") < strtotime(date('Y-m-d H:i:s'))) : ?>
													<span style="color:#FF0000" class="strong">Expired</span>
													<?php else : ?>
													<span style="color:#FF0000" class="strong">Unverified</span><span style="margin:0 5px;color:#FF0000">(max. pembayaran : <?php echo date('D, d/M/Y H:i:s',strtotime($detail->TanggalOrder . "+1 days")); ?>)</span>
													<?php endif; ?>
												<?php endif; ?>
											</td>
										</tr>
										<tr>
											<td style="padding:5px 0">Aksi</td>
											<td style="padding:5px 0">:</td>
											<td style="padding:5px 10px">
											<?php if($detail->IsVerified) : ?>
											<span onclick="window.location='<?php echo base_url();?>pdfgenerator/tiket/<?php echo $orderid?>/D/'" class="pointer underline"><img style="margin:0 5px;" src="<?php echo base_url(); ?>public/images/pdf.png">Download Tiket as PDF</span>
											<?php else : ?>
												<?php if(strtotime($detail->TanggalOrder . "+1 days") < strtotime(date('Y-m-d H:i:s'))) : ?>
												-
												<?php else : ?>
													<span onclick="window.location='<?php echo base_url();?>pdfgenerator/pemesanan/<?php echo $orderid?>/D/'" class="pointer underline"><img style="margin:0 5px;" src="<?php echo base_url(); ?>public/images/pdf.png">Download Bukti Pembayaran as PDF</span>
												<?php endif; ?>
											<?php endif; ?>
											
											</td>
										</tr>
									</tbody>
								</table>